//
//  Lukas_SweetsUITests.swift
//  Lukas SweetsUITests
//
//  Created by MacBook Air on 11.12.2019.
//

import XCTest

class Lukas_SweetsUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        
        let app = XCUIApplication()
        
        app.launch()
        
        //Enter phone
        app.textFields["phoneNumber"].tap()
        app.textFields["phoneNumber"].typeText("0975192551")
        app.buttons["nextButton"].tap()
        
        
        //Enter Code
        app.textFields["codeTF"].tap()
        app.textFields["codeTF"].typeText("6257")
        
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testMain() {
        
        let app = XCUIApplication()
        
        app.launch()
        
        
        //Main
        
        //Enter Card
//        XCUIApplication().scrollViews.otherElements/*@START_MENU_TOKEN@*/.otherElements["cardView"]/*[[".scrollViews.otherElements[\"cardView\"]",".otherElements[\"cardView\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .other).element.children(matching: .other).element.tap()
        
        app.scrollViews.otherElements["cardView"].tap()
        app.navigationBars.buttons.element(boundBy: 0).tap()

        
    }

}
