//
//  Sheets.swift
//  Lukas Sweets
//
//  Created by Максим on 20.01.2021.
//

import UIKit
import SwiftMessages

class Sheets: MessageView {

    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var raccoon: UIImageView!
    
    
    
    var getTacosAction: ((_ count: Int) -> Void)?
    var cancelAction: (() -> Void)?
    var okAction: (() -> Void)?
    
   
    @IBAction func okButton(_ sender: Any) {
        okAction?()
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        cancelAction?()
    }
    
    
    

    

}
