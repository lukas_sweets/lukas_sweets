//
//  SupportViewController.swift
//  Lukas Sweets
//
//  Created by Максим on 06.08.2021.
//

import UIKit


final class SupportViewController: UIViewController {
    
    
    @IBOutlet weak var FeedbackView: UIView!
    @IBOutlet weak var FAQView: UIView!
    @IBOutlet weak var RulesView: UIView!
    @IBOutlet weak var PrivacyView: UIView!
    @IBOutlet weak var HotLineView: UIView!
    
    @IBOutlet weak var youtubeImageView: UIImageView!
    @IBOutlet weak var facebookImageView: UIImageView!
    @IBOutlet weak var instagramImageView: UIImageView!
    
    @IBOutlet weak var feedbackBadge: BadgeLabel!
    
//    let db:DBHelper = DBHelper()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let FeedbacktapGesture = UITapGestureRecognizer(target: self, action: #selector(feedbackTapped(_:)))
        FeedbackView.addGestureRecognizer(FeedbacktapGesture)
        
        let FAQtapGesture = UITapGestureRecognizer(target: self, action: #selector(FAQTapped(_:)))
        FAQView.addGestureRecognizer(FAQtapGesture)
        
        let rulesTapGesture = UITapGestureRecognizer(target: self, action: #selector(rulesTapped(_:)))
        RulesView.addGestureRecognizer(rulesTapGesture)
        
        let privacyTapGesture = UITapGestureRecognizer(target: self, action: #selector(privacyTapped(_:)))
        PrivacyView.addGestureRecognizer(privacyTapGesture)
        
        let hotLineTapGesture = UITapGestureRecognizer(target: self, action: #selector(hotLineTapped(_:)))
        HotLineView.addGestureRecognizer(hotLineTapGesture)
        
        let youtubeTapGesture = UITapGestureRecognizer(target: self, action: #selector(youtubeTapped(_:)))
        youtubeImageView.addGestureRecognizer(youtubeTapGesture)
        youtubeImageView.isUserInteractionEnabled = true

        let facebookTapGesture = UITapGestureRecognizer(target: self, action: #selector(facebookTapped(_:)))
        facebookImageView.addGestureRecognizer(facebookTapGesture)
        facebookImageView.isUserInteractionEnabled = true

        let instagramTapGesture = UITapGestureRecognizer(target: self, action: #selector(instagramTapped(_:)))
        instagramImageView.addGestureRecognizer(instagramTapGesture)
        instagramImageView.isUserInteractionEnabled = true
        
        
    }
    
    @objc func feedbackTapped(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "showFeedback", sender: self)
    }
    
    @objc func FAQTapped(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "goQA", sender: self)
    }
    
    @objc func rulesTapped(_ sender: UITapGestureRecognizer) {
        guard let url = URL(string: "https://sweets.lukas.ua/terms") else {return}
        UIApplication.shared.open(url)
    }
    
    @objc func privacyTapped(_ sender: UITapGestureRecognizer) {
        guard let url = URL(string: "https://sweets.lukas.ua/privacy") else {return}
        UIApplication.shared.open(url)
    }
    
    @IBAction func hotLineTapped(_ sender: Any) {
        guard let number = URL(string: "tel://0800505091") else { return }
        UIApplication.shared.open(number)
        
    }
    
    
    @objc func youtubeTapped(_ sender: UITapGestureRecognizer) {
        let youtubeId = "UC08try613MHlV6y8qq8B7kg"
            var youtubeUrl = NSURL(string:"youtube://\(youtubeId)")!
        if UIApplication.shared.canOpenURL(youtubeUrl as URL){
                UIApplication.shared.open(youtubeUrl as URL)
            } else{
                    youtubeUrl = NSURL(string:"https://www.youtube.com/channel/\(youtubeId)")!
                UIApplication.shared.open(youtubeUrl as URL)
            }
    }
    
    @objc func facebookTapped(_ sender: UITapGestureRecognizer) {
        let instagramHooks = "facebook://user?username=lukas.company"
        let instagramUrl = NSURL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
            UIApplication.shared.open(instagramUrl! as URL)
        } else {
          //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.open(NSURL(string: "https://facebook.com/lukas.company")! as URL)
        }
    }
    
    @objc func instagramTapped(_ sender: UITapGestureRecognizer) {
        let instagramHooks = "instagram://user?username=lukas.from.ukraine"
        let instagramUrl = NSURL(string: instagramHooks)
        if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
            UIApplication.shared.open(instagramUrl! as URL)
        } else {
          //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.open(NSURL(string: "https://instagram.com/lukas.from.ukraine")! as URL)
        }
    }
    
    
    private func checkNewFeedback() {
        Task {
            let badgeSupport = await FeedbackRepository.getNewMessages()
//            DispatchQueue.main.async {
                if(badgeSupport > 0) {
                    self.feedbackBadge.text = String(badgeSupport)
                } else {
                    self.feedbackBadge.isHidden = true
                }
//            }
        }
//        DispatchQueue.global().async {
//            let badgeSupport = self.db.getCountNewTopicFeedback()
//            DispatchQueue.main.async {
//                if(badgeSupport > 0) {
//                    self.feedbackBadge.text = String(badgeSupport)
//                } else {
//                    self.feedbackBadge.isHidden = true
//                }
//            }
//        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBar()

        checkNewFeedback()

    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    
    
}
