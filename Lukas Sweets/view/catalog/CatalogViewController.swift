//
//  CatalogViewController.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 07.04.2023.
//

import UIKit



class CatalogViewController: UIViewController, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {
    typealias DataSource = UICollectionViewDiffableDataSource<SectionsData, PacksProductsModel>

    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var catalogCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    
    private let refreshControl = UIRefreshControl()

    
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        searchString = searchText
    }
    
    private var search = UISearchController(searchResultsController: nil)
    
    private var categories: [CategoriesModel] = []
    private var selectedCategories: [String] = [] {
        didSet {
            updateFilter()
        }
    }
    private var selectedCategoriesIndex: [IndexPath] = []
    private var catalog: [PacksProductsModel] = [] {
        didSet {
            filteredCatalog = catalog
        }
    }
    private var filteredCatalog: [PacksProductsModel] = []
    private var searchString: String = "" {
        didSet {
            updateFilter()
        }
    }
    
    private lazy var catalogDataSource = configureCatalogDataSource()
    
    private var sectionData: [SectionsData] = [] {
        didSet {
            filteredSectionData = sectionData
        }
    }
    private var filteredSectionData: [SectionsData] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = search
        definesPresentationContext = true
        
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        catalogCollectionView.delegate = self
        catalogCollectionView.dataSource = catalogDataSource
        
        getData()
        configureLayout()
        pullToRefresh()

        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        self.categoriesCollectionView.collectionViewLayout = flowLayout
        
        catalogCollectionView.register(
          CatalogSectionHeaderReusableView.self,
          forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
          withReuseIdentifier: CatalogSectionHeaderReusableView.reuseIdentifier
        )
    }
    
    private func pullToRefresh () {
        refreshControl.addTarget(self, action: #selector(getData), for: .valueChanged)
        self.refreshControl.tintColor = .red
        self.refreshControl.layer.zPosition += 1
        self.mainScrollView.addSubview(refreshControl)
    }
    
    @objc private func getData() {
        Task {
            activityIndicator.startAnimating()
            let resultApi = await CatalogRepository.getCategories()
            
            switch resultApi {
            case .success(let data):
                categories = data.categories
                let catalog = data.packs
                
                sectionData.removeAll()
                categories.forEach { category in
                    let packs = catalog.filter{$0.category == category.name}
                    if !packs.isEmpty {
                        sectionData.append(SectionsData(name: category.name, packs: packs))
                    }
                }
                
                categoriesCollectionView.reloadData()
                updateCatalogSnapshot()
                catalogCollectionView.layoutIfNeeded()
                break
            case .internet:
                Alert.noInternet()
                break
            default:
                Alert.serverError()
                break
            }
            activityIndicator.stopAnimating()
            refreshControl.isRefreshing ? refreshControl.endRefreshing() : nil
            updateFilter()
        }
    }
    
    private func updateFilter() {
        
        filteredSectionData = sectionData
        
        if !selectedCategories.isEmpty {
            filteredSectionData = filteredSectionData.filter({selectedCategories.contains($0.name)})
        }
        
        if !searchString.isEmpty {
            var filter: [SectionsData] = []
            filteredSectionData.forEach({section in
                let f = section.packs.filter({$0.name.lowercased().contains(searchString.lowercased())})
                if !f.isEmpty {
                    filter.append(SectionsData(name: section.name, packs: f))
                }
            })
            
            filteredSectionData = filter
        }
        
        updateCatalogSnapshot()
        catalogCollectionView.layoutIfNeeded()
        
    }
    
    
    @IBAction func scanProduct(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "scanProduct", sender: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}

extension CatalogViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case categoriesCollectionView:
            return categories.count
        default:
            return 0
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        switch collectionView{
        case categoriesCollectionView:
            let cellCategories = collectionView.dequeueReusableCell(
                withReuseIdentifier: "cellCategories",
                for: indexPath) as! CategoryCollectionViewCell
            let data = categories[indexPath.row]
            
            // Configure the cell
            cellCategories.categoryView.titleLabel.text = data.name
            cellCategories.categoryView.iconImageView.sd_setImage(with: URL(string: data.icon), placeholderImage: nil, options: .avoidAutoSetImage) { image,_,_, finished in
                if let image = image, let _ = finished {
                    cellCategories.categoryView.iconImageView.image = image.sd_tintedImage(with: .gray)
                }
            }
            
            if selectedCategoriesIndex.contains(indexPath) {
                cellCategories.categoryView.select = true
            } else {
                cellCategories.categoryView.select = false
            }
            return cellCategories
        default:
            break
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case categoriesCollectionView:
            if selectedCategoriesIndex.contains(indexPath) {
                selectedCategoriesIndex.removeAll(where: {$0 == indexPath})
                selectedCategories.removeAll(where: {$0 == categories[indexPath.row].name})
            } else {
                selectedCategoriesIndex.append(indexPath)
                selectedCategories.append(categories[indexPath.row].name)
            }
            UIView.performWithoutAnimation {
                collectionView.reloadItems(at: [indexPath])
            }
        case catalogCollectionView:
            guard let pack = catalogDataSource.itemIdentifier(for: indexPath) else {
              return
            }

            let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(identifier: "ProductViewController") as! ProductViewController
            vc.packID = pack.id
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2-15, height: UIScreen.main.bounds.height/5)
    }
    
}


private extension CatalogViewController {
    func configureCatalogDataSource() -> DataSource {
        
        let dataSource = DataSource(collectionView: catalogCollectionView, cellProvider:{ (collectionView, indexPath, product) -> UICollectionViewCell? in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCatalog", for: indexPath) as? CatalogCollectionViewCell
            cell?.titleLabel.text = product.name
            cell?.imageView.sd_setImage(with: URL(string: product.image))
            
            return cell
        })
        
        dataSource.supplementaryViewProvider = { collectionView, kind, indexPath in
//            guard kind == UICollectionView.elementKindSectionHeader else {
//                return nil
//            }
            let section = self.catalogDataSource.snapshot()
                .sectionIdentifiers[indexPath.section]
            let view = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: CatalogSectionHeaderReusableView.reuseIdentifier,
                for: indexPath) as? CatalogSectionHeaderReusableView
            view?.titleLabel.text = section.name.uppercased()
            return view
        }
        
        return dataSource
    }
    
    func updateCatalogSnapshot(animatingChange: Bool = false) {
        var snapshot = NSDiffableDataSourceSnapshot<SectionsData, PacksProductsModel>()
        snapshot.appendSections(filteredSectionData)
        filteredSectionData.forEach { section in
            snapshot.appendItems(section.packs, toSection: section)
        }
        catalogDataSource.apply(snapshot, animatingDifferences: true)
    }
}

struct SectionsData: Hashable {
    let name: String
    let packs: [PacksProductsModel]
}





extension CatalogViewController {
    private func configureLayout() {
        catalogCollectionView.register(
            CatalogSectionHeaderReusableView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: CatalogSectionHeaderReusableView.reuseIdentifier
        )
        catalogCollectionView.collectionViewLayout = UICollectionViewCompositionalLayout(sectionProvider: { (sectionIndex, layoutEnvironment) -> NSCollectionLayoutSection? in
            let size = NSCollectionLayoutSize(
                widthDimension: NSCollectionLayoutDimension.fractionalWidth(1),
                heightDimension: NSCollectionLayoutDimension.absolute(UIScreen.main.bounds.height/5)
            )
            let item = NSCollectionLayoutItem(layoutSize: size)
            item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: size, subitem: item, count: 2)
            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0)
//            section.interGroupSpacing = 10
            // Supplementary header view setup
            let headerFooterSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .estimated(20)
            )
            let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: headerFooterSize,
                elementKind: UICollectionView.elementKindSectionHeader,
                alignment: .top
            )
            section.boundarySupplementaryItems = [sectionHeader]
            return section
        })
    }
}
