//
//  AvailabilityViewController.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 12.04.2023.
//

import UIKit


class AvailabilityViewController: UIViewController, DropDownDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cityCustomTextField: CustomTextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var stores: [ProductAvailabilityModel] = []
    private var selectedCity: DropDownList?
    
    
    private var cities: [DropDownStruct] = []
    
    var packID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        getDB()
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(selectCity))
        tap2.numberOfTapsRequired = 1
        cityCustomTextField.tapGestureRecognizer = tap2
        
    }
    
    func changeValue(value: DropDownList?, clear: Bool) {
        if let value = value {
            cityCustomTextField.text = value.name
        }
        
        cityCustomTextField.isEdit = false
        selectedCity = value
        cityCustomTextField.text = selectedCity?.name
        updateUserCity(cityID: value?.name)
        getAvailabilities()
    }
    
    private func getDB() {
        Task {
            let result = await [
                UserRepository.getUserCity() ?? "",
                StoreRepositoty.getCities()
                    .enumerated()
                    .map { (index, element) in
                        DropDownList(id: index, name: element)
                    }
                    .sorted {
                        $0.name < $1.name
                    }
            ] as [Any]
            
            cities = [DropDownStruct(list: result[1] as? [DropDownList])]
            cities.forEach({ l in
                selectedCity = l.list?.filter({
                    $0.name == result[0] as? String
                }).first
            })
            cityCustomTextField.text = selectedCity?.name
            getAvailabilities()
        }
    }
    
    private func updateUserCity(cityID: String?) {
        Task {
            await UserRepository.updateUserCity(cityID: cityID)
        }
    }
    
    @objc private func selectCity() {
        cityCustomTextField.isEdit = true
        let storyboard = UIStoryboard(name: "DropDown", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DropDownController") as! DropDownController
        vc.dataList = cities
        vc.selected = selectedCity
        vc.titleLabel = "city".localized
        vc.showClear = false
        vc.delegate = self
        self.present(vc, animated: true)
    }
    
    private func getAvailabilities() {
        Task {
            activityIndicator.startAnimating()
            guard let packID = packID else { return }
            switch await CatalogRepository.getAvailabilityProduct(packID: packID, cityID: selectedCity?.name) {
            case .success(let data):
                stores = data
                tableView.reloadData()
                break
            case .internet:
                Alert.noInternet()
                self.dismiss(animated: true)
                break
            default:
                Alert.serverError()
                self.dismiss(animated: true)
            }
            activityIndicator.stopAnimating()

        }
    }
    
}

extension AvailabilityViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "availabilityCell", for: indexPath) as? AvailabilityTableViewCell else { return UITableViewCell() }
        let store = stores[indexPath.row]
        cell.addressLabel.text = store.address
        cell.timeWorkLabel.text = store.time_work
        switch store.availability {
        case .InStock:
            cell.availabilityLabel.text = "availability_ok".localized
            cell.availabilityLabel.textColor = .systemGreen
            break
        case .Ends:
            cell.availabilityLabel.text = "availability_min".localized
            cell.availabilityLabel.textColor = .orange
            break
        case .OutOfStock:
            cell.availabilityLabel.text = "availability_off".localized
            cell.availabilityLabel.textColor = .red
            break
        }
        
        return cell
    }
    
    
}
