//
//  PackCollectionViewCell.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 11.04.2023.
//

import UIKit

class PackCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var button: PackButton!
    
}


@IBDesignable
class PackButton: UIButton {
    
    override init(frame: CGRect) {
            super.init(frame: frame)
        }

    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
    
    var config: UIButton.Configuration = {
        
        var configuration = UIButton.Configuration.gray()
        configuration.baseForegroundColor = .gray
        configuration.baseBackgroundColor = .backgroundGray
        
        return configuration
    }()
    
    override func updateConfiguration() {
        guard let configuration = configuration else { return }
        
        // 1
        var updatedConfiguration = configuration
        var background = configuration.background
        
        updatedConfiguration.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
            var outgoing = incoming
            outgoing.font = .systemFont(ofSize: 15)
            return outgoing
         }


        var foregroundColor: UIColor = .gray
        var backgroundColor: UIColor = .backgroundGray
        
                switch self.state {
//                case .normal:
//                    backgroundColor = .backgroundGray
//                    foregroundColor = .gray
//
                case .selected:
                    backgroundColor = .selectedRed
                    foregroundColor = .red
                default:
                    break
                }
        
        background.backgroundColor = backgroundColor
        
        updatedConfiguration.baseForegroundColor = foregroundColor
        updatedConfiguration.background = background
        
        self.configuration = updatedConfiguration
    }
    
    func setTitleText(_ text: String?) {
        self.setTitle(text)
    }
    
    
}
