//
//  CatalogCategoryCollectionViewCell.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 07.04.2023.
//

import UIKit


class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryView: CategoryView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
