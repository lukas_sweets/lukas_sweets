//
//  CatalogSectionHeaderReusableView.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 17.04.2023.
//

import UIKit


class CatalogSectionHeaderReusableView: UICollectionReusableView {
  static var reuseIdentifier: String {
    return String(describing: CatalogSectionHeaderReusableView.self)
  }
  
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.systemFont(
        ofSize: 35,//UIFont.preferredFont(forTextStyle: .headline).pointSize,
        weight: .black)
    label.adjustsFontForContentSizeCategory = true
      label.textColor = #colorLiteral(red: 0.9607843137, green: 0.862745098, blue: 0.8588235294, alpha: 1)//#colorLiteral(red: 0.878935039, green: 0.878935039, blue: 0.878935039, alpha: 1)//.lightGray
    label.textAlignment = .left
    label.numberOfLines = 0
    label.setContentCompressionResistancePriority(
      .defaultHigh, for: .horizontal)
    return label
      
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubview(titleLabel)
    
      NSLayoutConstraint.activate([
        titleLabel.leadingAnchor.constraint(
          equalTo: readableContentGuide.leadingAnchor),
        titleLabel.trailingAnchor.constraint(
          lessThanOrEqualTo: readableContentGuide.trailingAnchor)
      ])
    
    NSLayoutConstraint.activate([
      titleLabel.topAnchor.constraint(
        equalTo: topAnchor,
        constant: 0),
      titleLabel.bottomAnchor.constraint(
        equalTo: bottomAnchor,
        constant: 0)
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
