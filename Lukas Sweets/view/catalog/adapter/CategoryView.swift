//
//  CategoryView.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 08.04.2023.
//

import UIKit

@IBDesignable
class CategoryView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override init(frame: CGRect){
            super.init(frame: frame)
        setup()
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)
            setup()
        }
    
    override func layoutSubviews() {
        super.layoutSubviews()

    }

    private func setup() {
        Bundle.main.loadNibNamed("CategoryView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

    var select: Bool = false {
        didSet {
            if select {
                bgView.backgroundColor = .selectedRed
                titleLabel.textColor = .red
                iconImageView.image = iconImageView.image?.withTintColor(.red)
            } else {
                bgView.backgroundColor = .backgroundGray
                titleLabel.textColor = .gray
                iconImageView.image = iconImageView.image?.withTintColor(.gray)

            }
            
        }
    }

}
