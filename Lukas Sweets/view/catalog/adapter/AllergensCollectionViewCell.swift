//
//  AllergensCollectionViewCell.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 11.04.2023.
//

import UIKit

class AllergensCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    
}
