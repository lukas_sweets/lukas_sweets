//
//  CatalogCollectionViewCell.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.04.2023.
//

import UIKit


class CatalogCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}
