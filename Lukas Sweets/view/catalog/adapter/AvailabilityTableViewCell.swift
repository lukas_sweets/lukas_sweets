//
//  AvailabilityTableViewCell.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 12.04.2023.
//

import UIKit

class AvailabilityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timeWorkLabel: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
