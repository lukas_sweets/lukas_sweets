//
//  ScanProduct.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 13.04.2023.
//

import AVFoundation
import UIKit
import PKHUD

class ScanProductViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
        
    private var videoCaptureDevice = AVCaptureDevice.default(for: .video)

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = videoCaptureDevice else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
//            try videoCaptureDevice.lockForConfiguration()
//            videoCaptureDevice.torchMode = .on
//            videoCaptureDevice.unlockForConfiguration()

        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [ .code128, .upce, .ean8, .ean13, .code39]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        previewLayer.zPosition -= 1

        DispatchQueue.global().async {
            self.captureSession.startRunning()
        }
        
    }

    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            DispatchQueue.global().async {
                self.captureSession.startRunning()
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    @IBAction func flash() {
        do {
            try videoCaptureDevice?.lockForConfiguration()
            videoCaptureDevice?.torchMode = videoCaptureDevice?.torchMode == .on ? .off : .on
            videoCaptureDevice?.unlockForConfiguration()
        } catch {}
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }

        dismiss(animated: true)
    }

    func found(code: String) {
        Task {
            HUD.show(.progress)
            let dataApi = await CatalogRepository.getProductFromBarcode(barcode: code)
            HUD.hide()
            
            switch dataApi {
            case .success(let packID):
                let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
                vc.packID = packID
                vc.scaned = true
                navigationController?.pushViewController(vc, animated: true)
                break
            case .internet:
                Alert.noInternet()
                _ = navigationController?.popViewController(animated: true)
            default:
                Alert.noFind()
                _ = navigationController?.popViewController(animated: true)
            }
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}
