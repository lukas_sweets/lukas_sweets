//
//  ProductViewController.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.04.2023.
//

import UIKit
import SkeletonView
import PKHUD

class ProductViewController: UIViewController {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var priceStackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var availabilityButton: UIButton!
    @IBOutlet weak var carbsLabel: UILabel!
    @IBOutlet weak var proteinsLabel: UILabel!
    @IBOutlet weak var fatsLabel: UILabel!
    @IBOutlet weak var energyLabel: UILabel!
    @IBOutlet weak var compositionLabel: UILabel!
    @IBOutlet weak var storageConditionsLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var infoStackView: UIStackView!
    
    @IBOutlet weak var packsCollectionView: UICollectionView!
    @IBOutlet weak var allergensCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    enum Section {
        case all
    }
    
    var packID: Int?
    var scaned: Bool = false
    
    private var loaded: Bool = true {
        didSet {
            if loaded {
                view.showAnimatedGradientSkeleton( transition: .crossDissolve(0.25))
            } else {
                view.hideSkeleton(transition: .crossDissolve(0.25))
                setupBgView()
                updatePacksSnapshot()
                updateAllergenSnapshot()
                allergensCollectionView.layoutIfNeeded()
                HUD.hide()
            }
        }
    }
    
    private var packs: [AnalogModel] = []
    private var allergens: [String] = []
    
    private lazy var packsDataSource = configurePacksDataSource()
    private lazy var allergensDataSource = configureAllergensDataSource()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if scaned {
            navigationController?.viewControllers.remove(at: 1)
            scaned = false
        }
        
        packsCollectionView.dataSource = packsDataSource
        allergensCollectionView.dataSource = allergensDataSource
        
        setupPackCollectionView()
        getProduct()
        loaded = true
    }
    
    private func setupBgView() {
        bgView.cornerRadius = 40
        bgView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    private func setupPackCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        self.packsCollectionView.collectionViewLayout = flowLayout
    }
    
    @IBAction func availabilityButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "availability", sender: self)
    }
    
    
    
    private func getProduct() {
        Task {
            HUD.show(.progress, onView: view)
            let userCity = await UserRepository.getUserCity()
            guard let packID = packID else { return }
            let result = await CatalogRepository.getProduct(packID: packID, cityID: userCity)
            switch result {
            case .success(let productModel):
                if let packs = productModel?.analog {
                    self.packs = packs
                }
                
                if let allergens = productModel?.pack.allergens {
                    self.allergens = allergens
                }
                setProduct(product: productModel?.pack)
                scrollView.setContentOffset(CGPoint.zero, animated: true)

                break
            case .internet:
                Alert.noInternet()
                self.dismiss(animated: false)
                break
            default:
                Alert.serverError()
                self.dismiss(animated: false)
                break
            }
        }
    }
    
    private func setProduct(product: PackModel?) {
        if let product = product {
            loaded = false
            productImageView.sd_setImage(with: URL(string: product.image))
            titleLabel.text = product.product_name
            descriptionLabel.text = product.description
            weightLabel.text = product.short_name
            
            if let price = product.price {
                priceLabel.text = "money".localized_with_argument(arg: price.clean)
            } else {
                priceStackView.isHidden = true
            }
            
            carbsLabel.text = "gram".localized_with_argument(arg: String(product.carbs))
            proteinsLabel.text = "gram".localized_with_argument(arg: String(product.proteins))
            fatsLabel.text = "gram".localized_with_argument(arg: String(product.fats))
            energyLabel.text = "kcal".localized_with_argument(arg: String(product.energy_value))
            
            compositionLabel.text = product.composition
            storageConditionsLabel.text = product.storage_conditions
            expirationDateLabel.text = product.expiration_date
        } else {
            self.dismiss(animated: false)
        }
    }
    
    @objc private func setPackAnalog(_ sender: UIButton) {
        packID = packs[sender.tag].id
        getProduct()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "availability":
            (segue.destination as? AvailabilityViewController)?.packID = packID
        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.whiteNavigationBar()
    }
    
    
}

/*
 
 MARK: Packs Data Source
 
*/

private extension ProductViewController {
    func configurePacksDataSource() -> UICollectionViewDiffableDataSource<Section, AnalogModel> {
        
        let dataSource = UICollectionViewDiffableDataSource<Section, AnalogModel>(collectionView: packsCollectionView) { (collectionView, indexPath, pack) -> UICollectionViewCell? in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "packCell", for: indexPath) as! PackCollectionViewCell
            cell.button.setTitleText(pack.name)
            if pack.current == true {
                cell.button.isSelected = true
            } else {
                cell.button.isSelected = false
            }
            cell.button.tag = indexPath.row
            cell.button.addTarget(self, action: #selector(self.setPackAnalog(_:)), for: .touchUpInside)
            
            return cell
        }
        
        return dataSource
    }
    
    func updatePacksSnapshot(animatingChange: Bool = false) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, AnalogModel>()
        snapshot.appendSections([.all])
        snapshot.appendItems(packs, toSection: .all)
        
        packsDataSource.apply(snapshot, animatingDifferences: true)
    }
}


/*
 
 MARK: Allergens Data Source
 
*/
private extension ProductViewController {
    func configureAllergensDataSource() -> UICollectionViewDiffableDataSource<Section, String> {
        
        let dataSource = UICollectionViewDiffableDataSource<Section, String>(collectionView: allergensCollectionView) { (collectionView, indexPath, allergen) -> UICollectionViewCell? in
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "allergenCell", for: indexPath) as! AllergensCollectionViewCell
            
            cell.title.text = allergen.localized
            
            switch allergen {
            case "celery":
                cell.icon.image = .init(named: "ic_celery")
            case "gluten":
                cell.icon.image = .init(named: "ic_gluten")
            case "crustaceans":
                cell.icon.image = .init(named: "ic_crustaceans")
            case "eggs":
                cell.icon.image = .init(named: "ic_egg")
            case "fish":
                cell.icon.image = .init(named: "ic_fish")
            case "milk":
                cell.icon.image = .init(named: "ic_milk")
            case "shellfish":
                cell.icon.image = .init(named: "ic_seashell")
            case "mustard":
                cell.icon.image = .init(named: "ic_mustard")
            case "nuts":
                cell.icon.image = .init(named: "ic_nut")
            case "peanuts":
                cell.icon.image = .init(named: "ic_peanut")
            case "lupin":
                cell.icon.image = .init(named: "ic_lupin")
            case "sesame":
                cell.icon.image = .init(named: "ic_sesame")
            case "soy":
                cell.icon.image = .init(named: "ic_soy")
            case "sulfite":
                cell.icon.image = .init(named: "ic_sulfite")
            default:
                break
            }
            
            
            return cell
        }
        
        return dataSource
    }
    
    func updateAllergenSnapshot(animatingChange: Bool = false) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, String>()
        snapshot.appendSections([.all])
        snapshot.appendItems(allergens, toSection: .all)
        
        allergensDataSource.apply(snapshot, animatingDifferences: true)
    }
}
