//
//  LoyalInfoViewController.swift
//  Lukas Sweets
//
//  Created by Максим on 18.12.2020.
//

import Foundation
import UIKit

final class LoyalInfoViewController: UIViewController {
    
    
    
    @IBOutlet weak var qaView: UIView!
    var QATapGesture = UITapGestureRecognizer()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        QATapGesture = UITapGestureRecognizer(target: self, action: #selector(QATapped(_:)))
        qaView.addGestureRecognizer(QATapGesture)

        
    }
    
    @objc func QATapped(_ sender: UITapGestureRecognizer) {
        let vc = UIStoryboard(name: "Support", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBarLargeTitle()
    }
    
    
   
    
    

    
    
    
    
}

