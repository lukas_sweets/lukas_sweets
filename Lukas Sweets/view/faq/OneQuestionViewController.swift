//
//  OneQuestionViewController.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 7/10/20.
//

import UIKit

final class OneQuestionViewController: UIViewController, UIScrollViewDelegate {
    
    var data: FAQ!
    @IBOutlet weak var titleQ: UILabel!
    @IBOutlet weak var answer: UILabel!
    
    

    @IBOutlet weak var scrollView: UIScrollView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleQ.text = data.question
        answer.text = data.answer
        self.title = data.question
        scrollView.delegate = self
        
        scrollViewDidScroll(scrollView)

           
    }
    
    @IBAction func callHotLine(_ sender: Any) {
        guard let number = URL(string: "tel://0800505091") else { return }
        UIApplication.shared.open(number)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       let contentYoffset = scrollView.contentOffset.y
        
        let valueAlpha = (contentYoffset-(titleQ.bounds.maxY/2))/(titleQ.bounds.maxY/2)
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(valueAlpha)]

   }
    
  
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBar()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.clear]

        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
