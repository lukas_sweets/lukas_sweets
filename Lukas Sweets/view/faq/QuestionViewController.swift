//
//  QuestionViewController.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 7/9/20.
//

import UIKit

final class QuestionViewController: UIViewController {
    
    var dataSource: [FAQ] = []
    
    let cellReuseIdentifier = "cell"
    var indexRow = 0
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
                
        tableView.dataSource = self
        tableView.delegate = self
        
        loadResource()
        
    }
    
    private func loadResource () {
        self.dataSource.append(FAQ(question: "faq_q_1".localized, answer: "faq_a_1".localized))
        self.dataSource.append(FAQ(question: "faq_q_2".localized, answer: "faq_a_2".localized))
        self.dataSource.append(FAQ(question: "faq_q_3".localized, answer: "faq_a_3".localized))
        self.dataSource.append(FAQ(question: "faq_q_4".localized, answer: "faq_a_4".localized))
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToOneQuestion" {
            //guard let selectedPath = tableView.indexPathForSelectedRow else { return }
            
            let dat = segue.destination as! OneQuestionViewController
            dat.data = FAQ(question: dataSource[indexRow].question, answer: dataSource[indexRow].answer)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)

        self.whiteNavigationBar()
        
    }
}

extension QuestionViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)!
        
        // set the text from the data model
        cell.textLabel?.text = self.dataSource[indexPath.row].question
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexRow = indexPath.row
        performSegue(withIdentifier: "goToOneQuestion", sender: self)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
}
