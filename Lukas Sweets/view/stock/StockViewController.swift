//
//  StockViewController.swift
//  Lukas Sweets
//
//  Created by Максим on 21.12.2020.
//

import UIKit

class StockViewController: UIViewController {
    
    
    @IBOutlet weak var bgLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imgImageView: UIImageView!
    @IBOutlet weak var newPriceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var newPriceStackView: UIStackView!
    @IBOutlet weak var oldPriceStackView: UIStackView!
    @IBOutlet weak var descriptionsLabel: UILabel!
    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var label_text: UILabel!
    
    var stockID: Int?
    var stock: StockModel?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bgLabel.alpha = 0.1
        bgView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        bgView.layer.cornerRadius = 40
        
        
        Task {
            if let stock = await StockRepository.getStock(id: stockID) {
                self.stock = stock
                timeLabel.text = stock.time
                nameLabel.text = stock.name
                bgLabel.text = stock.short_name
                
                
                if let new_price = stock.new_price {
                    newPriceLabel.text = new_price + " ₴"
                } else {
                    newPriceStackView.isHidden = true
                }
                
                if let old_price = stock.old_price {
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: old_price + " ₴")
                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                    oldPriceLabel.attributedText = attributeString
                } else {
                    oldPriceStackView.isHidden = true
                }
                
                
                if (stock.show_label) {
                    if let text = stock.label_text, let color = stock.label_color, let bg_color = stock.label_bg_color {
                        labelView.backgroundColor = UIColor(hex: bg_color + "FF")
                        label_text.text = text
                        label_text.textColor = UIColor(hex: color + "FF")
                        labelView.layer.cornerRadius = 10
                    } else {
                        labelView.isHidden = true
                    }
                } else {
                    labelView.isHidden = true
                }
            
                descriptionsLabel.text = stock.descriptions
                imgImageView.loadImage(fromURL: stock.picture)
                
                if let _ = stock.products_pack_id {
                    setLinkProduct()
                }
                
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    private func setLinkProduct() {
        let tabBatItem = UIBarButtonItem(image: .init(systemName: "info.circle"), style: .plain, target: self, action: #selector(openProduct))
        self.navigationItem.rightBarButtonItem = tabBatItem
    }
    
    @objc private func openProduct() {
        let vc = UIStoryboard(name: "Product", bundle: nil).instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
        vc.packID = stock?.products_pack_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        self.clearNavigationBar()
    }
}
