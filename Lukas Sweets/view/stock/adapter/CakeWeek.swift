//
//  CakeWeek.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 12.09.2022.
//
import UIKit

class CakeWeek: UIView{
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var bgMockupImage: UIImageView!
    
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CakeWeek", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        bgMockupImage.isHidden = true
    }
}
