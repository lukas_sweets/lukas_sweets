//
//  FeedBackViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 10.02.2020.
//

import UIKit
import AudioToolbox
import PKHUD
import Alamofire

final class FeedBackViewController: UIViewController, DropDownDelegate {
    
    @IBOutlet weak var nameCustomTextEdit: CustomTextField!
    @IBOutlet weak var subjectCustomTextEdit: CustomTextField!
    @IBOutlet weak var cityCustomTextEdit: CustomTextField!
    @IBOutlet weak var storeCustomTextEdit: CustomTextField!
    @IBOutlet weak var textCustomTextEdit: CustomTextField!
    @IBOutlet weak var sendButton: UIButton!
    
    
    private var newFeedbackModel = NewFeedbackModel()
    
    private var tagDropDown: DropDownTag = .none
    private var selectedCity: DropDownList?
    private var selectedStore: DropDownList?
    private var selectedSubject: DropDownList?
    
    enum DropDownTag {
        case cities
        case stores
        case subjects
        case none
    }
    
    
    func changeValue(value: DropDownList?, clear: Bool) {
        switch tagDropDown {
        case .cities:
            if let value = value {
                cityCustomTextEdit.text = value.name
                newFeedbackModel.city = value.name
            }
            if clear {
                cityCustomTextEdit.text = nil
                newFeedbackModel.city = nil
            }
            
            cityCustomTextEdit.isEdit = false
            getStores()
            if selectedCity?.name != value?.name {
                selectedStore = nil
                storeCustomTextEdit.text = nil
            }
            selectedCity = value
            break
        case .stores:
            if let value = value {
                storeCustomTextEdit.text = value.name
                cities.forEach({ city in
                    selectedCity = city.list?.filter({
                        $0.name == (value.model as? StoreModel)?.city_id
                    }).first
                })
                cityCustomTextEdit.text = selectedCity?.name
                getStores()
                newFeedbackModel.store = (value.model as? StoreModel)?.id
                newFeedbackModel.city = selectedCity?.name
            } else {
                storeCustomTextEdit.text = nil
                newFeedbackModel.store = nil
            }
            storeCustomTextEdit.isEdit = false
            selectedStore = value
            
            break
            
        case .subjects:
            if let value = value {
                subjectCustomTextEdit.text = value.name
                newFeedbackModel.subject = value.id
            }
            subjectCustomTextEdit.isEdit = false
            selectedSubject = value
            break
            
        default:
            break
        }
        tagDropDown = .none
    }
    
    
    
    private var cities: [DropDownStruct] = []
    private var store: [DropDownStruct] = []
    private var subjects: [DropDownStruct] = [
    DropDownStruct(list: [
    DropDownList(id: 1, name: "feedback_subject_gratitude".localized),
    DropDownList(id: 2, name: "feedback_subject_questions".localized),
    DropDownList(id: 3, name: "feedback_subject_complaint".localized),
    DropDownList(id: 4, name: "feedback_subject_offer".localized),
    DropDownList(id: 5, name: "feedback_subject_app".localized),
    ])]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dismissKeyboard()
        
        
        setStartFields()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(selectSubject))
        tap.numberOfTapsRequired = 1
        subjectCustomTextEdit.tapGestureRecognizer = tap
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(selectCity))
        tap2.numberOfTapsRequired = 1
        cityCustomTextEdit.tapGestureRecognizer = tap2
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(selectStore))
        tap3.numberOfTapsRequired = 1
        storeCustomTextEdit.tapGestureRecognizer = tap3
        
        
    }
    
    @objc func selectSubject() {
        subjectCustomTextEdit.isEdit = true
        let storyboard = UIStoryboard(name: "DropDown", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DropDownController") as! DropDownController
        vc.dataList = subjects
        vc.selected = selectedSubject
        vc.titleLabel = "subject".localized
        vc.showClear = false
        vc.delegate = self
        tagDropDown = .subjects
        self.present(vc, animated: true)
    }
    
    @objc func selectCity() {
        cityCustomTextEdit.isEdit = true
        let storyboard = UIStoryboard(name: "DropDown", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DropDownController") as! DropDownController
        vc.dataList = cities
        vc.selected = selectedCity
        vc.titleLabel = "city".localized
        vc.showClear = true
        vc.delegate = self
        tagDropDown = .cities
        self.present(vc, animated: true)
    }
    
    @objc func selectStore() {
        storeCustomTextEdit.isEdit = true
        let storyboard = UIStoryboard(name: "DropDown", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DropDownController") as! DropDownController
        vc.dataList = store
        vc.selected = selectedStore
        vc.titleLabel = "store".localized
        vc.showClear = true
        vc.delegate = self
        tagDropDown = .stores
        self.present(vc, animated: true)
    }
    
    
    
    private func setStartFields() {
        Task {
            newFeedbackModel = NewFeedbackModel()
            if let userModel = await UserRepository.getUser() {
                let lastName = userModel.name + " " + (userModel.surname ?? "")
                nameCustomTextEdit.text = lastName.trim()
            }
            subjectCustomTextEdit.text = ""
            cityCustomTextEdit.text = ""
            storeCustomTextEdit.text = ""
            textCustomTextEdit.text = ""
            selectedCity = nil
            selectedStore = nil
            selectedSubject = nil
            getCities()
            getStores()
        }
    }
    
    private func getCities() {
        Task {
            let result = await StoreRepositoty.getCities()
                .enumerated()
                .map { (index, element) in
                    DropDownList(id: index, name: element)
                }
                .sorted {
                    $0.name < $1.name
                }
            
            cities = [DropDownStruct(list: result)]
        }
    }
    
    private func getStores() {
        Task {
            let result = await StoreRepositoty.getStores(city: selectedCity?.name)
            store = Dictionary(grouping: result, by: {$0.city_id})
                .map {
                    DropDownStruct(section: $0.key, list: $0.value.enumerated().map{ (index, value) in
                        DropDownList(id: index , name: value.address, model: value)
                    })
                }
        }
    }
    
    
    @IBAction func sendFeedback(_ sender: Any) {
        if subjectCustomTextEdit.isEmpty {
            subjectCustomTextEdit.setError()
        }
        
        if textCustomTextEdit.isEmpty {
            textCustomTextEdit.setError()
        }
        
        if subjectCustomTextEdit.isEmpty || textCustomTextEdit.isEmpty {
            AudioServicesPlaySystemSound(1521)
        } else {
            newFeedbackModel.name = nameCustomTextEdit.textTrim
            newFeedbackModel.text = textCustomTextEdit.textTrim
            Task {
                HUD.show(.progress)
                let result = await FeedbackRepository.sendNewFeedback(feedback: newFeedbackModel)
                HUD.hide()
                
                switch result {
                case .success:
                    Alert.feedbcakSended()
                    setStartFields()
                    break
                case .internet:
                    Alert.noInternet()
                    break
                case .error:
                    Alert.serverError()
                    break
                default:
                    break
                }
            }
            
        }
    }
        
        //
        //
        //    private func showDatePicker() {
        //
        //        let alert = UIAlertController(title: "feedback_town".localized, message: "\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
        //
        //        let pickerFrame = UIPickerView(frame: CGRect(x: 5, y: 20, width: view.frame.width-25, height: 200))
        //        alert.view.addSubview(pickerFrame)
        //        pickerFrame.dataSource = self
        //        pickerFrame.delegate = self
        //        pickerFrame.tag = 2
        //
        //        alert.addAction(UIAlertAction(title: "next".localized, style: .default, handler: { (_) in
        //        }))
        //
        //        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: { (_) in
        //            self.townTextEdit.text = nil
        //        }))
        //
        //        self.present(alert, animated: true, completion: nil)
        //
        //        if(stores.count > 0 && townTextEdit.text?.count == 0){
        //            townTextEdit.text = stores[0].address
        //        }
        //
        //
        ////        let toolbar = UIToolbar()
        ////        toolbar.sizeToFit()
        ////
        ////        let doneButton = UIBarButtonItem(title: "next".localized, style: .plain, target: self, action: #selector(doneDatePicker))
        ////
        ////        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        ////
        ////        let cancelButton = UIBarButtonItem(title: "cancel".localized, style: .plain, target: self, action: #selector(cancelDatePicker))
        ////
        ////        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        ////        townTextEdit.inputAccessoryView = toolbar
        ////        townTextEdit.inputView = pickerView_kiosk
        ////
        ////        if(stores.count > 0){
        ////            townTextEdit.text = stores[0].address
        ////        }
        //    }
        
        //    @objc func doneDatePicker() {
        //        self.view.endEditing(true)
        //        textTextEdit.becomeFirstResponder()
        
        //    }
        
        //    @objc func cancelDatePicker() {
        //        townTextEdit.text = nil
        //        selectedKioskId = "---"
        //        self.view.endEditing(true)
        //    }
        
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            getCities()
            getStores()
        }
        
        
        ///END
    }
