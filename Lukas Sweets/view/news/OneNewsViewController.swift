//
//  OneNewsViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 22.01.2020.
//

import UIKit

final class OneNewsViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var news: NewsModel?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollViewDidScroll(scrollView)
        
        if let news = news {
            self.title = news.title
            titleLabel.text = news.title
            dateLabel.text = news.validity
            descriptionLabel.text = news.text_news
            descriptionLabel.sizeToFit()
            imageView.loadImage(fromURL: news.picture)
            
            if let button_text = news.button_text {
                if(news.button_show) {
                    actionButton.setTitle(button_text, for: .normal)
                } else {
                    actionButton.isHidden = true
                }
            } else {
                actionButton.isHidden = true
            }
        }
        
        scrollView.delegate = self
        
    }
    
    @IBAction func actionButtonURL(_ sender: Any) {
        guard let button_url = news?.button_url else {return}
        guard let url = URL(string: button_url) else {return}
        UIApplication.shared.open(url)
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       let contentYoffset = scrollView.contentOffset.y
        
        let valueAlpha = (contentYoffset-(titleLabel.bounds.maxY/2))/(titleLabel.bounds.maxY/2)
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(valueAlpha)]

   }
        
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBar()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.clear]

        
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        self.title = article.title
//
//    }
    
  
    
    
    // END
}



