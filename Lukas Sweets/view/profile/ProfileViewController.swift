//
//  ProfileViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 28.01.2020.
//

import UIKit
import AudioToolbox
import PKHUD
import SPIndicator
import SwiftMessages

final class ProfileViewController: UIViewController {
    
    @IBOutlet weak var nameCustomTextField: CustomTextField!
    @IBOutlet weak var surnameCustomTextField: CustomTextField!
    @IBOutlet weak var patronymicCustomTextField: CustomTextField!
    @IBOutlet weak var sexCustomTextField: CustomTextField!
    @IBOutlet weak var emailCustomTextField: CustomTextField!
    @IBOutlet weak var birthdayCustomTextField: CustomTextField!
    @IBOutlet weak var phoneCustomTextField: CustomTextField!

    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var sendEcoCheckSwitch: UISwitch!
    @IBOutlet weak var echoLabel: UILabel!
    @IBOutlet weak var notificationSwitch: UISwitch!
    

        
    private var sexChoiceMB = 0
    private var phone = "null"
    
    var enterEmail = false
    private var user: UserModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dismissKeyboard()
        
        setupNavigation()
        setupData()
        
        birthdayCustomTextField.view = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(selectSex))
        tap.numberOfTapsRequired = 1
        sexCustomTextField.tapGestureRecognizer = tap
        
    }
    

    private func setupNavigation() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveDataProfile))
    }
    
    private func setupData() {
        Task {
            if let userModel = await UserRepository.getUser() {
                user = userModel
                switch userModel.sex {
                case 1:
                    sexCustomTextField.text = "man".localized
                case 2:
                    sexCustomTextField.text = "woman".localized
                default:
                    sexCustomTextField.text = ""
                }

                
                nameCustomTextField.text = userModel.name
                surnameCustomTextField.text = userModel.surname ?? ""
                patronymicCustomTextField.text = userModel.patronymic ?? ""
                emailCustomTextField.text = userModel.email ?? ""
                phoneCustomTextField.text = "38\(userModel.phone)"
                birthdayCustomTextField.text = userModel.birthday?.formatDate(fromFormat: "yyyy-MM-dd", toFormat: "dd.MM.yyyy") ?? ""

//                if(userModel.send_eco_check == true){
//                    sendEcoCheckSwitch.isOn = true
//                    echoLabel.text = NSLocalizedString("ok_eco_check_text", comment: "")
//                } else {
//                    sendEcoCheckSwitch.isOn = false
//                    echoLabel.text = NSLocalizedString("no_eco_check_text", comment: "")
//                }
            } else {
                Alert.serverError()
            }
        }
    }
    
    
    @objc func selectSex() {
        sexCustomTextField.isEdit = true
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(.init(title: "man".localized, style: .default){
            action in
            self.sexCustomTextField.text = "man".localized
            self.sexCustomTextField.isEdit = false
            self.user?.sex = 1
        })
        
        alert.addAction(.init(title: "woman".localized, style: .default){
            action in
            self.sexCustomTextField.text = "woman".localized
            self.sexCustomTextField.isEdit = false
            self.user?.sex = 2
        })
        
        alert.addAction(.init(title: "other".localized, style: .destructive){
            action in
            self.sexCustomTextField.text = ""
            self.sexCustomTextField.isEdit = false
            self.user?.sex = 0
        })
        
        alert.addAction(.init(title: "cancel".localized, style: .cancel){
            action in
            self.sexCustomTextField.isEdit = false
        })
        self.present(alert, animated: true)
    }
    
    
//    @IBAction func ecoCheckAction(_ sender: UISwitch) {
//        if(!sendEcoCheckSwitch.isOn){
//            UserDefaults.standard.setValue(false, forKey: "hideEcoCheck")
//            echoLabel.text = NSLocalizedString("no_eco_check_text", comment: "")
//        } else {
//            echoLabel.text = NSLocalizedString("ok_eco_check_text", comment: "")
//        }
//
//
//    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBarLargeTitle()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(enterEmail){
            let point = CGPoint(x: 0, y: emailCustomTextField.frame.origin.y - mainScrollView.bounds.size.height/2)
            mainScrollView.setContentOffset(point, animated: true)
            user?.send_eco_check = true
            emailCustomTextField.isEdit = true
        }
    }
    
    
    
    @objc func saveDataProfile(_ sender: Any) {
        view.endEditing(true)
        
        if(enterEmail && emailCustomTextField.isEmpty || enterEmail && emailCustomTextField.isEmpty) {
            emailCustomTextField.setError()
            AudioServicesPlaySystemSound(1521)
            return
        }
                
        else {
            if nameCustomTextField.isEmpty {
                nameCustomTextField.setError()
                AudioServicesPlaySystemSound(1521)
            } else {
                HUD.show(.progress)
                user?.name = nameCustomTextField.textTrim
                user?.surname = surnameCustomTextField.isEmpty ? nil : surnameCustomTextField.textTrim
                user?.patronymic = patronymicCustomTextField.isEmpty ? nil : patronymicCustomTextField.textTrim
                user?.birthday = birthdayCustomTextField.isEmpty ? nil : birthdayCustomTextField.textTrim.formatDate(fromFormat: "dd.MM.yyyy", toFormat: "yyyy-MM-dd")
                user?.email = emailCustomTextField.isEmpty ? nil : emailCustomTextField.textTrim.replacingOccurrences(of: "\\s", with: "", options: .regularExpression)
//                user?.send_eco_check = sendEcoCheckSwitch.isOn
                
                Task {
                    if let user = user {
                        let result = await UserRepository.updateUser(userModel: user)
                        HUD.hide()
                        switch result {
                        case .success:
                            Alert.saved()
                            self.navigationController?.popViewController(animated: true)
                            break
                        default:
                            Alert.noInternet()
                            break
                        }
                    }
                }
            }
            
        }
    }
    
    
//    private func showAlertSex() {
//        let view: Sheets = try! SwiftMessages.viewFromNib()
//        view.configureDropShadow()
//        view.titleLabel?.text = "profile_alert_sex_title".localized
//        view.bodyLabel?.text = "profile_alert_sex_text".localized
//
//        view.cancelAction = {
//            SwiftMessages.hide()
//        }
//
//        view.okAction = {
//            SwiftMessages.hide()
//        }
//
//        view.okButton.setTitle("yes".localized, for: .normal)
//        view.cancelButton.setTitle("no".localized, for: .normal)
//
//
//        var config = SwiftMessages.defaultConfig
//        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
//        config.duration = .forever
//        config.presentationStyle = .bottom
//        config.dimMode = .gray(interactive: true)
//        SwiftMessages.show(config: config, view: view)
//
//    }
    
    // END
}



//
//
//extension ProfileViewController: UITextFieldDelegate {
//    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//
//    
//    func dissmissKey () {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.dissmissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//    
//    @objc func dissmissKeyboard() {
//        view.endEditing(true)
//    }
//}
//
