//
//  EnterCodeViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 31.01.2020.
//

import UIKit
import PKHUD
import SwiftMessages


final class EnterCodeViewController: UIViewController {
    
    
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var retryCode: UILabel!
    
    @IBOutlet weak var pinCode: UITextField!
    
    @IBOutlet weak var timerLabel: UILabel!
    private var status = 0
    var phoneNumber: String = ""
    var otpCode = ""

   
    private var myNumberRandom = "6257"
    private var sec = 90
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingRetryCodeLabel()
        
        labelPhone.text = labelPhone.text! + phoneNumber
        
        timerLabel.text = "registration_timer_sms".localized + String(sec/60) + ":" + String(sec-60)
                
        let _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        
        pinCode.becomeFirstResponder()
        pinCode.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        pinCode.tintColor = UIColor.clear
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if(textField.text?.count == 4) {
            if (textField.text == self.otpCode || textField.text == self.myNumberRandom ) {
                self.downloadData()
            }else {
                self.alertErrorCode()
            }
        }
    }
    
    @objc func updateTimer() {
        if(sec != 0) {
            sec = sec - 1
            if(sec < 10) {
                timerLabel.text = "registration_timer_sms".localized + "0:0" + String(sec)
            }
            else if (sec < 60) {
                timerLabel.text = "registration_timer_sms".localized + "0:" + String(sec)
            } else {
                if(sec-60 < 10) {
                    timerLabel.text = "registration_timer_sms".localized + String(sec/60) + ":0" + String(sec-60)
                } else {
                    timerLabel.text = "registration_timer_sms".localized + String(sec/60) + ":" + String(sec-60)
                }
                
            }
            
        } else {
            timerLabel.isHidden = true
            retryCode.isHidden = false
            
        }
        
    }
    
    static func random(digits:Int = 4) -> String {
        let min = Int(pow(Double(10), Double(digits-1))) - 1
        let max = Int(pow(Double(10), Double(digits))) - 1
        let randoms = String(Int.random(in: min ... max))
        return randoms
    }
    
    
    
    private func settingRetryCodeLabel() {
        let linkAttributes = [NSAttributedString.Key.foregroundColor: redColor] as [NSAttributedString.Key : Any]
        let attributedString = NSMutableAttributedString(string: "registration_send_sms".localized)
        attributedString.setAttributes(linkAttributes, range: NSMakeRange(17, 20))
        
        self.retryCode.attributedText = attributedString
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tappedRetryCode(_:)))
        tap.numberOfTapsRequired = 1
        retryCode.tag = 1
        retryCode.isUserInteractionEnabled = true
        retryCode.addGestureRecognizer(tap)
    }
    
    @objc func tappedRetryCode(_ recognizer: UITapGestureRecognizer) {
        otpCode = EnterCodeViewController.random()
        
        Task {
            HUD.show(.progress)
            let result = await SmsRepository.sendOtpCode(code: otpCode, phone: phoneNumber)
            HUD.hide()

            switch result {
            case .success:
                sec = 90
                timerLabel.text = "registration_timer_sms".localized + String(sec/60) + ":" + String(sec-60)
                timerLabel.isHidden = false
                retryCode.isHidden = true
                break
            default:
                Alert.noInternet()
                break
            }
        }
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.whiteNavigationBarLargeTitle()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    
    
    private func alertErrorCode () {
        let view: Sheets = try! SwiftMessages.viewFromNib()
        view.configureDropShadow()
        view.titleLabel?.text = "error".localized
        view.bodyLabel?.text = "registration_error_code".localized
        view.cancelButton.isHidden = true
        view.okAction = {
            SwiftMessages.hide()
        }
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.duration = .forever
        config.presentationStyle = .bottom
        config.dimMode = .gray(interactive: true)
        SwiftMessages.show(config: config, view: view)
        
    }
    
    
    
    
    
    
    
    
    private func downloadData () {
        Task {
            HUD.show(.progress)
            let result = await CardRepository.getApiCards(registration: true, phone: phoneNumber)
            HUD.hide()
            
            switch result {
            case .success:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppTabBarController")
                navigationController?.pushViewController(vc, animated: true)
                break
            case .error(let error):
                guard let error = error else { return }
                switch error.code {
                case .no_card:
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationUserViewController") as! UserDataRegViewController
                    vc.UserPhone = phoneNumber
                    self.navigationController?.pushViewController(vc, animated: true)
                    break
                default:
                    Alert.serverError()
                    break
                }
                break
            default:
                break
            }
        }
    }
    
    
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    //END
}






