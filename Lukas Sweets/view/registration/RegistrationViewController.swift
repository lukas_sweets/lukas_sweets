//
//  RegistrationViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 28.01.2020.
//

import UIKit
import PKHUD

final class RegistrationViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var labelPolicy: UILabel!
    private var nextButton = UIButton()
    private var configButton = UIButton.Configuration.filled()
    private var otpCode = ""
    private var isEdit = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingButtonNext()
        settingPolicyLabel()
        
        numberTextField.delegate = self
        numberTextField.tintColor = redColor
        
        numberTextField.addTarget(self, action: #selector(buttonState), for: .editingChanged)
        
    }
    
    @objc private func buttonState() {
        if deleteFormatPhone(phone: numberTextField.text!).count == 10 {
            buttonColor(active: true, loaded: false)
        } else {
            buttonColor(active: false, loaded: false)
        }
    }
    
    
    @objc func doubleTapped(_ recognizer: UITapGestureRecognizer) {
        guard let url = URL(string: "https://lukas.ua/wp-content/uploads/2020/07/fr_bp.pdf") else {return}
        UIApplication.shared.open(url)
    }
    
    
    private func deleteFormatPhone(phone: String?) -> String {
        if var number = phone  {
            let removeCharacters: Set<Character> = ["(", ")", " ", "-"]
            number.removeAll(where: {removeCharacters.contains($0)})
            return number
        } else {
            return ""
        }
    }
    
    
    
    private func settingButtonNext() {
        configButton = UIButton.Configuration.filled()
        configButton.title = "next".localized
        configButton.baseBackgroundColor = redColor
        configButton.cornerStyle = .large
        configButton.baseForegroundColor = .white
        configButton.showsActivityIndicator = false
        
        nextButton.addTarget(self, action: #selector(self.nextsButton), for: .touchUpInside)
        nextButton.accessibilityIdentifier = "nextButton"
        nextButton.configuration = configButton
        nextButton.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50)
        nextButton.setNeedsUpdateConfiguration()
        numberTextField.inputAccessoryView = nextButton
    }
    
    private func settingPolicyLabel() {
        let linkAttributes = [NSAttributedString.Key.foregroundColor: redColor] as [NSAttributedString.Key : Any]
        
        let attributedString = NSMutableAttributedString(string: "registration_rules".localized)
        attributedString.setAttributes(linkAttributes, range: NSMakeRange(9, 10))
        
        self.labelPolicy.attributedText = attributedString
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.doubleTapped(_:)))
        tap.numberOfTapsRequired = 1
        labelPolicy.tag = 1
        labelPolicy.isUserInteractionEnabled = true
        labelPolicy.addGestureRecognizer(tap)
    }
    
    
    private func buttonColor(active: Bool, loaded: Bool) {
        nextButton.configurationUpdateHandler = { [unowned self] button in
            var config = button.configuration
            if active {
                config?.showsActivityIndicator = false
                config?.baseBackgroundColor = redColor
                config?.title = "next".localized
                nextButton.isEnabled = true
                isEdit = true
            } else {
                if loaded {
                    config?.showsActivityIndicator = true
                    config?.title = ""
                    isEdit = false
                } else {
                    config?.title = "next".localized
                    isEdit = true
                }
                config?.baseBackgroundColor = .lightGray
                nextButton.isEnabled = false
            }
            nextButton.configuration = config
        }

        nextButton.setNeedsUpdateConfiguration()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.whiteNavigationBar()
        numberTextField.becomeFirstResponder()
        buttonState()
    }
    
    
    
    @objc func nextsButton()
    {
        let phone = deleteFormatPhone(phone: numberTextField.text)
        
        buttonColor(active: false, loaded: true)
        
                    Task {
                        otpCode = EnterCodeViewController.random()
                        let result = await SmsRepository.sendOtpCode(code: otpCode, phone: phone)
        
                        switch result {
                        case .success:
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterCodeViewController") as! EnterCodeViewController
                            vc.phoneNumber = phone
                            vc.otpCode = otpCode
                            self.navigationController?.pushViewController(vc, animated: true)
                            break
                        default:
                            Alert.noInternet()
                            buttonState()
                        }
                    }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
}


extension RegistrationViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !isEdit {
            return false
        }
        
        let textCount = deleteFormatPhone(phone: textField.text).count
        
        let char = string.cString(using: String.Encoding.utf8)!
        
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92){
            if range.location == 11 || range.location == 8 || range.location == 4 {
                textField.text?.removeLast(2)
            }
            else {
                if(textField.text!.count > 0){
                    textField.text?.removeLast()
                }
                
            }
            
            buttonState()
            
            return false
        }
        
        if textCount == 3  {
            if Array(textField.text!)[0] != " " {
                textField.text = "\(textField.text!) "
            }
            else {
                textField.text = "\(textField.text!) "
            }
        }
        else if textCount == 6 {
            textField.text =  "\(textField.text!) "
        }
        else if textCount == 8 {
            textField.text =  "\(textField.text!) "
        }
        else if textCount == 10{
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func dissmissKey () {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistrationViewController.dissmissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dissmissKeyboard() {
        view.endEditing(true)
    }
    
    
    
    
    
    
}
