//
//  UserDataRegViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 31.01.2020.
//

import UIKit
import AudioToolbox
import PKHUD


final class UserDataRegViewController: UIViewController {
    
    var UserPhone: String = ""
    @IBOutlet weak var userName: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dismissKeyboard()
        userName.startEdit()
        navigationController?.viewControllers.remove(at: 2)
        Log.e(navigationController?.viewControllers.count)
        settingButtonNext()
    }
    
    private func settingButtonNext() {
        var configButton = UIButton.Configuration.filled()
        configButton.title = "save".localized
        configButton.baseBackgroundColor = .red
        configButton.cornerStyle = .large
        configButton.baseForegroundColor = .white
        configButton.showsActivityIndicator = false
        
        let nextButton = UIButton()
        nextButton.addTarget(self, action: #selector(saveData), for: .touchUpInside)
        nextButton.accessibilityIdentifier = "nextButton"
        nextButton.configuration = configButton
        nextButton.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50)
        nextButton.setNeedsUpdateConfiguration()
        userName.itemField.inputAccessoryView = nextButton
        
    }
    
    @objc private func saveData(_ sender: Any) {
        view.endEditing(true)
        if userName.isEmpty {
            AudioServicesPlaySystemSound(1521)
            userName.setError()
        } else {
            Task {
                HUD.show(.progress)
                let resultApi = await CardRepository.registrationBonusCard(phone: UserPhone, name: userName.textTrim)
                HUD.hide()
                
                switch resultApi {
                case .success:
                    let vc = UIStoryboard(name: "Storyboard", bundle: nil).instantiateViewController(withIdentifier: "LaunchViewController") as! LaunchViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                    break
                case .internet:
                    Alert.noInternet()
                default:
                    Alert.serverError()
                    break
                }
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBarLargeTitle()
    }
    
    
    
    
    
//
//
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .default
//    }
    
    //END
}
//
//extension UserDataRegViewController: UITextFieldDelegate {
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
//
//    func dissmissKey () {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserDataRegViewController.dissmissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//
//    @objc func dissmissKeyboard() {
//        view.endEditing(true)
//    }
//}
