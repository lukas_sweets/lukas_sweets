//
//  CardView.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 21.03.2023.
//

import UIKit
import ScrollCounter




class CardView: UIView {
   
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var checksButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    private var numberScrollCounter: NumberScrollCounter?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CardView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        balanceLabel.text = "₴ \(Preference.instance.bonus_card_balance)"
        balanceLabel.adjustsFontSizeToFit(maxFontSize: 200, width: balanceLabel.frame.width, height: balanceLabel.frame.height)
        
        numberScrollCounter = NumberScrollCounter(value: Float(Preference.instance.bonus_card_balance), scrollDuration: 0.5, decimalPlaces: 0, prefix: "₴", suffix: "", font: UIFont.boldSystemFont(ofSize: balanceLabel.font.pointSize-5), textColor: .black, gradientColor: .black, gradientStop: 0.0)
        if let numberScrollCounter = numberScrollCounter {
            balanceLabel.isHidden = true
            stackView.addArrangedSubview(numberScrollCounter)
        }
    }
    
    private var balanceInt = 0
    var balance: Int {
        get {
            return balanceInt
        }
        set(newValue) {
            balanceInt = newValue
            balanceLabel.text = "₴ \(newValue)"
            numberScrollCounter?.setValue(Float(balanceInt))
            Preference.instance.bonus_card_balance = newValue
        }
    }
    
    
//    @IBAction func navigateToChecks(_ sender: UIButton) {
//        self.performSegue(withIdentifier: "goRegister", sender: self)
//    }
    
    
}



