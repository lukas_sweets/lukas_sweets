//
//  FirstViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 11.12.2019.
//

import UIKit
//import SQLite3
import SwiftMessages
import PKHUD
//import UserNotifications

class Main: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var loadNews: UIActivityIndicatorView!
    @IBOutlet weak var bannersPageControl: UIPageControl!
    @IBOutlet weak var inetKioskView: UIView!
    @IBOutlet weak var loyalInfoView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var noSignalView: UIStackView!
    @IBOutlet weak var stocksScrollView: UIScrollView!
    @IBOutlet weak var mainEcoCheckView: UIView!
    @IBOutlet weak var okEcoCheckView: UIView!
    @IBOutlet weak var ecoView: UIView!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var loyalView: UIView!
    @IBOutlet weak var cakeWeekScrollView: UIScrollView!
    @IBOutlet weak var cakeWeekPageControl: UIPageControl!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var leftBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var luckyWheelView: UIView!
    
    
    
//    private var dataNews: [News]?
//    private var tapGesture = UITapGestureRecognizer()
//    private var cardTapGesture = UITapGestureRecognizer()
//    private var inetKioskTapGesture = UITapGestureRecognizer()
//    private var loyalInfoTapGesture = UITapGestureRecognizer()
//    private var luckyWeelTapGesture = UITapGestureRecognizer()
    
    private let refreshControl = UIRefreshControl()
    private var getTapScrollView: Bool = false
    private var arrayImageString: Array<String> = []
    
//    private let db:DBHelper = DBHelper()
//    private var dataCard: [Card]?
    
//    private var cakeWeek: [Stock]?
    
    private var stocks: ([StockModel], [StockModel]) = ([], [])
    private var newsData: [NewsModel] = []
    
    
    private var tagStocks: Int = -1
    private var typeStock = 0
    private let CLICK_DOWN_STOCK = 1
    private let CLICK_TOP_STOCK = 2
    
    private var flipCardiew = MainFlipCardView()
    
    var timer : Timer? = nil {
        willSet {
            timer?.invalidate()
        }
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        noSignalView.visibility = .gone
        bannersPageControl.isHidden = true
        noSignalView.isHidden = true
        loadNews.startAnimating()
        
        
        getStocks()
        getNews()
        getBonusCard()
        
        pullToRefresh()
        
        
        bannerScrollView.delegate = self
        cakeWeekScrollView.delegate = self
        
        //Scroll banner
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(myviewTapped))
        tapGesture.numberOfTapsRequired = 1
        bannerScrollView.isUserInteractionEnabled = true
        bannerScrollView.addGestureRecognizer(tapGesture)
        
        
        let inetKioskTapGesture = UITapGestureRecognizer(target: self, action: #selector(inetKioskTapped(_:)))
        inetKioskView.addGestureRecognizer(inetKioskTapGesture)
        
        let loyalInfoTapGesture = UITapGestureRecognizer(target: self, action: #selector(loyalInfoTapped(_:)))
        loyalInfoView.addGestureRecognizer(loyalInfoTapGesture)
        
        setupLuckyWheelView()
        
        
        
        // Setup Left button on Navigation bar
        leftBarButtonItem.action = #selector(openStores)
        leftBarButtonItem.target = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        setupLoyalView()
    }
    
    @objc func willEnterForeground() {
        getAllData()
    }
    
    private func setupLoyalView() {
        loyalView.subviews.forEach({$0.removeFromSuperview()})
        flipCardiew = MainFlipCardView()
        loyalView.addSubview(flipCardiew)
        flipCardiew.frame = loyalView.bounds
        flipCardiew.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(info))
        tap.numberOfTapsRequired = 1
        flipCardiew.addGestureRecognizer(tap)
        
    }
    
    private func setupLuckyWheelView() {
        luckyWheelView.layer.cornerRadius = 10
        luckyWheelView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        let luckyWeelTapGesture = UITapGestureRecognizer(target: self, action: #selector(luckyWeelTapped(_:)))
        luckyWheelView.addGestureRecognizer(luckyWeelTapGesture)
    }
    
    @objc private func info() {
        if flipCardiew.purchaseAmount ?? 0 < 2000 && flipCardiew.loyalLevel < 5 {
            flipCardiew.mainView.isHidden = !flipCardiew.mainView.isHidden
            flipCardiew.detailView.isHidden = !flipCardiew.detailView.isHidden
            UIView.transition(with: loyalView, duration: 0.3, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        }
    }
    
    // Tap to banner scroll view
    @objc private func myviewTapped(_ sender: UITapGestureRecognizer) {
        if (getTapScrollView == true && !newsData.isEmpty){
            let vc = UIStoryboard(name: "News", bundle: nil).instantiateViewController(withIdentifier: "NewsViewController") as! OneNewsViewController
            vc.news = newsData[bannersPageControl.currentPage]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @objc private func inetKioskTapped(_ sender: UITapGestureRecognizer) {
        guard let url = URL(string: "https://lukas-sweet.shop") else {return}
        UIApplication.shared.open(url)
    }
    
    @objc private func loyalInfoTapped(_ sender: UITapGestureRecognizer) {
        let vc = UIStoryboard(name: "Loyal", bundle: nil).instantiateViewController(withIdentifier: "LoyalInfoViewController") as! LoyalInfoViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func stockTapped(_ sender: UITapGestureRecognizer) {
        if let view = sender.view {
            tagStocks = view.tag
            let storyboard = UIStoryboard(name: "Stock", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "StockViewController") as! StockViewController
            vc.stockID = view.tag
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc private func openStores() {
        let storyboard = UIStoryboard(name: "Store", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StoresViewController") as! StoresViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction private func detailForestButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "showForest", sender: self)
    }
    
    
    @objc private func luckyWeelTapped(_ sender: UITapGestureRecognizer) {
        let vc = UIStoryboard(name: "LuckyWheel", bundle: nil).instantiateViewController(withIdentifier: "LuckyWeelViewController")
        navigationController?.pushViewController(vc, animated: true)
//        performSegue(withIdentifier: "goLuckyWeel", sender: self)
    }
    
    
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        default:
            break
        }
    }
    
    
    
    
    
    /*
     
     MARK: - Eco Check
     
     */
    
    private func ecoCheck(){
        Task {
            if Preference.show_eco_check {
                let user = await UserRepository.getUser()
                
                if user?.send_eco_check ?? false {
                    okEcoCheckView.isHidden = false
                    mainEcoCheckView.isHidden = true
                } else {
                    okEcoCheckView.isHidden = true
                    mainEcoCheckView.isHidden = false
                }
                
            } else {
                ecoView.isHidden = true
            }
        }
    }
    
    
    @IBAction func hideEcoCheck(_ sender: UIButton) {
        self.ecoView.isHidden = true
        Preference.show_eco_check = false
    }
    
    
    
    @IBAction func dismissForestButtonAction(_ sender: UIButton) {
        let view: Sheets = try! SwiftMessages.viewFromNib()
        view.configureDropShadow()
        view.titleLabel?.text = NSLocalizedString("sad_eco_check", comment: "")
        view.bodyLabel?.text = NSLocalizedString("sad_eco_check_text", comment: "")
        view.raccoon.visibility = .gone
        view.cancelButton.isHidden = true
        view.cancelAction = { SwiftMessages.hide() }
        view.okAction = { SwiftMessages.hide() }
        SwiftMessages.defaultConfig.preferredStatusBarStyle = .lightContent
        SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.defaultConfig.duration = .forever
        SwiftMessages.defaultConfig.presentationStyle = .bottom
        SwiftMessages.defaultConfig.dimMode = .gray(interactive: true)
        SwiftMessages.show(view: view)
        
        Preference.show_eco_check = false
        ecoView.isHidden = true
    }
    
    
    
    
    @IBAction func confirmEcoCheckButtonAction(_ sender: UIButton) {
        Task {
            HUD.show(.progress)
            let user = await UserRepository.getUser()
            HUD.hide()
            
            if (user?.email ?? "").isEmpty {
                let view: Sheets = try! SwiftMessages.viewFromNib()
                view.configureDropShadow()
                view.titleLabel?.text = NSLocalizedString("good_eco_check", comment: "")
                view.bodyLabel?.text = NSLocalizedString("no_email_eco_check_text", comment: "")
                view.raccoon.visibility = .gone
                view.cancelButton.isHidden = false
                view.cancelAction = { SwiftMessages.hide() }
                
                view.okAction = {
                    SwiftMessages.hide()
                    let vc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    vc.enterEmail = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                view.cancelButton.setTitle("cancel".localized, for: .normal)
                view.okButton.setTitle("settings", for: .normal)
                
                SwiftMessages.defaultConfig.preferredStatusBarStyle = .default
                SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
                SwiftMessages.defaultConfig.duration = .forever
                SwiftMessages.defaultConfig.presentationStyle = .bottom
                SwiftMessages.defaultConfig.dimMode = .gray(interactive: true)
                SwiftMessages.show(view: view)
            } else {
                let view: Sheets = try! SwiftMessages.viewFromNib()
                view.configureDropShadow()
                view.titleLabel?.text = NSLocalizedString("good_eco_check", comment: "")
                view.bodyLabel?.text = NSLocalizedString("email_eco_check_text", comment: "") + "\n\n" + (user?.email ?? "")
                view.raccoon.visibility = .gone
                view.cancelButton.isHidden = true
                view.cancelAction = { SwiftMessages.hide() }
                
                view.okAction = {
                    SwiftMessages.hide()
                    Task {
                        HUD.show(.progress)
                        let result = await UserRepository.updateUserEcoCheck(send: true)
                        HUD.hide()
                        
                        switch result {
                        case .internet:
                            Alert.noInternet()
                            break
                        case .error:
                            Alert.serverError()
                            break
                        default:
                            break
                        }
                    }
                }
                SwiftMessages.defaultConfig.preferredStatusBarStyle = .lightContent
                SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
                SwiftMessages.defaultConfig.duration = .forever
                SwiftMessages.defaultConfig.presentationStyle = .bottom
                SwiftMessages.defaultConfig.dimMode = .gray(interactive: true)
                SwiftMessages.show(view: view)
            }
        }
        
    
    }
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     
     MARK: - Загрузка Новостей
     
     */
    
    private func getNews() {
        Task {
            getTapScrollView = false
            let newNewsData = await NewsRepository.getNews()
            
            if newNewsData.isEmpty && newsData.isEmpty {
                loadNews.isHidden = true
                loadNews.stopAnimating()
                noSignalView.isHidden = false
                bannerView.isHidden = true
            } else if !newNewsData.isEmpty {
                newsData = newNewsData
                setNews(news: newsData)
                
            }
        }
    }
    
    private func setNews(news: [NewsModel]) {
        var contentWidth:CGFloat = 0.0
        bannerScrollView.subviews.forEach({($0).removeFromSuperview()})
        bannersPageControl.numberOfPages = news.count
        if news.count > 0 { getTapScrollView = true }
        
        for (index, newsModel) in news.enumerated() {
            let imageView1 = UIImageView()
            imageView1.loadImage(fromURL: newsModel.picture)
            
            let views = UIView()
            views.backgroundColor = .white
            
            views.addSubview(imageView1)
            bannerScrollView.addSubview(views)
            
            
            views.frame = CGRect(x: bannerScrollView.frame.width * CGFloat(index) + 20, y: 0, width: bannerScrollView.bounds.width - 40, height: bannerScrollView.frame.height)
            
            imageView1.frame = CGRect(x: 0, y: 0, width: bannerScrollView.bounds.width - 40, height:
                                        bannerScrollView.frame.height)
            
            imageView1.cornerRadius = 15
            imageView1.contentMode = .scaleAspectFill
            imageView1.borderWidth = 1
            imageView1.borderColor = systemGray6
            
            
            contentWidth += bannerScrollView.frame.width
            
            bannersPageControl.currentPage = 0
            bannerScrollView.contentSize = CGSize(width: contentWidth, height: bannerScrollView.bounds.height)
        }
        
        loadNews.stopAnimating()
        loadNews.isHidden = true
        noSignalView.isHidden = true
        bannerView.isHidden = false
    }
    
    
    
    
    
    
    
    
    /*
     MARK: - Загрузка акций
     */
    
    
    
    private func getStocks() {
        Task {
            stocks = await StockRepository.getStocks()
            setTopPositionStocks(stocks: stocks.0)
            setDownPositionStocks(stocks: stocks.1)
        }
    }
    
    private func setTopPositionStocks(stocks: [StockModel]) {
        
        cakeWeekPageControl.numberOfPages = stocks.count
        cakeWeekScrollView.subviews.forEach({($0).removeFromSuperview()})
        cakeWeekScrollView.backgroundColor = colorsArrayWeekCake[0]
        var contentWidthCard: CGFloat = 0.0
        
        if stocks.isEmpty {
            self.cakeWeekScrollView.subviews.forEach({($0).removeFromSuperview()})
            
            let view_stock = CakeWeek()
            cakeWeekScrollView.addSubview(view_stock)
            view_stock.frame = CGRect(x: 0, y: 0, width: cakeWeekScrollView.frame.width, height: cakeWeekScrollView.frame.height)
            view_stock.nameLabel.text = nil
            view_stock.label.text = "no_data".localized.uppercased()
            view_stock.img.image = UIImage(named: "mockup_product")
            view_stock.img.tintColor = .mockup
            
            view_stock.bgMockupImage.isHidden = false
            view_stock.bgMockupImage.alpha = 0.4
            
            cakeWeekScrollView.contentSize = CGSize(width: self.cakeWeekScrollView.frame.width, height: self.cakeWeekScrollView.bounds.height)
        } else {
            for (index, stock) in stocks.enumerated() {
                let view_stock = CakeWeek()
                self.cakeWeekScrollView.addSubview(view_stock)
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.stockTapped(_:)))
                tap.numberOfTapsRequired = 1
                view_stock.addGestureRecognizer(tap)
                view_stock.tag = stock.id
                
                //                view_stock.frame = CGRect(x: self.cakeWeekScrollView.frame.width * CGFloat(index)+10, y: 0, width: width, height: height+40)
                view_stock.frame = CGRect(x: cakeWeekScrollView.frame.width * CGFloat(index), y: 0, width: cakeWeekScrollView.frame.width, height: cakeWeekScrollView.frame.height)
                
                var name = ""
                for _ in 0...30 {
                    name += stock.short_name + " "
                }
                view_stock.nameLabel.text = name
                view_stock.img.loadImage(fromURL: stock.picture)
                view_stock.label.text = stock.label_text?.uppercased()
                
                contentWidthCard += self.cakeWeekScrollView.frame.width
                self.cakeWeekScrollView.contentSize = CGSize(width: contentWidthCard, height: self.cakeWeekScrollView.bounds.height)
            }
        }
    }
    
    private func setDownPositionStocks(stocks: [StockModel]) {
        stocksScrollView.subviews.forEach({($0).removeFromSuperview()})
        
        if stocks.isEmpty {
            inetKioskView.isHidden = false
            stocksScrollView.isHidden = true
            return
        } else {
            inetKioskView.isHidden = true
            stocksScrollView.isHidden = false
        }
        
        var contentWidthCard: CGFloat = 0.0
        let height = self.stocksScrollView.frame.height
        let width = self.stocksScrollView.frame.width * 0.6
        
        for (index, stock) in stocks.enumerated() {
            let view_stock = StockView()
            self.stocksScrollView.addSubview(view_stock)
            
            view_stock.borderWidth = 1
            view_stock.borderColor = systemGray6
            view_stock.cornerRadius = 10
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.stockTapped(_:)))
            tap.numberOfTapsRequired = 1
            view_stock.addGestureRecognizer(tap)
            view_stock.tag = stock.id
            
            
            if index == 0 {
                view_stock.frame = CGRect(x: 20, y: 0, width: width, height: height)
                view_stock.backgroundColor = .clear
                contentWidthCard += width + 40
            } else {
                view_stock.frame = CGRect(x: contentWidthCard , y: 0, width: width, height: height)
                view_stock.backgroundColor = .clear
                contentWidthCard += width + 20
            }
            
            
            if let text = stock.label_text, let textColor = stock.label_color , let bgColor = stock.label_bg_color {
                if (!text.isEmpty) {
                    view_stock.labelView.layer.cornerRadius = 10
                    view_stock.labelView.layer.maskedCorners = [.layerMinXMaxYCorner]
                    view_stock.labelView.backgroundColor = UIColor(hex: bgColor )
                    view_stock.labelText.text = text
                    view_stock.labelText.textColor = UIColor(hex: textColor )
                    view_stock.labelView.isHidden = false
                } else {
                    view_stock.labelView.isHidden = true
                }
            } else {
                view_stock.labelView.isHidden = true
            }
            
            
            view_stock.nameLabel.text = stock.name
            view_stock.img.loadImage(fromURL: stock.picture)
            
            if let new_price = stock.new_price {
                view_stock.newPriceLabel.text = new_price + " ₴"
            } else {
                view_stock.newPriceLabel.isHidden = true
            }
            
            if let old_price = stock.old_price {
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: old_price + " ₴")
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                view_stock.oldPriceLabel.attributedText = attributeString
            } else {
                view_stock.oldPriceLabel.isHidden = true
            }
            
            self.stocksScrollView.contentSize = CGSize(width: contentWidthCard, height: self.stocksScrollView.bounds.height)
        }
        
    }
    
    
    
    /*
     
     MARK: - PULL TO REFRESH
     
     */
    
    private func pullToRefresh () {
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.refreshControl.tintColor = .red
        self.refreshControl.layer.zPosition += 1
        self.mainScrollView.addSubview(refreshControl)
        
    }
    
    
    @objc private func refreshData (refreshControl: UIRefreshControl) {
        getAllData()
    }
    
    func getAllData() {
        getNews()
        getStocks()
        getBonusCard()
        ecoCheck()
        updateFeedback()
    }
    
    
    
    //Page Control
    internal func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == bannerScrollView {
            bannersPageControl.currentPage = Int(scrollView.contentOffset.x / CGFloat(scrollView.bounds.width-40))
        }
        else if scrollView == cakeWeekScrollView {
            let offset = scrollView.contentOffset.x
            let lowerIndex = Int(floor(offset / scrollView.frame.width))
            let upperIndex = Int(ceil(offset / scrollView.frame.width))
            guard lowerIndex >= 0, upperIndex <= colorsArrayWeekCake.count - 1 else { return }
            let percentComplete = (offset - CGFloat(lowerIndex) * scrollView.frame.width) / scrollView.frame.width
            scrollView.backgroundColor = fadeFromColor(fromColor: colorsArrayWeekCake[lowerIndex], toColor: colorsArrayWeekCake[upperIndex], withPercentage: percentComplete
            )
            cakeWeekPageControl.currentPage = Int(scrollView.contentOffset.x / CGFloat(scrollView.bounds.width-40))
        }
    }
    
    func fadeFromColor(fromColor: UIColor, toColor: UIColor, withPercentage: CGFloat) -> UIColor {
        var fromRed: CGFloat = 0.0
        var fromGreen: CGFloat = 0.0
        var fromBlue: CGFloat = 0.0
        var fromAlpha: CGFloat = 0.0
        
        fromColor.getRed(&fromRed, green: &fromGreen, blue: &fromBlue, alpha: &fromAlpha)
        
        var toRed: CGFloat = 0.0
        var toGreen: CGFloat = 0.0
        var toBlue: CGFloat = 0.0
        var toAlpha: CGFloat = 0.0
        
        toColor.getRed(&toRed, green: &toGreen, blue: &toBlue, alpha: &toAlpha)
        
        // calculate the actual RGBA values of the fade colour
        let red = (toRed - fromRed) * withPercentage + fromRed
        let green = (toGreen - fromGreen) * withPercentage + fromGreen
        let blue = (toBlue - fromBlue) * withPercentage + fromBlue
        let alpha = (toAlpha - fromAlpha) * withPercentage + fromAlpha
        
        // return the fade colour
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    
    
    //Slide to next page banners
    @objc private func moveToNextPage() {
        let pageWidth: CGFloat = self.bannerScrollView.frame.width
        let maxWidth:CGFloat = pageWidth * CGFloat((arrayImageString.count))
        let contentOffset:CGFloat = self.bannerScrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if contentOffset + pageWidth == maxWidth {
            slideToX = 0
        }
        self.bannerScrollView.scrollRectToVisible(CGRect(x: slideToX, y: 0, width: pageWidth, height:  self.bannerScrollView.frame.height), animated: true)
    }
    
    
    
    
    
    
    
    
    
    /*
     
     MARK: - Настройка карт
     
     */
    
    private func getBonusCard() {
        Task {
            switch await CardRepository.getBonusCard() {
            case .success(let cardModel):
                setBonusCard(cardModel: cardModel)
                break
            case .internet(let data):
                guard let data = data else { return }
                setBonusCard(cardModel: data)
                Alert.noInternet()
                break
            case .error:
                RealmDatabase.shared.deleteAll()
                Preference.instance.deleteAll()
                let vc = UIStoryboard(name: "Registration", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController")
                navigationController?.pushViewController(vc, animated: true)
                break
            }
            refreshControl.endRefreshing()
        }
    }
    
    private func setBonusCard(cardModel: CardModel) {
        if let balance = cardModel.balance {
            cardView.balance = balance
        }
        
        flipCardiew.purchaseAmount = cardModel.purchase_amount
        flipCardiew.loyalLevel = cardModel.loyal_size

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(navigateChecks))
        tap.numberOfTapsRequired = 1
        cardView.checksButton.addGestureRecognizer(tap)
        
        if cardModel.purchase_amount ?? 0 > 2000 && cardModel.loyal_size > 3 {
            flipCardiew.infoImageView.isHidden = true
        } else {
            flipCardiew.infoImageView.isHidden = false
        }
    }
    
    @objc private func navigateChecks() {
        let vc = UIStoryboard(name: "Checks", bundle: nil).instantiateViewController(withIdentifier: "CheckViewController") as! HistoryViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    
    public func newYear() -> Bool {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let startDate = dateFormatter.date(from: "20.12.2021") as NSDate?
        let endDate = dateFormatter.date(from: "14.01.2022") as NSDate?
        
        return NSDate().isBetween(date: startDate!, andDate: endDate!)
        
    }
    
    
    
    
    /*
     
     MARK: - Проверка отзывов
     
     */
    
    
    private func checkNewFeedback() {
        Task {
            let newMessages = await FeedbackRepository.getNewMessages()
            if(newMessages > 0) {
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[4]
                    tabItem.badgeValue = String(newMessages)
                }
            } else {
                if let tabItems = self.tabBarController?.tabBar.items {
                    let tabItem = tabItems[4]
                    tabItem.badgeValue = nil
                }
            }
        }
    }
    
    private func updateFeedback() {
        Task {
            _ = await FeedbackRepository.getApiFeedbacks()
            checkNewFeedback()
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        
        ecoCheck()
        updateFeedback()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

            NotificationCenter.default.removeObserver(self)
    } 
    
    
}






