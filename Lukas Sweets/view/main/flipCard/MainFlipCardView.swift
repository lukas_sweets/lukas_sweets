//
//  MainFlipCardView.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 06.04.2023.
//

import UIKit
import ALProgressView


class MainFlipCardView: UIView {
    
    @IBOutlet weak var loyalPercentLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var infoImageView: UIImageView!
    
    override init(frame: CGRect){
            super.init(frame: frame)
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)

//            fatalError("init(coder:) has not been implemented")
        }

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    var purchaseAmount: Double? = 0 {
        didSet {
            progressRing.setProgress(Float((purchaseAmount ?? 0 * 100) / 2000), animated: true)
            amount.text = purchaseAmount?.clean
        }
    }
    
    var loyalLevel: Int = 3 {
        didSet {
            loyalPercentLabel.text = "\(loyalLevel)%"
        }
    }
    
    private lazy var progressRing = ALProgressRing()
    private let amount = UILabel()

    
    private func setup() {
        Bundle.main.loadNibNamed("MainFlipCardView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        setupProgressRing()

    }
    
    private func setupProgressRing() {
        detailView.addSubview(progressRing)

        progressRing.translatesAutoresizingMaskIntoConstraints = false
        progressRing.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30).isActive = true
        progressRing.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        progressRing.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        progressRing.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
        
        progressRing.startAngle = (5 * .pi) / 6 //-.pi / 2 // The start angle of the ring to begin drawing.
        progressRing.endAngle = .pi / 6 // The end angle of the ring to end drawing.
        progressRing.startColor = .white
        progressRing.endColor = .white
        progressRing.grooveColor = .systemGray3
        
        detailView.addSubview(amount)
        
        amount.translatesAutoresizingMaskIntoConstraints = false
        amount.centerXAnchor.constraint(equalTo: progressRing.centerXAnchor).isActive = true
        amount.centerYAnchor.constraint(equalTo: progressRing.centerYAnchor).isActive = true
        amount.text = purchaseAmount?.clean
        amount.textColor = .white
        amount.font = amount.font.withSize(15)
    }

    
}
