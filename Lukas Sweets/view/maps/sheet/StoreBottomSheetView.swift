//
//  BottomSheetView.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 04.04.2023.
//

import UIKit
import MapKit


class StoreBottomSheetView: UIViewController {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timeWorkLabel: UILabel!
    @IBOutlet weak var phoneView: RoundIcon!
    @IBOutlet weak var routeView: RoundIcon!
    
    var store: StoreModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStore()
    }
    
    private func setStore() {
        if let addressLabel = addressLabel {
            if let store = store {
                print(store.address)
                addressLabel.text = store.address
                timeWorkLabel.text = store.time_work
                let tapPhone = UITapGestureRecognizer(target: self, action: #selector(callStore))
                tapPhone.numberOfTapsRequired = 1
                phoneView.addGestureRecognizer(tapPhone)
                
                let tapRoute = UITapGestureRecognizer(target: self, action: #selector(routeStore))
                tapRoute.numberOfTapsRequired = 1
                routeView.addGestureRecognizer(tapRoute)
            }
        }
    }
    
    @objc private func callStore() {
        if let phone = store?.phone_number {
            guard let number = URL(string: "tel://"+phone) else { return }
            UIApplication.shared.open(number)
        }
    }
    
    @objc private func routeStore() {
        if let lat = store?.lat, let lon = store?.lon {
            let isAvailableGoogleMaps = UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)
            
            let isAvailableWaze = UIApplication.shared.canOpenURL(URL(string: "waze://")!)
            
            if(isAvailableGoogleMaps == true || isAvailableWaze == true){
                
                let alert = UIAlertController(title: "maps_go_title".localized, message: "maps_go_text".localized, preferredStyle: .actionSheet)
                
                if(isAvailableGoogleMaps){
                    alert.addAction(UIAlertAction(title: "Google Maps", style: .default, handler: { (_) in
                        self.openGoogleDirectionMap(lat: lat, lon: lon)
                    }))
                }
                
                if(isAvailableWaze){
                    alert.addAction(UIAlertAction(title: "Waze", style: .default, handler: { (_) in
                        self.openWazeMap(lat: lat, lon: lon)
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Apple Maps", style: .default, handler: { (_) in
                    self.openMapForPlace(lat: lat, lon: lon)
                }))
                
                alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: { (_) in
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                self.openMapForPlace(lat: lat, lon: lon)
            }
        }
    }
    
    //START MAPS GOOGLE
    private func openGoogleDirectionMap(lat: String, lon: String) {
        guard let _ = store else {return}
        let tempURL = URL(string: "comgooglemaps://?saddr=&daddr=\(lat),\(lon)&directionsmode=driving")
        UIApplication.shared.open(tempURL!, options: [:], completionHandler: nil)
        
        
    }
    
    
    //START WAZE
    private func openWazeMap(lat: String, lon: String) {
        guard let _ = store else {return}
        let urlStr: String = "waze://?ll=\(lat),\(lon)&navigate=yes"
        UIApplication.shared.open(URL(string: urlStr)!)
    }
    
    
    
    
    //START MAPS APPLE
    private func openMapForPlace(lat: String, lon: String) {
        guard let _ = store else {return}
        if let latD = Double(lat), let lonD = Double(lon) {
            let regionDistance: CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latD, lonD)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span),
                MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
            ] as [String : Any]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "Lukas Sweets"
            mapItem.openInMaps(launchOptions: options)
        }
    }
    
}


