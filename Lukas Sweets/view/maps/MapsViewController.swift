//
//  MapsViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 05.02.2020.
//

import UIKit
import GoogleMaps
import MapKit

import LBBottomSheet




struct mapMarker {
    var lat: Double
    var lon: Double
    var addr: String
    var time: String
}


class MapsViewController: UIViewController {
    
    @IBOutlet weak var mapViews: GMSMapView!
    
    private let locationManager = CLLocationManager()
    private var showBottomSheet = false

    
    var selectedStore: StoreModel?
    private var stores: [StoreModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapViews.delegate = self
        
        getStores()
    }
    

    
    private func getStores() {
        Task {
            stores = await StoreRepositoty.getStores()
            setStores()
        }
    }
    
    private func setStores() {
        for (index, store) in stores.enumerated() {
            if let lat = store.lat, let lon = store.lon {
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: Double(lat) ?? 0.0, longitude: Double(lon) ?? 0.0)
                marker.title = store.address
                marker.snippet = store.time_work
                marker.icon = UIImage(named: "marker")
                marker.appearAnimation = .pop
                marker.accessibilityLabel = String(index)
                marker.map = self.mapViews
            }
        }
    }
    

    
    
    //START MAPS APPLE
    private func openMapForPlace(lat: Double, lon: Double) {

        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(lat, lon)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span),
            MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
        ] as [String : Any]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Lukas Sweets"
        mapItem.openInMaps(launchOptions: options)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.clearNavigationBar()
        
    }
    
    private func showStoreBottomSheet() {
        dismissBottomSheet(){
            let s = UIStoryboard(name: "StoreBottomSheetView", bundle: nil)
            let vc = s.instantiateViewController(withIdentifier: "StoreBottomSheetView") as! StoreBottomSheetView
            vc.store = self.selectedStore
//            let grabberBackground: BottomSheetController.Theme.Grabber.Background = .color(.clear, isTranslucent: false)
//            let grabber: BottomSheetController.Theme.Grabber = .init(background: grabberBackground)
//            let theme: BottomSheetController.Theme = .init(grabber: grabber, dimmingBackgroundColor: .clear)
            let theme: BottomSheetController.Theme = .init(dimmingBackgroundColor: .clear)
            let behavior: BottomSheetController.Behavior = .init(swipeMode: .full, forwardEventsToRearController: true, heightMode: .free(minHeight: 150.0, maxHeight: 150.0))
            self.presentAsBottomSheet(vc, theme: theme, behavior: behavior)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissBottomSheet()
    }
    
}


extension MapsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if let selectedStore = selectedStore {
            if let latString = selectedStore.lat, let lonString = selectedStore.lon {
                if let lat = Double(latString), let lon = Double(lonString) {
                    mapViews.animate(to: GMSCameraPosition(target: CLLocationCoordinate2D(latitude: lat, longitude: lon), zoom: 16, bearing: 0, viewingAngle: 0))
                    showStoreBottomSheet()
                }
            }
        } else {
            switch status {
                
            case .denied:
                mapViews.camera = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 49.0139, longitude: 31.2858), zoom: 5, bearing: 0, viewingAngle: 0)
                break
                
            default:
                locationManager.startUpdatingLocation()
                
                mapViews.isMyLocationEnabled = true
                mapViews.settings.myLocationButton = true
            }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapViews.camera = GMSCameraPosition(target: location.coordinate, zoom: 12, bearing: 0, viewingAngle: 0)
        
        locationManager.stopUpdatingLocation()
    }
    
    
}

extension MapsViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let indexString = marker.accessibilityLabel{
            if let index = Int(indexString) {
                selectedStore = stores[index]
                if let selectedStore = selectedStore {
                    if let latString = selectedStore.lat, let lonString = selectedStore.lon {
                        if let lat = Double(latString), let lon = Double(lonString) {
                            mapViews.animate(to: GMSCameraPosition(target: CLLocationCoordinate2D(latitude: lat, longitude: lon), zoom: 16, bearing: 0, viewingAngle: 0))
                            showStoreBottomSheet()
                        }
                    }
                }
            }
        }
        return true
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        dismissBottomSheet()
    }
    
    
}



extension UIView {
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
    
    class func instanceFromNib(_ name: String) -> UIView? {
        return UINib(nibName: name, bundle: nil).instantiate(withOwner: nil, options: nil).first as? UIView
    }
    
}


