//
//  StoreViewController.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 31.03.2023.
//

import UIKit


class StoresViewController: UIViewController, UIScrollViewDelegate, DropDownDelegate {
    
    @IBOutlet weak var cityCustomTextEdit: CustomTextField!
    @IBOutlet weak var storesTableView: UITableView!
    
    private var selectedCity: DropDownList?
    private var cities: [DropDownStruct] = []
    private var stores: [StoreModel] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dismissKeyboard()
        
        self.storesTableView.delegate = self
        self.storesTableView.dataSource = self
        
        setupRightItemBar()
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(selectCity))
        tap2.numberOfTapsRequired = 1
        cityCustomTextEdit.tapGestureRecognizer = tap2
        
        //Fix tap gesture for table view and other
        for gesture in (self.view?.gestureRecognizers)! {
            if gesture.isKind(of: UITapGestureRecognizer.self) {
                gesture.isEnabled = false // or true if you want to enable it
            }
        }
    }
    
    func changeValue(value: DropDownList?, clear: Bool) {
        if let value = value {
            cityCustomTextEdit.text = value.name
        }
        if clear {
            cityCustomTextEdit.text = ""
        }
        
        cityCustomTextEdit.isEdit = false
        updateUserCity(cityID: value?.name)
        selectedCity = value
        getStores(cityID: value?.name)
    }
    
    @objc func selectCity() {
        cityCustomTextEdit.isEdit = true
        let storyboard = UIStoryboard(name: "DropDown", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DropDownController") as! DropDownController
        vc.dataList = cities
        vc.selected = selectedCity
        vc.titleLabel = "city".localized
        vc.delegate = self
        self.present(vc, animated: true)
    }
    
    private func updateUserCity(cityID: String?) {
        Task {
            await UserRepository.updateUserCity(cityID: cityID)
        }
    }
    
    private func getStores(cityID: String? = nil) {
        Task {
            guard let cityID = cityID else { return }
            stores = await StoreRepositoty.getStores(city:cityID)
            storesTableView.reloadData()
        }
    }
    
    private func setup() {
        Task {
            let result = await [
                UserRepository.getUserCity() ?? "",
                StoreRepositoty.getCities()
                    .enumerated()
                    .map { (index, element) in
                        DropDownList(id: index, name: element)
                    }
                    .sorted {
                        $0.name < $1.name
                    }
            ] as [Any]
            
            cities = [DropDownStruct(list: result[1] as? [DropDownList])]
            cities.forEach({ l in
                selectedCity = l.list?.filter({
                    $0.name == result[0] as? String
                }).first
            })
            cityCustomTextEdit.text = selectedCity?.name
            getStores(cityID: result[0] as? String)
        }
    }
    
    private func setupRightItemBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "maps".localized, style: .plain, target: self, action: #selector(openMaps))
        //        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_stores_front"), style: .plain, target: self, action: nil)
    }
    
    @objc private func openMaps() {
        self.performSegue(withIdentifier: "showMaps", sender: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBar()
        setup()
    }
}

extension StoresViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Store", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapsViewController") as! MapsViewController
        vc.selectedStore = stores[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as? StoreTableViewCell else { return UITableViewCell() }
        let store = stores[indexPath.row]
        cell.addressLabel.text = store.address
        cell.timeWorkLabel.text = store.time_work
        
        return cell
        
    }
    
}
