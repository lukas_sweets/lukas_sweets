//
//  StoreViewCell.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 02.04.2023.
//

import UIKit


class StoreTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var timeWorkLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
