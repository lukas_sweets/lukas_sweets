//
//  DropDownController.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 30.03.2023.
//

import UIKit



protocol DropDownDelegate {
    func changeValue(value: DropDownList?, clear: Bool)
}

struct DropDownStruct {
    var section: String?
    var list: [DropDownList]?
}

struct DropDownList {
    var id: Int
    var name: String
    var model: Any?
}

class DropDownController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var navigationRightItem: UIBarButtonItem!
    @IBOutlet weak var navigationLeftItem: UIBarButtonItem!
    
    var dataList: [DropDownStruct] = []
    var titleLabel: String?
    var selected : DropDownList?
    var clear : Bool = false
    var showClear: Bool = false
    
    var delegate: DropDownDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationBar.topItem?.title = titleLabel
        if showClear {
            setupClearButton()
        }
    }
    
    @IBAction func closeDropDown(_ sender: Any) {
        dismiss(animated: true)
    }
    
    private func setupClearButton() {
        let navigationLeftItem = UIBarButtonItem(title: "clear".localized, style: .plain, target: self, action: #selector(deleteSelected))
        navigationBar.topItem?.leftBarButtonItem = navigationLeftItem
    }
    
    @objc func deleteSelected(_ sender: Any) {
        selected = nil
        clear = true
        dismiss(animated: true)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let delegate = self.delegate {
            delegate.changeValue(value: selected, clear: clear)
        }
    }
    
}

extension DropDownController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList[section].list?.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let data = dataList[indexPath.section].list?[indexPath.row]
        var content = cell.defaultContentConfiguration()
        content.text = data?.name
        
        if selected?.id == data?.id {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.contentConfiguration = content
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dataList[section].section
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = dataList[indexPath.section].list?[indexPath.row]
        dismiss(animated: true)
        
    }
    
}
