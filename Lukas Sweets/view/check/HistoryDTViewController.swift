//
//  HistoryDTViewController.swift
//  Lukas Sweets
//
//  Created by Максим on 17.05.2021.
//

import UIKit
import PKHUD
import ContentLoader



class HistoryDTViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var rowsTableView: UITableView!
    
    @IBOutlet weak var qrView: UIView!
    @IBOutlet weak var qrImageView: UIImageView!
    
    @IBOutlet weak var headDateTimeLabel: UILabel!
    @IBOutlet weak var headStoreLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    @IBOutlet weak var typeCheck: UILabel!
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var pashalLabel: UILabel!
    
    @IBOutlet weak var payCashView: UIView!
    @IBOutlet weak var payCashLabel: UILabel!
    @IBOutlet weak var payCardView: UIView!
    @IBOutlet weak var payCardLabel: UILabel!
    @IBOutlet weak var payBonusView: UIView!
    @IBOutlet weak var payBonusLabel: UILabel!
    @IBOutlet weak var payCertificateView: UIView!
    @IBOutlet weak var payCertificateLabel: UILabel!
    
    var id_history : Int = 0

    
    
    private var check: HistoryModel?
    private var checkDetails: [HistoryDetailsModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pashalLabel.layer.zPosition = -1
        
        
        headerView.cornerRadius = 10
        headerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.startLoading()
        
        
        rowsTableView.dataSource = self
        rowsTableView.delegate = self
        
        //        paysTableView.dataSource = self
        //        paysTableView.delegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        qrImageView.isUserInteractionEnabled = true
        qrImageView.addGestureRecognizer(tapGestureRecognizer)
        
        let add = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(updateHistory))
        navigationItem.rightBarButtonItems = [add]
        
    }
    
    private func getCheck(reload: Bool = false) {
        Task {
            HUD.show(.progress)
            let result = await CheckRepository.getCheck(checkID: id_history, reload: reload)
            HUD.hide()
            
            switch result {
            case .success(let data):
                check = data
                setCheck()
                break
            case .internet(let data):
                check = data
                setCheck()
                break
            default:
                Alert.serverError()
                break
            }
            
            
        }
    }
    
    private func setCheck() {
        headDateTimeLabel.text = check?.date_time.formatDate()
        headStoreLabel.text = check?.store ?? ""
        numLabel.text = check?.num
        totalLabel.text = String(format: "%.02f ₴", check?.sum_total ?? 0.0)
        bonusLabel.text = String(check?.bonus_add ?? 0)
        
        if(check?.reason == 1){
            typeCheck.text = NSLocalizedString("history_dt_sale", comment: "")
        } else {
            typeCheck.text = NSLocalizedString("history_dt_refuse", comment: "")
            typeCheck.textColor = redColor
        }
        
        if let qr = check?.qr {
            qrImageView.image = self.generateQRCode(from: qr)
        } else {
            qrView.isHidden = true
        }
        //        paysTableView.reloadData()
        if check?.pay_cash ?? 0.0 > 0.0 {
            payCashLabel.text = String(format: "%.02f ₴", check?.pay_cash ?? 0.0)
        } else {
            payCashView.isHidden = true
        }
        
        if check?.pay_card ?? 0.0 > 0.0 {
            payCardLabel.text = String(format: "%.02f ₴", check?.pay_card ?? 0.0)
        } else {
            payCardView.isHidden = true
        }
        
        if check?.pay_bonus ?? 0.0 > 0.0 {
            payBonusLabel.text = String(format: "%.02f ₴", check?.pay_bonus ?? 0.0)
        } else {
            payBonusView.isHidden = true
        }
        
        if check?.pay_certificate ?? 0.0 > 0.0 {
            payCertificateLabel.text = String(format: "%.02f ₴", check?.pay_certificate ?? 0.0)
        } else {
            payCertificateView.isHidden = true
        }
        view.hideLoading()
    }
    
    private func getCheckDetails(reload: Bool = false){
        Task {
            switch await CheckRepository.getCheckDetails(checkID: id_history, reload: reload) {
            case .success(let data):
                checkDetails = data
                break
            case .internet(let data):
                Alert.noInternet()
                guard let data = data else { return }
                checkDetails = data
                break
            default:
                Alert.serverError()
                break
            }
            rowsTableView.reloadData()
        }
    }
    
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if let check = check, let qr = check.qr {
            guard let url = URL(string: qr) else { return }
            UIApplication.shared.open(url)
        }
    }
    
    
    @objc func updateHistory(gesture: UITapGestureRecognizer) {
        getCheck(reload: true)
        getCheckDetails(reload: true)
    }
    
    
    
    
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.whiteNavigationBar()
        self.topBarColor(color: backgroundGray)
        getCheck()
        getCheckDetails()
    }
    
    
    
    
}






extension HistoryDTViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return checkDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let menuCell = tableView.dequeueReusableCell(withIdentifier: "GoodsCell", for: indexPath) as? RowsHistoryTableViewCell else { return UITableViewCell() }
        
        let dataCell = checkDetails[indexPath.row]
        
        menuCell.GoodLabel.text = dataCell.goods_name
        menuCell.CntLabel.text = dataCell.cnt.clean
        menuCell.SumLabel.text =  String(format: "%.02f ₴", dataCell.sum_total)
        
        return menuCell
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
}


