//
//  HistoryViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 16.03.2020.
//

import UIKit
import PKHUD

final class HistoryViewController: UIViewController {
    
    @IBOutlet weak var noHistoryView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let refreshControl = UIRefreshControl()
    private let moreButton = UIButton()
    private var selectedCell = IndexPath()
    
    
    
    struct MonthSection : Comparable{
        
        var month : Date
        var headlines : [HistoryModel]
        
        static func == (lhs: MonthSection, rhs: MonthSection) -> Bool {
            return lhs.month == rhs.month
        }
        
        
        static func < (lhs: MonthSection, rhs: MonthSection) -> Bool {
            return lhs.month < rhs.month
        }
    }
    
    
    private var sections = [MonthSection]()
    private var dataSource: [HistoryModel] = []
    
    private var limit = 1
    private var countHistotyToOne = 10
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        noHistoryView.isHidden = true
        
        pullToRefresh()
        refreshButton.addTarget(self, action: #selector(getChecks), for: .touchUpInside)
        
        getChecks()
        
    }
    
    
    @objc private func getChecks(limit: Int = 10, reload: Bool = true) {
        Task {
//            HUD.show(.progress)
            activityIndicator.startAnimating()
            
            let data = await CheckRepository.getChecks(limit: limit, reload: reload)
            
            switch data {
            case .success(let data):
                dataSource = data
                setChecks()
                Log.e(dataSource)
                break
            case .internet(let data):
                if let data = data {
                    if data.count > 0 {
                        dataSource = data
                        setChecks()
                    }
                }
                Alert.noInternet()
                break
            case .error:
                Alert.serverError()
                break
            }
            
        }
    }
    
    private func setChecks() {
        noHistoryView.isHidden = dataSource.isEmpty ? false : true
        
        let groups = Dictionary(grouping: dataSource) { (headline) -> Date in
            return firstDayOfMonth(date: headline.date_time.toDateTime()
            )}
        self.sections = groups.map { (date, healines) in
            return MonthSection(month: date, headlines: healines)
        }
        
        sections.sort {(lhs, rhs) in lhs.month > rhs.month}
        
        if (dataSource.count >= Preference.instance.checks_count) {
            moreButton.isHidden = true
        } else {
            moreButton.isHidden = false
        }
        tableView.reloadData()
        
        activityIndicator.stopAnimating()
    }
    
    
    
    private func pullToRefresh () {
        self.refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.refreshControl.tintColor = redColor
        self.refreshControl.layer.zPosition += 1
        self.tableView.addSubview(refreshControl)
        
    }
    
    
    @objc func refreshData (refreshControl: UIRefreshControl) {
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        getChecks(reload: true)
    }
    
    
    
    
    private func firstDayOfMonth(date: Date) -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: date)
        return calendar.date(from: components)!
    }
    
    
    @objc func moreData() {
        limit += 1
        getChecks(limit: limit*countHistotyToOne, reload: false)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showHistoryDT" {
            
            let sec = self.sections[selectedCell.section]
            let head = sec.headlines[selectedCell.row]
            
            let id_history: Int = head.id
            (segue.destination as? HistoryDTViewController)?.id_history = id_history
            
            
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBarLargeTitle()

        
    }
    
    ///END
}


extension HistoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = self.sections[section]
        return section.headlines.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let menuCell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as? HistoryTableViewCell else { return UITableViewCell() }
        
        let section = self.sections[indexPath.section]
        let headline = section.headlines[indexPath.row]
        
        //        print(headline)
        
        menuCell.selectionStyle = .none
        
        
        menuCell.dateLabel.text = headline.date_time.formatDate(toFormat: "dd.MM.yyyy")
        menuCell.timeLabel.text = headline.date_time.formatDate(toFormat: "HH:mm")
        
        if let store = headline.store {
            menuCell.sumLabel.text = String(format: "%.02f ₴", headline.sum_total)
            menuCell.adresLabel.text = store
        } else {
            menuCell.sumLabel.text = "🔥"
            if let comment = headline.comment {
                menuCell.adresLabel.text = comment
            } else {
                menuCell.adresLabel.text = "history_no_comment".localized
            }
        }
        
        
        if(headline.bonus_total < 0){
            menuCell.bonus.textColor = redColor
            menuCell.bonus.text = String(headline.bonus_total)
        } else if(headline.bonus_total > 0){
            menuCell.bonus.textColor = greenColor
            menuCell.bonus.text = "+" + String(headline.bonus_total)
        } else {
            menuCell.bonus.textColor = yellowColor
            menuCell.bonus.text = String(headline.bonus_total)
            
        }
        
        return menuCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let section = self.sections[indexPath.section]
        let headline = section.headlines[indexPath.row]
        
        self.selectedCell = indexPath
        
        if let _ = headline.store {
            performSegue(withIdentifier: "showHistoryDT", sender: self)
        }
        //        if(headline.comment?.count == 0){
        //
        //        }
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerNib = UINib.init(nibName: "HistorySectionHeaderCell", bundle: Bundle.main)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HistorySectionHeaderCell")
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HistorySectionHeaderCell") as! HistorySectionHeaderCell
        
        
        let section = self.sections[section]
        let date = section.month
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "locale_time".localized)
        dateFormatter.dateFormat = "LLLL"
        
        let dateFormatterYear = DateFormatter()
        dateFormatterYear.locale = Locale(identifier: "locale_time".localized)
        dateFormatterYear.dateFormat = "yyyy"
        
        headerView.monthLabel.text = dateFormatter.string(from: date).capitalized
        headerView.yearLabel.text = dateFormatterYear.string(from: date)
        
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        
        let sections = tableView.numberOfSections-1
        if (sections == section) {
            
            footerView.backgroundColor = .clear
            footerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
            
            moreButton.frame = CGRect(x: 10, y: 0, width: self.view.frame.width-20, height: 50)
            moreButton.setTitle("history_more_button".localized, for: .normal)
            moreButton.setTitleColor(.white, for: .normal)
            moreButton.backgroundColor = redColor
            moreButton.layer.cornerRadius = 5
            moreButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            moreButton.addTarget(self, action: #selector(self.moreData), for: .touchUpInside)
            footerView.addSubview(moreButton)
            
            
        }
        return footerView
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let sections = tableView.numberOfSections-1
        if (sections == section) {
            return 80
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 31
    }
    
    
    
}



