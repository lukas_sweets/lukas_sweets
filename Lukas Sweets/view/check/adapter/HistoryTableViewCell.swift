//
//  HistoryTableViewCell.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 16.03.2020.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var adresLabel: UILabel!
    @IBOutlet weak var bonus: UILabel!
    @IBOutlet weak var mainView: UIStackView!
    
    @IBOutlet weak var containerView: UIView!
    
    //@IBOutlet weak var dateLabel: UILabel!
    //@IBOutlet weak var adresLabel: UILabel!
    
    //@IBOutlet weak var bonus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    
}
