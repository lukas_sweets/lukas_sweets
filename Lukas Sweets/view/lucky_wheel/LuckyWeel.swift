//
//  LuckyWeel.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 13.09.2022.
//

import UIKit
import SwiftFortuneWheel
import MHLoadingButton
import SPConfetti
import SwiftMessages
import BadgeHub
//import BadgeControl



class LuckyWeelViewController: UIViewController {
    
    @IBOutlet weak var startButton: LoadingButton!
    @IBOutlet weak var startButtonView: UIView!
    @IBOutlet weak var logoWheel: UIImageView!
    @IBOutlet weak var bgWheel: UIImageView!
    @IBOutlet weak var countLabel: UILabel!
    
    
    @IBOutlet weak var LogoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var wheelTopConstant: NSLayoutConstraint!
    @IBOutlet weak var startButtonViewConstant: NSLayoutConstraint!
    
    private var slices: [Slice] = []
    private var startAnimation = true
    private var article: [LuckyWheelArticleModel] = []
//    private var couponButton = UIButton()
    private var couponsBarItem = UIBarButtonItem()
//    private var badge = BadgeController(for: UIView())
    
    
    @IBOutlet weak var wheelView: SwiftFortuneWheel!{
        didSet {
            //turns on tap gesture recognizer
            wheelView.wheelTapGestureOn = true
            
            //selected index by tap
            wheelView.onWheelTap = { (index) in
                print("tap to index: \(index)")
            }

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setStartButton()
        setDefaultWheel()
        
        countLabel.alpha = 0
        animationAfter()
        
        wheelView.pinImageViewCollisionEffect = CollisionEffect(force: 8, angle: 20)
        wheelView.edgeCollisionDetectionOn = true
        
        settingBarItems()
    }
    
    @objc private func getInfo(send: Any) {
        performSegue(withIdentifier: "goInfoWheel", sender: nil)
    }
    
    @objc private func getCoupons(send: Any) {
        performSegue(withIdentifier: "goCoupons", sender: nil)
    }
    
    
    
    
    @IBAction func startWeel(_ sender: Any) {
        Task {
            startButton.showLoader(userInteraction: false)
            
            switch await LuckyWheelRepository.startLuckyWheel() {
            case .success(let data):
                self.setWheel(prises: data.prises)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.wheelView.startRotationAnimation(rotationOffset: CGFloat(data.win_prise.angle), { status in
                        self.showWinPrise(prise: data.win_prise)
                        self.setStatusStartButton(data.count)
                        self.setCountCoupons(count: data.coupons)
                        self.startButton.hideLoader()
                    })
                }
                break
            case .internet:
                Alert.noInternet()
                self.startButton.hideLoader()
                break
            default:
                Alert.serverError()
                self.startButton.hideLoader()
                break
            }
        }
    }
    
    private func setBackground() {
        let gradientView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 35))
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.view.frame.size
        gradientLayer.colors = [
            UIColor(red: 0.00, green: 0.29, blue: 0.29, alpha: 1.00).cgColor,
            UIColor(red: 0.89, green: 0.92, blue: 0.62, alpha: 1.00).withAlphaComponent(1).cgColor
        ]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.0)
        //        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        gradientView.layer.addSublayer(gradientLayer)
        view.insertSubview(gradientView, at: 0)
    }
    
    private func setDefaultWheel () {
        let sliceColorType = SFWConfiguration.ColorType.customPatternColors(colors: colorsArrayLuckyWheel , defaultColor: .white)
        let slice = Slice(contents: [])
        let slicePreferences = SFWConfiguration.SlicePreferences(backgroundColorType: sliceColorType, strokeWidth: 0, strokeColor: .black)
        let circlePreferences = SFWConfiguration.CirclePreferences(strokeWidth: 25, strokeColor: borderLuckyWheel)
        
        let anchorImage = SFWConfiguration.AnchorImage(imageName: "white_dot", size: CGSize(width: 12, height: 12), verticalOffset: -6)
        var wheelPreferences = SFWConfiguration.WheelPreferences(circlePreferences: circlePreferences, slicePreferences: slicePreferences,  startPosition: .top)
        wheelPreferences.centerImageAnchor = anchorImage
        
        let configuration = SFWConfiguration(wheelPreferences: wheelPreferences, pinPreferences: SFWConfiguration.PinImageViewPreferences(size: CGSize(width: 50, height: 50), position: .top, verticalOffset: -25))
        wheelView.configuration = configuration
        
        for _ in 0...7 {
            slices.append(slice)
        }
        wheelView.slices = slices
    }
    
    
    
    
    private func setWheel (prises: [LuckyWheelPrisesModel]) {
        slices.removeAll()
        
        for prise in prises {
            var textPref = TextPreferences(textColorType: .evenOddColors(evenColor: .white, oddColor: .white), font: UIFont.systemFont(ofSize: 20, weight: .bold), verticalOffset: 0)
            textPref.alignment = .center
            textPref.orientation = .vertical
            let text = Slice.ContentType.text(text: prise.icon, preferences: textPref)
            let slice = Slice(contents: [text])
            
            slices.append(slice)
        }
        wheelView.slices = slices
    }
    
    private func setStartButton() {
        startButton.indicator = MaterialLoadingIndicator(color: .blue)
        startButton.setImage(UIImage(systemName: "play.fill"))
        startButton.imageView?.layer.transform = CATransform3DMakeScale(2.0, 2.0, 2.0)
        startButton.applyshadowWithCornerView(containerView: startButtonView, cornerRadious: startButton.frame.size.width * 0.5)
    }
    
    private func animationBefore () {
        if(startAnimation) {
            LogoTopConstraint.constant -= self.view.bounds.height
            wheelTopConstant.constant += self.view.bounds.height
            startButtonViewConstant.constant -= self.view.bounds.height
            startAnimation = !startAnimation
        }
    }
    
    private func animationAfter () {
        
        UIView.animate(withDuration: 1,
                       delay: 0.0,
                       options: [.curveEaseOut],
                       animations: {
            self.LogoTopConstraint.constant += self.view.bounds.height
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        
        UIView.animate(withDuration: 1,
                       delay: 0.5,
                       options: [.curveEaseOut],
                       animations: {
            self.wheelTopConstant.constant -= self.view.bounds.height
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        
        UIView.animate(withDuration: 1,
                       delay: 0.5,
                       options: [.curveEaseOut],
                       animations: {
            self.startButtonViewConstant.constant += self.view.bounds.height
            
            self.view.layoutIfNeeded()
            
        }, completion: nil)
        
        UIView.animate(withDuration: 1,
                       delay: 1.5,
                       animations: {
            self.countLabel.alpha = 1
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    private func rotate(view: UIView) {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 30
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        view.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    private func getCountLuckyWheel() {
        Task {
            let resultApi = await LuckyWheelRepository.getCountFreeSpin()
            switch resultApi {
            case .success(let data):
                setStatusStartButton(data.count)
                article = data.article
                setCountCoupons(count: data.coupons)
                break
            case .internet:
                Alert.noInternet()
                break
                
            default:
                Alert.serverError()
                break
            }
        }
    }
    
    private func setStatusStartButton (_ count: Int) {
        countLabel.text = "count_wheel".localized_with_argument(arg: String(count))
        if (count > 0) {
            startButton.isEnabled = true
            startButton.tintColor = .blue
        } else {
            startButton.isEnabled = false
            startButton.tintColor = .lightGray
        }
    }
    
    private func setCountCoupons(count: Int) {
//        let hub = BadgeHub(view: couponButton) // Initially count set to 0
        let hub = BadgeHub(barButtonItem: couponsBarItem)
        guard let hub = hub else { return }
        hub.setCount(count)
        hub.scaleCircleSize(by: 0.4)
        hub.setCircleColor(.red, label: .red)
//        if(count > 0) {
//            badge = BadgeController(for: self.couponButton)
//            badge.badgeHeight = 20
//            badge.badgeTextFont = UIFont.systemFont(ofSize: 15)
//            let y = self.couponButton.frame.origin.y + 10
//            badge.centerPosition = .custom(x: self.couponButton.frame.origin.x + self.couponButton.frame.maxX-10, y: y)
//            badge.addOrReplaceCurrent(with: String(count), animated: false)
//        } else {
//            badge.remove(animated: false)
//        }
    }
    
    private func showWinPrise(prise: LuckyWheelWinPriseModel?) {
        if let prise = prise {
            SPConfetti.startAnimating(.fullWidthToDown, particles: [.triangle, .arc], duration: 5)
            
            let view: PriseLuckyWheel = try! SwiftMessages.viewFromNib()
            view.configureDropShadow()
            view.okAction = {
                SwiftMessages.hide()
            }
            
            view.iconImageView?.loadImage(fromURL: prise.picture)
            view.titleLabel?.text = "title_win_prise_sheet".localized
            view.bodyLabel?.text = "body_win_prise_sheet".localized_with_argument(arg: prise.name)
            
            if(prise.is_bonus){
                view.infoView?.isHidden = true
            }
            
            var config = SwiftMessages.defaultConfig
            config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            config.duration = .forever
            config.presentationStyle = .bottom
            config.dimMode = .gray(interactive: false)
            config.keyboardTrackingView = KeyboardTrackingView()
            config.preferredStatusBarStyle = .lightContent
            config.interactiveHide = false
            
            SwiftMessages.show(config: config, view: view)
        }
    }
    
    private func settingBarItems() {
        let infoBarItem = UIBarButtonItem(image: .init(named: "ic_info")?.withTintColor(.white), style: .done, target: self, action: #selector(getInfo))
        couponsBarItem = UIBarButtonItem(image: .init(named: "ic_coupon")?.withTintColor(.white), style: .done, target: self, action: #selector(getCoupons))
        
        navigationItem.rightBarButtonItems = [couponsBarItem, infoBarItem]
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        getCountLuckyWheel()
        animationBefore()
        rotate(view: bgWheel)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as? LuckyWheelInfoController)?.article = article
    }
}
