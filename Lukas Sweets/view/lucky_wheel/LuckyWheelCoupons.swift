//
//  LuckyWheelCoupons.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 26.09.2022.
//

import UIKit
import PKHUD

class LuckyWheelCoupons: UIViewController {
    
    @IBOutlet weak var couponsCollectionView: UICollectionView!
    @IBOutlet weak var sortedSegment: UISegmentedControl!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var filterCoupons: [LuckyWheelCouponsModel] = []
    private var coupons: [LuckyWheelCouponsModel] = [] {
        didSet {
            filterCoupons = coupons
        }
    }
    
    private var selectedCoupon = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        couponsCollectionView.delegate = self
        couponsCollectionView.dataSource = self
        
    }
    
    func getCoupons () {
        Task {
            activityIndicator.startAnimating()
            switch await LuckyWheelRepository.getCoupons() {
            case .success(let data):
                coupons = data
                couponsCollectionView.reloadData()
                break
            case .internet:
                Alert.noInternet()
                break
            default:
                Alert.serverError()
                break
            }
            activityIndicator.stopAnimating()
            applyFilter()
        }
    }
    
    
    private func sortCouponsActive() -> [LuckyWheelCouponsModel] {
        return coupons.filter { data in
            if !data.is_off && !data.is_used {
                return true
            }
            return false
        }
    }

    
    
    @IBAction func filteredArray(_ sender: Any) {
       applyFilter()
    }
    
    private func applyFilter() {
        switch sortedSegment.selectedSegmentIndex {
        case 1:
            filterCoupons = sortCouponsActive()
            break
        default:
            filterCoupons = coupons
        }
        couponsCollectionView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as? LuckyWheelCouponDetailsViewController)?.coupon = filterCoupons[selectedCoupon]
        (segue.destination as? LuckyWheelCouponDetailsViewController)?.color = colorsArrayCoupons[selectedCoupon]

    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.whiteNavigationBar()
        getCoupons()
    }
    
    
    
}

extension LuckyWheelCoupons: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterCoupons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CouponCell", for: indexPath) as! CouponCell
        let data = filterCoupons[indexPath.row]
        myCell.pictureImgeView.loadImage(fromURL: data.picture)
        myCell.nameLabel.text = data.name
        myCell.dateLabel.text = "lucky_wheel_use_to".localized_with_argument(arg: data.date_to)// + data.date_to.formatDate(toFormat: "dd.MM.yyyy")
        
        if (data.is_used) {
            myCell.statusLabel.text = "lucky_wheel_status_used".localized
            myCell.dateLabel.isHidden = true
            myCell.statusLabel.textColor = .systemGreen
        } else {
            myCell.statusLabel.text = "lucky_wheel_status_off".localized
            myCell.dateLabel.isHidden = false
            myCell.statusLabel.textColor = .systemRed
        }
        
        if !data.is_off && !data.is_used {
            myCell.statusLabel.isHidden = true
        } else {
            myCell.statusLabel.isHidden = false
        }
        
        myCell.topView.isHidden = !data.is_off

        if !data.is_off && !data.is_used {
            myCell.bgView.backgroundColor = colorsArrayCoupons[indexPath.row]
        } else {
            myCell.bgView.backgroundColor = .white
        }
        
        return myCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCoupon = indexPath.row
        let data = filterCoupons[selectedCoupon]
        
        if !data.is_used && !data.is_off {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CouponDetailLuckyWheelViewController") as! LuckyWheelCouponDetailsViewController
            vc.coupon = data
            vc.color = colorsArrayCoupons[indexPath.row]
            self.present(vc, animated: true)
//            self.navigationController?.pushViewController(vc, animated: true)
        }
        
//        if(data.is_used != true && data.date_to.toDateTime() >= Date()) {
//            performSegue(withIdentifier: "goCouponDetail", sender: nil)
//        }
    }
    
}

extension LuckyWheelCoupons: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemPerRow = 1.0
        let paddingWith = 15 * (itemPerRow + 1)
        let availableWith = collectionView.frame.width - CGFloat(paddingWith)
        let withPerItem = availableWith / itemPerRow
        return CGSize(width: withPerItem, height: withPerItem * 0.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    
}




