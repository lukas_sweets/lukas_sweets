//
//  LuckyWheelCouponDetailsViewController.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 27.09.2022.
//

import UIKit

class LuckyWheelCouponDetailsViewController: UIViewController {

//    private let db:DBHelper = DBHelper()

    
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var dateOffLabel: UILabel!
    @IBOutlet weak var barcodeImageView: UIImageView!
    
    var coupon: LuckyWheelCouponsModel?
    var color: UIColor? = .blue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCouponDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setCouponDetails() {
        
        view.backgroundColor = color
       
        if let coupon = coupon {
            nameLabel.text = coupon.name
            pictureImageView.loadImage(fromURL: coupon.picture)
            storeLabel.text =  coupon.store
            dateOffLabel.text = coupon.date_to//.formatDate(toFormat: "dd.MM.yyyy")
            barcodeImageView.image =  BarcodeGenerator.generate(from: Preference.instance.bonus_card_number ?? "", descriptor: .code128, size: barcodeImageView.frame.size, white: true)
        }
    }

    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
