//
//  LuckyWheelInfo.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 22.09.2022.
//

import UIKit

class LuckyWheelInfoController: UIViewController {
    
    var article: [LuckyWheelArticleModel] = []
    @IBOutlet weak var articleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        articleLabel.text = "article_wheel".localized
        for (index, a) in article.enumerated() {
            articleLabel.text! += "\n\(index+1). \(a.name)"
        }
    }
    
    @IBAction func CloseButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func officialRules(_ sender: Any) {
        if let url = URL(string: "https://lukas.ua/pravyla-ta-umovy-aktsii-koleso-udachi") {
            UIApplication.shared.open(url)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.topBarColor(color: .clear)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
