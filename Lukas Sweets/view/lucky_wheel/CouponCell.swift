//
//  CouponCell.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 26.09.2022.
//

import UIKit

class CouponCell: UICollectionViewCell {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var pictureImgeView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    

    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    private func setup() {
        topView.alpha = 0.3
    }
}
