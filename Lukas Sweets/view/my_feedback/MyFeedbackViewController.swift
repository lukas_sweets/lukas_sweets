//
//  FeedbackViewController.swift
//  Lukas Sweets
//
//  Created by Максим on 14.09.2021.
//

import UIKit
import PKHUD



class MyFeedbackViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var noFeedbackLabel: UILabel!
    
    
    private var selectedCell = IndexPath()
    private var feedbackModel : [FeedbackModel] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshFeedabck))
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        
       
        getFeedback(remote: false)
    }
    
    @objc private func refreshFeedabck() {
        HUD.show(.progress)
        getFeedback(remote: true)
    }
    
    private func getFeedback(remote: Bool) {
        Task {
            let result = await FeedbackRepository.getTopicFeedbacks(remote: remote)
            HUD.hide()

            switch result {
            case .success(let data):
                feedbackModel = data
                break
            case .internet(let data):
                Alert.noInternet()
                guard let data = data else { return }
                if !data.isEmpty {
                    feedbackModel = data
                }
            default:
                Alert.serverError()
            }
            setFeedback()

        }
    }
    
    private func setFeedback() {
        tableView.reloadData()
        noFeedbackLabel.isHidden = !feedbackModel.isEmpty
        
    }
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDTfeedback" {
            (segue.destination as? MyFeedbackDTViewController)?.feedbackID = feedbackModel[selectedCell.row].feedback_id
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBar()
        //self.whiteNavigationBarLargeTitle()

        getFeedback(remote: true)

    }
    
    
    
}


extension MyFeedbackViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedbackModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let feedbackCell = tableView.dequeueReusableCell(withIdentifier: "FeedbackCell", for: indexPath) as? FeedbackTableViewCell else { return UITableViewCell() }
        
        
        let feedback = feedbackModel[indexPath.row]
        print(feedback)
        
        if(feedback.is_read == 1) {
            feedbackCell.newMessageDot.isHidden = true
        } else {
            feedbackCell.newMessageDot.isHidden = false
        }

        
        switch feedback.status {
        case 1:
            feedbackCell.statusLabel.text = "feedback_status_new".localized
            feedbackCell.statusLabel.textColor = .darkGray
        case 2:
            feedbackCell.statusLabel.text = "feedback_status_work".localized
            feedbackCell.statusLabel.textColor = #colorLiteral(red: 0.8352941176, green: 0.6039215686, blue: 0.1254901961, alpha: 1)
        case 3:
            feedbackCell.statusLabel.text = "feedback_status_close".localized
            feedbackCell.statusLabel.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        
        default:
            print("")
        }
        
        switch feedback.subject {
        case 1:
            feedbackCell.subjectLabel.text = "feedback_subject_gratitude".localized
        case 2:
            feedbackCell.subjectLabel.text = "feedback_subject_questions".localized
        case 3:
            feedbackCell.subjectLabel.text = "feedback_subject_complaint".localized
        case 4:
            feedbackCell.subjectLabel.text = "feedback_subject_offer".localized
        
        default:
            print("")
        }
        
        feedbackCell.dateLabel.text = feedback.date_time.toDateTimeStringRelative
        
        return feedbackCell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedCell = indexPath
        
        performSegue(withIdentifier: "showDTfeedback", sender: self)
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
}

class FeedbackTableViewCell: UITableViewCell {
    
    @IBOutlet weak var newMessageDot: UIImageView!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
