//
//  MyFeedbackDTViewController.swift
//  Lukas Sweets
//
//  Created by Максим on 15.09.2021.
//

import UIKit



class MyFeedbackDTViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomMarginTableView: NSLayoutConstraint!
    @IBOutlet weak var bottomMarginTextView: NSLayoutConstraint!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textFieldView: UIView!
    @IBOutlet weak var sendImage: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var closeStatus: UILabel!
    
    
    private var timers: Timer? = nil
    
    private var visualEffectView = UIVisualEffectView()
    
    deinit {
        timers?.invalidate()
    }
    
     var feedbackID: Int?
    private var feedbacksData: [FeedbackModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dismissKeyboard()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        tableView.contentInset = UIEdgeInsets(
            top: textFieldView.bounds.height + (UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0.0),
            left: 0,
            bottom:  0,
            right: 0
        )
        tableView.allowsSelection = false
        
        
        closeStatus.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        
        let youtubeTapGesture = UITapGestureRecognizer(target: self, action: #selector(youtubeTapped(_:)))
        sendImage.addGestureRecognizer(youtubeTapGesture)
        sendImage.isUserInteractionEnabled = true
        
        indicator.isHidden = false
        indicator.startAnimating()
        
        getFeedbacks(remote: false)

        startUpdate()

        
    }
    
    
    private func getSubjectFeedback(subject: Int) -> String {
        switch subject {
        case 1:
            return "feedback_subject_gratitude".localized
        case 2:
            return "feedback_subject_questions".localized
        case 3:
            return "feedback_subject_complaint".localized
        case 4:
            return "feedback_subject_offer".localized
            
        default:
            return ""
        }
    }
    
    
    
    @objc func youtubeTapped(_ sender: UITapGestureRecognizer) {
        if let feedbackID = feedbackID, let text = textField.text?.trim() {
            if !text.isEmpty {
                let answerFull = FeedbackModel(message_id: -1, feedback_id: feedbackID, date_time: Date().toFormatString(), subject: -1, status: -1, text: text, author: 0, is_read: 1)
                
                feedbacksData.insert(answerFull, at: 0)
                textField.text = nil
                tableView.reloadData()
                tableView.scrollToTop()
                Task {
                    switch await FeedbackRepository.sendAnswerFeedback(feedbackID: answerFull.feedback_id, dateTime: answerFull.date_time, text: answerFull.text) {
                    case .success:
                        break
                    case .internet:
                        Alert.noInternet()
                        break
                    default:
                        Alert.serverError()
                    }

                }
            }
        }
    }
    
    
    
    
    
    
    
    @objc func keyboardWillShow(sender: NSNotification) {
        
        guard let userInfo = sender.userInfo else { return }
        
        let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let endFrameY = endFrame?.origin.y ?? 0
        let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
        if endFrameY >= UIScreen.main.bounds.size.height {
            bottomMarginTextView.constant = 0.0
        } else {
            bottomMarginTextView.constant = (endFrame?.size.height ?? 0.0) - (UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0.0)
        }

        
        UIView.animate(
            withDuration: duration,
            delay: TimeInterval(0),
            options: animationCurve,
            animations: { self.view.layoutIfNeeded() },
            completion: nil)
        
        tableView.contentInset = UIEdgeInsets(
            top: textFieldView.bounds.height + (endFrame?.size.height ?? 0.0), //(endFrame?.size.height ?? 0.0) - (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0.0) ,//+ textFieldView.bounds.height,
            left: 0,
            bottom: 20,
            right: 0
        )
        tableView.scrollToTop()
        
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        bottomMarginTableView.constant = 0
        bottomMarginTextView.constant = 0
        tableView.contentInset = UIEdgeInsets(
            top: textFieldView.bounds.height + (UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0.0),
            left: 0,
            bottom: 0,
            right: 0
        )
        
    }
    
    
    
    private func startUpdate(){
        if(timers == nil) {
            timers = Timer.scheduledTimer(withTimeInterval: 3, repeats: true, block: { [weak self]
                _ in
                self?.getFeedbacks(remote: true)
            })
        }
    }
    
    private func getFeedbacks(remote: Bool) {
        Task {
            if let feedbackID = feedbackID {
                let data = await FeedbackRepository.getFeedbacks(feedbackID: feedbackID, remote: remote)
                switch data {
                case .success(let data):
                    feedbacksData = data
                case .internet(let data):
                    Alert.noInternet()
                    guard let data = data else { return }
                    if !data.isEmpty {
                        feedbacksData = data
                    }
                    break
                default:
                    Alert.serverError()
                    break
                }
                setFeedbacks()
            }
        }
    }
    
    private func setFeedbacks() {
        if(feedbacksData[0].status == 3) {
            textFieldView.isHidden = true
            closeStatus.isHidden = false
        }
        title = getSubjectFeedback(subject: feedbacksData[0].subject)
        indicator.stopAnimating()
        indicator.isHidden = true
        tableView.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBar()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
    }
    
}




extension MyFeedbackDTViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedbacksData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let feedbackCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? FeedbackDTTableViewCell else { return UITableViewCell() }
        let feedback_dt = feedbacksData[indexPath.row]
        
        
        var feedback_dt_last: FeedbackModel?
        if((indexPath.row - 1) >= 0){
            feedback_dt_last = feedbacksData[indexPath.row - 1]
        }
        
        var feedback_dt_next: FeedbackModel?
        if((indexPath.row + 1) <= feedbacksData.count-1){
            feedback_dt_next = feedbacksData[indexPath.row + 1]
        }
        
        
        feedbackCell.textMessageLabel.text = feedback_dt.text
        feedbackCell.timeMessageLabel.text = feedback_dt.date_time.toDateTimeStringRelative
        
        feedbackCell.leadingBgMessageThan.constant = self.view.bounds.width * 0.15
        feedbackCell.trailingBgMessageThan.constant = self.view.bounds.width * 0.15
        feedbackCell.trailingBgMessage.constant = 10
        feedbackCell.leadingBgMessage.constant = 10
        
        
        if(feedback_dt.author == 0) {
            //Sender - User
            feedbackCell.textMessageLabel.textColor = .white
            feedbackCell.timeMessageLabel.textColor = #colorLiteral(red: 0.9619765619, green: 0.9619765619, blue: 0.9619765619, alpha: 1)
            feedbackCell.timeMessageLabel.textAlignment = .right
            feedbackCell.bgMessage.backgroundColor = bubbleBlueColor
            
            feedbackCell.leadingBgMessageThan?.priority = .defaultHigh
            feedbackCell.leadingBgMessage?.priority = .defaultLow
            feedbackCell.trailingBgMessageThan?.priority = .defaultLow
            feedbackCell.trailingBgMessage?.priority = .defaultHigh
            
            
            feedbackCell.iconSupport.isHidden = true
            feedbackCell.bgMessage.cornerRadius = 18
            
            if let feedback_dt_last = feedback_dt_last {
                if(feedback_dt_last.author == 0) {
                    feedbackCell.bottomBgMessage.constant = 3
                    feedbackCell.bgMessage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                    
                } else {
                    feedbackCell.bottomBgMessage.constant = 20
                    feedbackCell.bgMessage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
                }
            } else {
                feedbackCell.bottomBgMessage.constant = 20
                feedbackCell.bgMessage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner]
            }
            feedbackCell.topBgMessage.constant = 0
            feedbackCell.newMessageLabel.isHidden = true
            
        } else {
            //Sender - Support
            feedbackCell.textMessageLabel.textColor = .black
            feedbackCell.timeMessageLabel.textColor = .darkGray
            feedbackCell.bgMessage.backgroundColor = systemGray6
            
            
            feedbackCell.leadingBgMessageThan?.priority = .defaultLow
            feedbackCell.leadingBgMessage?.priority = .defaultHigh
            feedbackCell.trailingBgMessageThan?.priority = .defaultHigh
            feedbackCell.trailingBgMessage?.priority = .defaultLow
            
            
            feedbackCell.bgMessage.cornerRadius = 18
            if let feedback_dt_last = feedback_dt_last {
                if(feedback_dt_last.author == 1) {
                    feedbackCell.iconSupport.isHidden = true
                    feedbackCell.bottomBgMessage.constant = 3
                    feedbackCell.bgMessage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                } else {
                    feedbackCell.bottomBgMessage.constant = 20
                    feedbackCell.iconSupport.isHidden = false
                    feedbackCell.bgMessage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
                }
            } else {
                feedbackCell.bottomBgMessage.constant = 20
                feedbackCell.iconSupport.isHidden = false
                feedbackCell.bgMessage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            }
            
            //NEW MESSAGES
            if(feedback_dt.is_read == 1){
                feedbackCell.topBgMessage.constant = 0
                feedbackCell.newMessageLabel.isHidden = true
            } else {
                if let feedback_dt_next = feedback_dt_next {
                    if(feedback_dt_next.is_read == 0) {
                        feedbackCell.topBgMessage.constant = 0
                        feedbackCell.newMessageLabel.isHidden = true
                    } else {
                        feedbackCell.topBgMessage.constant = 10
                        feedbackCell.newMessageLabel.isHidden = false
                    }
                }
                //                if let feedback_dt_next = feedback_dt_next {
                //                    if (feedback_dt_next.is_read == 0) {
                //                        feedbackCell.topBgMessage.constant = 0
                //                        feedbackCell.newMessageLabel.isHidden = true
                //                    } else {
                //                        feedbackCell.topBgMessage.constant = 10
                //                        feedbackCell.newMessageLabel.isHidden = false
                //                    }
                //                }  else {
                //                    feedbackCell.topBgMessage.constant = 10
                //                    feedbackCell.newMessageLabel.isHidden = false
                //                }
            }
        }
        
        feedbackCell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        return feedbackCell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


class FeedbackDTTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var textMessageLabel: UILabel!
    @IBOutlet weak var topBgMessage: NSLayoutConstraint!
    @IBOutlet weak var bottomBgMessage: NSLayoutConstraint!
    @IBOutlet weak var leadingBgMessage: NSLayoutConstraint!
    @IBOutlet weak var leadingBgMessageThan: NSLayoutConstraint!
    @IBOutlet weak var trailingBgMessage: NSLayoutConstraint!
    @IBOutlet weak var trailingBgMessageThan: NSLayoutConstraint!
    @IBOutlet weak var bgMessage: UIView!
    @IBOutlet weak var iconSupport: UIImageView!
    @IBOutlet weak var timeMessageLabel: UILabel!
    @IBOutlet weak var newMessageLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

