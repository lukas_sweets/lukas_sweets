//
//  SettingsViewController.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 05.04.2023.
//

import UIKit

struct SettingStruct {
    let id: Int
    let section: String
    let data: [SettingDataStruct]
}

struct SettingDataStruct {
    let id: Int
    let label: String
    var switchButton: Bool = false
}



class SettingsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var deleteProfileButton: UIButton!
    
    private var data: [SettingStruct] = [
        SettingStruct(id: 1, section: "push_notifications".localized, data: [
            SettingDataStruct(id: 1, label: "notifications_promo".localized, switchButton: true),
            SettingDataStruct(id: 3, label: "notifications_feedback".localized, switchButton: true),
        ]),
        SettingStruct(id: 2, section: "email_notifications".localized, data: [
            SettingDataStruct(id: 2, label: "notifications_eco_checks".localized, switchButton: true)]),
    ]
    
    private var user: UserModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        getUser()
    }
    
    private func getUser() {
        Task {
            user = await UserRepository.getUser()
            tableView.reloadData()
        }
    }
    
    
    @IBAction func deleteAccount(_ sender: UIButton) {
        let alert = UIAlertController(title: "delete_card".localized, message: "delete_card_body".localized, preferredStyle: .actionSheet)
        alert.addAction(.init(title: "delete".localized, style: .destructive){
            action in
            self.deleteCard()
        })
        
        alert.addAction(.init(title: "cancel".localized, style: .cancel){
            action in
            
        })
        self.present(alert, animated: true)
    }
    
    private func deleteCard() {
        Task {
            switch await CardRepository.deleteCard() {
            case .success:
                RealmDatabase.shared.deleteAll()
                Preference.instance.deleteAll()
                let vc = UIStoryboard(name: "Storyboard", bundle: nil).instantiateViewController(withIdentifier: "LaunchViewController")
                self.navigationController?.setViewControllers([vc], animated: false)
                break
            case .internet:
                Alert.noInternet()
                break
            default:
                Alert.serverError()
                break
            }
        }
    }
    
    
    @objc private func switchChangedEco(_ sender: UISwitch) {
        Task {
            switch await UserRepository.updateUserEcoCheck(send: sender.isOn) {
            case .internet:
                Alert.noInternet()
                break
            case .error:
                Alert.serverError()
                break
            default:
                break
            }
        }
    }
    
    @objc private func switchChangedFeedbackNotifications(_ sender: UISwitch) {
        Task {
            switch await UserRepository.updateUserFeedbackNotifications(send: sender.isOn) {
            case .internet:
                Alert.noInternet()
                break
            case .error:
                Alert.serverError()
                break
            default:
                break
            }
        }
    }
    
    @objc private func switchChangedPromoNotifications(_ sender: UISwitch) {
        Task {
            switch await UserRepository.updateUserPromoNotifications(send: sender.isOn) {
            case .internet:
                Alert.noInternet()
                break
            case .error:
                Alert.serverError()
                break
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.whiteNavigationBar()
    }
    
    
}



extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].data.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingsTableViewCell
        let data = data[indexPath.section].data[indexPath.row]
        cell.label.text = data.label
        
        Log.e(data.id)
        
        switch data.id {
        case 1:
            if let user = user {
                cell.switchButton.isOn = user.promo_notifications
                cell.switchButton.addTarget(self, action: #selector(switchChangedPromoNotifications(_:)), for: .valueChanged)
                cell.switchButton.tag = 1
            }
            break
        case 2:
            if let user = user {
                cell.switchButton.isOn = user.send_eco_check
                cell.switchButton.addTarget(self, action: #selector(switchChangedEco(_:)), for: .valueChanged)
                cell.switchButton.tag = 2
            }
            break
        case 3:
            if let user = user {
                cell.switchButton.isOn = user.feedback_notifications
                cell.switchButton.addTarget(self, action: #selector(switchChangedFeedbackNotifications(_:)), for: .valueChanged)
                cell.switchButton.tag = 3
            }
            break
        default:
            break
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].section
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 1 {
            return "eco_checks_footer".localized
        }
        return nil
    }
}
