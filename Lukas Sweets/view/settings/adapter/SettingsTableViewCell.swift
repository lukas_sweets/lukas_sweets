//
//  SettingsTableViewCell.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 05.04.2023.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
