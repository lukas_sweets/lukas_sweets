//
//  CardBarcodeViewController.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 17.03.2023.
//

import UIKit


class CardBarcodeViewController: UIViewController {
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var barcodeImageView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let bonus_card_number = Preference.instance.bonus_card_number else { return }
        cardNumberLabel.text = bonus_card_number
        barcodeImageView.image = BarcodeGenerator.generate(from: bonus_card_number, descriptor: .code128, size: barcodeImageView.frame.size)
        
    }
}
