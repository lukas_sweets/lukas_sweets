//
//  MenuViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 27.01.2020.
//

import UIKit
import SwiftMessages
import BadgeHub

final class MenuViewController: UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var cityBarItem: UIBarButtonItem!
    
    struct Menu {
        let id: Int
        let label: String
        let img: UIImage?
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    private var tapForDeveloper = 0
        
    private var dataSource: [Menu] = []
    private var versions = ""
    private var badgeSupport = 0
    private var lastIndex: IndexPath!

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: 0, right: 0)
        
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versions = version
        }
        
        loadResource()
        
        
    }
    
    
    private func loadResource () {
        dataSource.removeAll()
        dataSource.append(Menu(id: 1, label: "profile".localized, img: UIImage(named: "ic_profile")))
        dataSource.append(Menu(id: 2, label: "stores".localized, img: UIImage(named: "ic_stores")))
        dataSource.append(Menu(id: 5, label: "support".localized, img: UIImage(named: "ic_support")))
        dataSource.append(Menu(id: 7, label: "settings".localized, img: UIImage(named: "ic_settings")))
        dataSource.append(Menu(id: 6, label: "exit".localized, img: UIImage(named: "ic_exit")))

    }
    
    private func employeeCard() {
        let isExist = !dataSource.filter({ $0.id == 3 }).isEmpty
        let indexPath = [IndexPath(row: 2, section: 0)]

        tableView.performBatchUpdates { [unowned self] in
            if Preference.instance.employee_card_number.isEmpty && isExist {
                dataSource.removeAll(where: {$0.id == 3})
                tableView.deleteRows(at: indexPath, with: .automatic)
            } else if !Preference.instance.employee_card_number.isEmpty && !isExist {
                dataSource.insert(Menu(id: 3, label: "employee_card".localized, img: UIImage(named: "ic_employee_card")), at: 2)
                tableView.insertRows(at: indexPath, with: .automatic)
            }
        }
    }
    
    
    
    private func checkNewFeedback() {
        Task {
            badgeSupport = await FeedbackRepository.getNewMessages()
            var indexPath = [IndexPath(row: 2, section: 0)]
            
            // If user have employee card - change index path
            if !dataSource.filter({ $0.id == 3 }).isEmpty {
                indexPath = [IndexPath(row: 3, section: 0)]
            }

            tableView.performBatchUpdates{ [unowned self] in
                tableView.reloadRows(at: indexPath, with: .none)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        setTabBar()
        checkNewFeedback()
        employeeCard()
    }
    
    
    private func setTabBar() {
        
        navigationBar.shadowImage = UIImage()
        Task {
            let userCity = await UserRepository.getUserCity()
            cityBarItem.customView = setupCityButton(userCity)
        }
    }
    
    private func setupCityButton(_ city: String? = nil) -> UIView {//UIBarButtonItem{
        var configuration = UIButton.Configuration.plain()
        let container = AttributeContainer()
        configuration.attributedTitle = AttributedString(city ?? "your_city".localized, attributes: container)
        configuration.image = UIImage(named: "ic_location")
        configuration.imagePadding = 5
        let button = UIButton(configuration: configuration, primaryAction: nil)
        button.tintColor = .gray
        button.addTarget(self, action: #selector(openStores), for: .touchUpInside)
        return button
    }
    
    @objc private func openStores() {
        let vc = UIStoryboard(name: "Store", bundle: nil).instantiateViewController(withIdentifier: "StoresViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary =  defaults.dictionaryRepresentation()
        dictionary.keys.forEach{key in
            defaults.removeObject(forKey: key)
        }
    }
    
    private func alertExit () {
        let alert = UIAlertController(title: nil, message: "menu_exit_text".localized, preferredStyle: .actionSheet)
        alert.addAction(.init(title: "menu_exit_button".localized, style: .destructive){
            action in
            RealmDatabase.shared.deleteAll()
            Preference.instance.deleteAll()
            let vc = UIStoryboard(name: "Storyboard", bundle: nil).instantiateViewController(withIdentifier: "LaunchViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        })

        alert.addAction(.init(title: "cancel".localized, style: .cancel))
        self.present(alert, animated: true)
    }
    
    
    @objc func tapDeveloper (_ gestureRecognizer: UITapGestureRecognizer) {
        
        tapForDeveloper = tapForDeveloper + 1
        
        if(tapForDeveloper > 10) {
            developer()
            self.tapForDeveloper = 0
        }
        
    }
    
    private func developer() {
        let view: Sheets = try! SwiftMessages.viewFromNib()
        view.configureDropShadow()
        view.titleLabel?.text = "From LUKAS with LOVE"
        view.bodyLabel?.text = "Developer: Maksim Khaidarov"
        view.raccoon.visibility = .visible
        view.cancelButton.isHidden = true
        view.cancelAction = {
            SwiftMessages.hide()
        }
        
        view.okAction = {
            SwiftMessages.hide()
        }
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.preferredStatusBarStyle = .lightContent
        config.duration = .forever
        config.presentationStyle = .bottom
        config.dimMode = .gray(interactive: true)
        SwiftMessages.show(config: config, view: view)
    }
    
    
    
    private func showBarcodeBottomSheet() {
        do {
            let view: BarcodeEmployeeBottomSheet = try SwiftMessages.viewFromNib()
            view.configureDropShadow()
            view.bodyLabel?.text = Preference.instance.employee_card_number
            view.iconImageView?.image = BarcodeGenerator.generate(from: Preference.instance.employee_card_number, descriptor: .code128, size: CGSize(width: view.iconImageView?.frame.width ?? 20, height: view.iconImageView?.frame.height ?? 100))
            view.cornerRadius = 32
            
            var config = SwiftMessages.defaultConfig
            config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            config.preferredStatusBarStyle = .default
            config.duration = .forever
            config.presentationStyle = .bottom
            config.dimMode = .gray(interactive: true)
            SwiftMessages.show(config: config, view: view)
        } catch {
        }
    }
    
    // END
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let menuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? MenuTableViewCell else { return UITableViewCell() }
        let menu = dataSource[indexPath.row]
        
        
        
        menuCell.nameLabel.text = menu.label
        menuCell.menuIconView.icon = menu.img
        
        //Badge
        if(dataSource[indexPath.row].id == 5) {
            let hub = BadgeHub(view: menuCell.menuIconView) // Initially count set to 0
            hub.setCount(badgeSupport)
            hub.scaleCircleSize(by: 0.4)
            hub.setCircleColor(.red, label: .red)
        }
        
        return menuCell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch dataSource[indexPath.row].id {
        case 1:
            let vc = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController")
            self.navigationController?.pushViewController(vc, animated: true)

            
        case 2:
            openStores()
            break
        case 3: showBarcodeBottomSheet()
        case 4: performSegue(withIdentifier: "showCompany", sender: self)
        case 5:
            let vc = UIStoryboard(name: "Support", bundle: nil).instantiateViewController(withIdentifier: "SupportViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        case 6: alertExit()
        case 7:
            let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView()
        footerView.backgroundColor = .clear
        footerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 50.2)
        
        let vers = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width-30, height: 50.2))
        
        vers.textAlignment = .center
        
        vers.font = UIFont.systemFont(ofSize: 13)
        vers.textColor = .gray
        vers.text = "Version: " + versions
        
        footerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapDeveloper(_:))))
        footerView.addSubview(vers)
        
        return footerView
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let sections = tableView.numberOfSections-1
        if (sections == section) {
            return 80
        } else {
            return 0
        }
    }
    
}
