//
//  MenuRoundIcon.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 14.03.2023.
//

import UIKit

@IBDesignable
final class RoundIcon: UIView {
    
    override init(frame: CGRect){
            super.init(frame: frame)
        }

        required init?(coder: NSCoder) {
            super.init(coder: coder)

//            fatalError("init(coder:) has not been implemented")
        }
    
    private var iconImage = UIImage()
    private var iconBackgroundColor: UIColor = .backgroundGray
    private var tint: UIColor = .gray
    private var padding: CGFloat = 12.0
    
    @IBInspectable var icon: UIImage? {
            didSet {
                guard let icon = icon else { return }
                self.iconImage = icon
            }
        }
    
    @IBInspectable var background: UIColor? {
            didSet {
                guard let backgroundColor = background else { return }
                self.iconBackgroundColor = backgroundColor
            }
        }
    
    @IBInspectable var tintIcon: UIColor? {
            didSet {
                guard let tint = tintIcon else { return }
                self.tintColor = tint
            }
        }
    
    @IBInspectable var paddingIcon: CGFloat = 12.0 {
            didSet {
                self.padding = paddingIcon
            }
        }

        override func layoutSubviews() {
            super.layoutSubviews()
            setup()
        }

        private func setup() {
            self.layer.cornerRadius = self.frame.size.width / 2.0
            self.layer.backgroundColor = iconBackgroundColor.cgColor
            
            let width = self.frame.width-(padding*2)
            let height = self.frame.height-(padding*2)
            
            let iconImageView = UIImageView(frame: CGRect(x: padding, y: padding, width: width, height: height))
            iconImageView.contentMode = UIView.ContentMode.scaleAspectFit
            iconImageView.frame.size.width = width
            iconImageView.frame.size.height = height
            iconImageView.image = iconImage
            iconImageView.tintColor = tint
            self.addSubview(iconImageView)
        }
    
}
