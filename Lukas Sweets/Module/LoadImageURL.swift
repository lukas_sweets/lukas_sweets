//
//  LoadImageURL.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 03.02.2020.
//

import UIKit
//import SGImageCache
import SDWebImage

public extension UIImageView {
    func loadImage(fromURL url: String) {
        guard let _ = URL(string: url) else {
            return
        }
        
        self.sd_setImage(with: URL(string: url))

//        DispatchQueue.global(qos: .userInitiated).async {
//            if let image = SGImageCache.image(forURL: url) {
//                DispatchQueue.main.async {
//                    self.transition(toImage: image)
//                    //self.image = image   // image loaded immediately from cache
//                }
//            } else {
//                SGImageCache.getImage(url: url) { [weak self] image in
//                    DispatchQueue.main.async {
//                        //self?.image = image   // image loaded async
//                        self?.transition(toImage: image)
//                    }
//                }
//            }
//
//        }
        
        
//        let cache =  URLCache.shared
//        let request = URLRequest(url: imageURL)
//        DispatchQueue.global(qos: .userInitiated).async {
//            if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
//                DispatchQueue.main.async {
//                    print("Set from Cache")
//                    self.transition(toImage: image)
//                    //self.image = image
//                }
//            } else {
//                URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
//                    if let data = data, let response = response, ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300, let image = UIImage(data: data) {
//                        let cachedData = CachedURLResponse(response: response, data: data)
//                        cache.storeCachedResponse(cachedData, for: request)
//                        DispatchQueue.main.async {
//                            print("Set from uRL")
//                            self.transition(toImage: image)
//                            //self.image = image
//                        }
//                    }
//                }).resume()
//            }
//        }
    }
    
    func transition(toImage image: UIImage?) {
        UIView.transition(with: self, duration: 0.3,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.image = image
                          },
                          completion: nil)
    }
}






