//
//  Alert.swift
//  Lukas Sweets
//
//  Created by Максим on 20.01.2021.
//

import Foundation

import SwiftMessages
import SPIndicator

class Alert {
    
    static func noInternet () {
        SPIndicator.present(title: "alert_no_internet".localized, preset: .error, haptic: .error)
    }
    
    static func serverError() {
        SPIndicator.present(title: "alert_error".localized, preset: .error, haptic: .error)
        
    }
    static func saved() {
        SPIndicator.present(title: "alert_saved".localized, preset: .done, haptic: .success)
    }
    
    static func feedbcakSended() {
        SPIndicator.present(title: "feedback_send".localized, preset: .done, haptic: .success)
    }
    
    static func noFind() {
        SPIndicator.present(title: "no_find".localized, preset: .error, haptic: .error)
    }
    
    
    
    
    static func raccoon(statusBarStyle: UIStatusBarStyle) {
        let view: Sheets = try! SwiftMessages.viewFromNib()
        view.configureDropShadow()
        view.titleLabel?.text = "alert_no_connect_to_server_title".localized
        view.bodyLabel?.text = "alert_no_connect_to_server_text".localized
        view.raccoon.visibility = .visible
        view.cancelButton.isHidden = true
        
        
        view.okAction = {
            SwiftMessages.hide()
        }
        
        
        SwiftMessages.defaultConfig.preferredStatusBarStyle = statusBarStyle
        SwiftMessages.defaultConfig.duration = .forever
        SwiftMessages.defaultConfig.presentationStyle = .bottom
        SwiftMessages.defaultConfig.dimMode = .gray(interactive: true)
        
        
        SwiftMessages.show(view: view)
        
        
        
    }
    
    
    
    static func alert (title: String, body: String, statusBarStyle: UIStatusBarStyle) {
        let view: Sheets = try! SwiftMessages.viewFromNib()
        view.configureDropShadow()
        view.titleLabel?.text = title
        view.bodyLabel?.text = body
        view.raccoon.visibility = .gone
        view.cancelButton.isHidden = true
        
        
        view.okAction = {
            SwiftMessages.hide()
        }
        
        SwiftMessages.defaultConfig.preferredStatusBarStyle = statusBarStyle
        SwiftMessages.defaultConfig.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.defaultConfig.duration = .forever
        SwiftMessages.defaultConfig.presentationStyle = .bottom
        SwiftMessages.defaultConfig.dimMode = .gray(interactive: true)
        SwiftMessages.show(view: view)
    }
    
    
    
//    static func showBarcodeBottomSheet(cardNumber: String, isBonusCard: Bool = true) {
//        do {
//            let view: BarcodeBottomSheet = try SwiftMessages.viewFromNib()
//            view.configureDropShadow()
//            view.bodyLabel?.text = cardNumber
//            view.iconImageView?.image = BarcodeGenerator.generate(from: cardNumber, descriptor: .code128, size: CGSize(width: view.iconImageView?.frame.width ?? 20, height: view.iconImageView?.frame.height ?? 100))
//            view.cornerRadius = 32
//            
//            var config = SwiftMessages.defaultConfig
//            config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
//            config.preferredStatusBarStyle = .default
//            config.duration = .forever
//            config.presentationStyle = .bottom
//            config.dimMode = .gray(interactive: true)
//            SwiftMessages.show(config: config, view: view)
//        } catch {
//            
//        }
//        
//    }
    
    
    
}
