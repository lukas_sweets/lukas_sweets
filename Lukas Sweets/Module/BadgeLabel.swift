//
//  BadgeLabel.swift
//  Lukas Sweets
//
//  Created by Максим on 20.09.2021.
//

import UIKit


class BadgeLabel: UILabel {
    
    //var text: String
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()

    }
    
    
    override init(frame: CGRect) {
           super.init(frame: frame)
           self.commonInit()
       }

    
    func commonInit(){
        self.layer.cornerRadius = self.bounds.width/2
        self.clipsToBounds = true
        self.textColor = .white
        self.backgroundColor = .red
        
       }
    
//    override func drawText(in rect: CGRect) {
//        let insets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
//        super.drawText(in: rect.inset(by: insets))
//       }

//       func setProperties(borderWidth: Float, borderColor: UIColor) {
//           self.layer.borderWidth = CGFloat(borderWidth)
//           self.layer.borderColor = borderColor.CGColor
//       }
}
