//
//  Attribute.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 19.03.2020.
//

import UIKit

//FF5349
let redColor = #colorLiteral(red: 1, green: 0.3254901961, blue: 0.2862745098, alpha: 1)

//02C01A
let greenColor = #colorLiteral(red: 0.007843137255, green: 0.7529411765, blue: 0.1019607843, alpha: 1)

//FFDD00
let yellowColor = #colorLiteral(red: 1, green: 0.7450980392, blue: 0, alpha: 1)

let systemGray6 = #colorLiteral(red: 0.9117930065, green: 0.9182942145, blue: 0.9377978383, alpha: 1)

//F2F2F7
let backgroundGray = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)

let bubbleBlueColor = #colorLiteral(red: 0.09411764706, green: 0.5568627451, blue: 0.9764705882, alpha: 1)

let christmasBg = #colorLiteral(red: 0.4392156863, green: 0.7176470588, blue: 0.9137254902, alpha: 1)
let christmasLabelColor = #colorLiteral(red: 0.3960784314, green: 0.6549019608, blue: 0.8431372549, alpha: 1)

// Week's cake
private let firstWeekCake = #colorLiteral(red: 0.6509803922, green: 0.8392156863, blue: 0.8784313725, alpha: 1)
private let secondWeekCake = #colorLiteral(red: 0.9294117647, green: 0.8156862745, blue: 0.8156862745, alpha: 1)
private let thirdWeekCake = #colorLiteral(red: 0.8156862745, green: 0.8784313725, blue: 0.6509803922, alpha: 1)
private let fourthWeekCake = #colorLiteral(red: 0.7568627451, green: 0.6509803922, blue: 0.8784313725, alpha: 1)
private let fifthWeekCake = #colorLiteral(red: 0.8784313725, green: 0.6509803922, blue: 0.6509803922, alpha: 1)

private let weekCakeColors = [firstWeekCake, secondWeekCake, thirdWeekCake, fourthWeekCake, fifthWeekCake]
let colorsArrayWeekCake = (0..<(5*weekCakeColors.count)).map{weekCakeColors[$0%weekCakeColors.count]}



// Lucky Wheel
let redLuckyWheel = #colorLiteral(red: 0.9215686275, green: 0.2588235294, blue: 0.1411764706, alpha: 1)
let yellowLuckyWheel = #colorLiteral(red: 1, green: 0.8823529412, blue: 0, alpha: 1)
let greenLuckyWheel = #colorLiteral(red: 0.1529411765, green: 0.7725490196, blue: 0.2862745098, alpha: 1)
let blueLuckyWheel = #colorLiteral(red: 0.1294117647, green: 0.5215686275, blue: 0.862745098, alpha: 1)
let borderLuckyWheel = #colorLiteral(red: 0.8745098039, green: 0.1019607843, blue: 0.1294117647, alpha: 1)

private let wheelColors = [redLuckyWheel, greenLuckyWheel, yellowLuckyWheel, blueLuckyWheel]
let colorsArrayLuckyWheel = (0..<(5*wheelColors.count)).map{wheelColors[$0%wheelColors.count]}


//  Coupons
private let blueCoupons = #colorLiteral(red: 0.8428676724, green: 0.9079454541, blue: 0.8766802549, alpha: 1)
private let greenCoupons = #colorLiteral(red: 0.9356514215, green: 0.8763770461, blue: 0.8774743676, alpha: 1)
private let pinkCoupons = #colorLiteral(red: 0.8760221004, green: 0.9109334946, blue: 0.7975168824, alpha: 1)
private let grayCoupons = #colorLiteral(red: 0.7984829545, green: 0.9092794061, blue: 0.8773319125, alpha: 1)
private let blackCoupons = #colorLiteral(red: 0.8948472142, green: 0.7964763045, blue: 0.9097955823, alpha: 1)
//private let redCoupons = #colorLiteral(red: 0.7960784314, green: 0.007843137255, blue: 0.2509803922, alpha: 1)

private let couponsColors = [blueCoupons, greenCoupons, pinkCoupons, grayCoupons, blackCoupons]
let colorsArrayCoupons = (0..<(100*couponsColors.count)).map{couponsColors[$0%couponsColors.count]}




public func newYear() -> Bool {
    let dateFormatter = DateFormatter()

    dateFormatter.dateFormat = "dd.MM.yyyy"
    
    let startDate = dateFormatter.date(from: "20.12.2022") as NSDate?
    let endDate = dateFormatter.date(from: "14.01.2023") as NSDate?

    return NSDate().isBetween(date: startDate!, andDate: endDate!)
    
}
