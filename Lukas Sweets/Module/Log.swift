//
//  Log.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 08.04.2023.
//

class Log {
    static func e(_ text: Any?) {
        print("LOG: \(text)")
    }
}
