//
//  CustomTextField.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 28.03.2023.
//

import UIKit

@IBDesignable
final class CustomTextField: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var itemLabel: UILabel!
    @IBOutlet var itemField: UITextField!
    @IBOutlet var bgView: UIView!
    @IBOutlet var itemFootNote: UILabel!
    @IBOutlet var itemIconView: UIView!
    @IBOutlet var itemIcon: UIImageView!
    @IBOutlet var itemMenuButton: UIButton!
    
    
    private var alert = UIAlertController()
    private var itemDatePicker = UIDatePicker()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        mainSetup()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        mainSetup()
        
        //            fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    var field: UITextField? {
        didSet {}
    }
    
    
    
    var tapGestureRecognizer: UITapGestureRecognizer?
    var selector: Void?
    var view: UIViewController?
    
    
    var text: String? {
        didSet {
            if let itemField = itemField {
                itemField.text = text
                if let text = text {
                    itemDatePicker.date = text.toDateTime(format: "dd.MM.yyyy")
                }
            }
        }
    }
    
    var textTrim: String {
        get {
            return itemField.text?.trim() ?? ""
        }
    }
    
    @IBInspectable var labelText: String = "" {
        didSet {}
    }
    
    @IBInspectable var requiredValue: Bool = false {
        didSet {}
    }
    
    @IBInspectable var footNote: String = "" {
        didSet {}
    }
    
    @IBInspectable var enable: Bool = true {
        didSet {}
    }
    
    @IBInspectable var rightIcon: UIImage? {
        didSet {}
    }
    
    @IBInspectable var datePicker: Bool = false {
        didSet {}
    }
    
    @IBInspectable var dropDown: Bool = false {
        didSet {}
    }
    
    var isEmpty: Bool {
        if let text = itemField.text?.trim() {
            return text.isEmpty
        } else {
            return true
        }
    }
    
    var isEdit: Bool = false {
        didSet {
            setEdited(isEdit: isEdit)
        }
    }
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    private func mainSetup() {
        Bundle.main.loadNibNamed("CustomTextField", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        itemField.delegate = self
    }
    
    private func setup() {
        itemLabel.text = labelText
        itemFootNote.isHidden = footNote.isEmpty
        itemFootNote.text = footNote
        itemField.isEnabled = enable
        bgView.borderWidth = 1
        
        
        field = itemField
        itemField.text = text
        
        setupRightIcon()
        setupDatePicker()
        setupDropDown()
        setupTouch()
        
        // Drop Down menu
        itemMenuButton.isHidden = true
    }
    
    @objc func startEdit(){
        if datePicker {
            setEdited(isEdit: true)
            view?.present(alert, animated: true)
        }
        else if enable {
            itemField.becomeFirstResponder()
        }
    }
    
    func setError() {
        bgView.borderColor = .red
        itemLabel.textColor = .red
    }
    
    
    
    private func setupDatePicker() {
        if datePicker {
            itemField.isEnabled = false
            
            alert = UIAlertController(title: "birthday".localized, message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
            itemDatePicker = UIDatePicker()
            itemDatePicker.frame = CGRect(x: 0, y: 0, width: alert.view.frame.width, height: alert.view.frame.height - 40)
            itemDatePicker.preferredDatePickerStyle = .wheels
            itemDatePicker.datePickerMode = .date
            itemDatePicker.locale = Locale(identifier: "locale_time".localized)
            itemDatePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -16, to: Date())
            itemDatePicker.center = CGPoint(x: alert.view.frame.maxX/2, y: (itemDatePicker.frame.height + 40)/2 )
            if let date = itemField.text {
                itemDatePicker.date = date.toDateTime(format: "dd.MM.yyyy")
            }
            
            alert.view.addSubview(itemDatePicker)
            
            let selectAction = UIAlertAction(title: "ok".localized, style: .default, handler: { _ in
                self.itemField.text = self.itemDatePicker.date.toFormatString(format: "dd.MM.yyyy")
                self.setEdited()
            })
            let cancelAction = UIAlertAction(title: "cancel".localized, style: .cancel, handler: { _ in
                self.setEdited()
            })
            let deleteAction = UIAlertAction(title: "clear".localized, style: .destructive, handler: {_ in
                self.itemField.text = ""
                self.setEdited()
            })
            
            alert.addAction(selectAction)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
        }
    }
    
    private func setupRightIcon() {
        if let rightIcon = rightIcon {
            itemIconView.isHidden = false
            itemIcon.image = rightIcon
        } else {
            itemIconView.isHidden = true
        }
    }
    
    private func setupDropDown() {
        if dropDown {
            itemIcon.image = UIImage(systemName: "chevron.right")
            itemIcon.tintColor = .systemGray2
            itemIcon.contentMode = .scaleAspectFill
            itemIcon.heightAnchor.constraint(equalToConstant: 15).isActive = true
            itemIcon.widthAnchor.constraint(equalToConstant: 11).isActive = true
            itemIconView.isHidden = false
        }
    }
    
    private func setupTouch() {
        if let tapGestureRecognizer = tapGestureRecognizer {
            bgView.isUserInteractionEnabled = true
            bgView.addGestureRecognizer(tapGestureRecognizer)
            itemField.isEnabled = false
        } else {
            let tap = UITapGestureRecognizer(target: self, action: #selector(startEdit))
            tap.numberOfTapsRequired = 1
            bgView.isUserInteractionEnabled = true
            bgView.addGestureRecognizer(tap)
        }
        
        
    }
    
    private func setEdited(isEdit: Bool = false) {
        if let bgView = bgView, let itemLabel = itemLabel {
            if isEdit {
                bgView.borderColor = .blue
                itemLabel.textColor = .blue
            } else {
                bgView.borderColor = .clear
                itemLabel.textColor = .gray2
            }
        }
    }
}


extension CustomTextField: UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        text = ""
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        setEdited(isEdit: true)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let text = itemField.text?.trim() {
            if requiredValue && text.isEmpty {
                setError()
            } else {
                setEdited()
            }
        } else {
            if requiredValue {
                setError()
            }
        }
        
        return true
    }
    
    
    
    
    
    
    
}
