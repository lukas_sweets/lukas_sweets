//
//  Network.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//
import Alamofire

struct ApiError {
    var code: ResponseCode? = nil
    let message: String?
    var responseCode: Int? = nil
}


enum Result<T> {
    case success(T)
    case internet(_:T?)
    case error(_: ApiError?)
}



class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
