//
//  ResponceCode.swift
//  Lukas Sweets
//
//  Created by Максим on 18.01.2021.
//

import Foundation
import UIKit

public let RESPONCE_CODE_ERROR = 0
public let RESPONCE_CODE_OK = 1
public let RESPONCE_CODE_NO_CONNECT = 2


public let RESPONCE_CODE_NO_CARD = 3

public let RESPONCE_CODE_NO_HISTORY = 4

public let RESPONCE_CODE_TOKEN_IS_FAIL = 5

public let RESPONCE_CODE_NO_INTERNET = 6


enum ResponseCode {
    case ok
    case error
    case error_device
    case no_card
}
