//
//  Preference.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 12.03.2023.
//

import Foundation

protocol PreferenceProtocol {
    static var phone_number: String { get set }
    var bonus_card_number: String? { get set }
    var employee_card_number: String { get set }
    var bonus_card_balance: Int { get set }
    var checks_count: Int { get set }
    static var show_eco_check: Bool { get set }
    static var push_token: String { get set }
}

class Preference: PreferenceProtocol {
    static let instance = Preference()
    
    private init() {}
    
    
    static var phone_number: String {
        get {
            return UserDefaults.standard.string(forKey: "phone_number") ?? ""
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "phone_number")
        }
    }
    
    var bonus_card_number: String? {
        get {
            return UserDefaults.standard.string(forKey: "bonus_card_number")
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "bonus_card_number")
        }
    }
    
    var employee_card_number: String {
        get {
            return UserDefaults.standard.string(forKey: "employee_card_number") ?? ""
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "employee_card_number")
        }
    }
    
    var bonus_card_balance: Int {
        get {
            return UserDefaults.standard.integer(forKey: "bonus_card_balance")
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "bonus_card_balance")
        }
    }
    
    var checks_count: Int {
        get {
            return UserDefaults.standard.integer(forKey: "checks_count")
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "checks_count")
        }
    }
    
    static var show_eco_check: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "show_eco_check")
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "show_eco_check")
        }
    }
    
    static var push_token: String {
        get {
            return UserDefaults.standard.string(forKey: "push_token") ?? ""
        }
        
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "push_token")
        }
    }
    
    func deleteAll() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
}
