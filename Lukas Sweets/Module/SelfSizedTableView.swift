//
//  SelfSizedTableView.swift
//  Lukas Sweets
//
//  Created by Максим on 17.05.2021.
//

import UIKit

class SelfSizedTableView: UITableView {
    
    //    override open var contentSize: CGSize {
    //        didSet { // basically the contentSize gets changed each time a cell is added
    //            // --> the intrinsicContentSize gets also changed leading to smooth size update
    //            if oldValue != contentSize {
    //                invalidateIntrinsicContentSize()
    //            }
    //        }
    //    }
    //
    //    override open var intrinsicContentSize: CGSize {
    //        return CGSize(width: contentSize.width, height: contentSize.height)
    //    }
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}


