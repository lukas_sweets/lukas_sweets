import UIKit
import SwiftMessages

enum VersionError: Error {
    case invalidBundleInfo, invalidResponse
}

class LookupResult: Decodable {
    var results: [AppInfo]
}

class AppInfo: Decodable {
    var version: String
    var trackViewUrl: String
    //let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String,
    // You can add many thing based on "http://itunes.apple.com/lookup?bundleId=\(identifier)"  response
    // here version and trackViewUrl are key of URL response
    // so you can add all key beased on your requirement.
    
}

class ArgAppUpdater: NSObject {
    private static var _instance: ArgAppUpdater?;
    
    private override init() {
        
    }
    
    public static func getSingleton() -> ArgAppUpdater {
        if (ArgAppUpdater._instance == nil) {
            ArgAppUpdater._instance = ArgAppUpdater.init();
        }
        return ArgAppUpdater._instance!;
    }
    
    private func getAppInfo(completion: @escaping (AppInfo?, Error?) -> Void) -> URLSessionDataTask? {
        guard let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String,
              let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                  DispatchQueue.main.async {
                      completion(nil, VersionError.invalidBundleInfo)
                  }
                  return nil
              }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }
                
                let result = try JSONDecoder().decode(LookupResult.self, from: data)
                
                //let dictionary = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                //print("dictionary",dictionary!)
                
                
                guard let info = result.results.first else { throw VersionError.invalidResponse }
                // print("result:::",result)
                completion(info, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
        
        return task
    }
    private  func checkVersion(force: Bool) {
        let info = Bundle.main.infoDictionary
        let currentVersion = info?["CFBundleShortVersionString"] as? String
        _ = getAppInfo { (info, error) in
            
            let appStoreAppVersion = info?.version
            if let error = error {
                print(error)
                
                
                
            } else if appStoreAppVersion!.compare(currentVersion!, options: .numeric) == .orderedDescending {
                
                DispatchQueue.main.async {
                    if let topController = UIApplication.shared.windows.first?.rootViewController {
                        if let info = info {
                            topController.showAppUpdateAlert(Version: info.version, Force: force, AppURL: info.trackViewUrl)
                        }
                    }
                }
                
            }
        }
        
        
    }
    
    func showUpdateWithConfirmation() {
        checkVersion(force : false)
        
        
    }
    
    func showUpdateWithForce() {
        checkVersion(force : true)
    }
    
    
    
}

extension UIViewController {
    
    
    fileprivate func showAppUpdateAlert( Version : String, Force: Bool, AppURL: String) {
        
        
        let bundleName = Bundle.main.infoDictionary!["CFBundleName"] as! String;
        //      var alertMessage = "text_new_version".localized_with_argument(arg: bundleName, Version)
        //      let alertTitle = "new_version".localized
        
        
        let view: Sheets = try! SwiftMessages.viewFromNib()
        view.configureDropShadow()
        view.titleLabel?.text = "new_version".localized
        
        if Force {
            view.bodyLabel?.text = "force_text_new_version".localized_with_argument(arg: bundleName, Version)
            view.cancelButton.isHidden = true
        } else {
            view.bodyLabel?.text = "text_new_version".localized_with_argument(arg: bundleName, Version)
        }
        
        view.cancelAction = {
            SwiftMessages.hide()
        }
        
        view.okAction = {
            guard let url = URL(string: AppURL) else {
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
        view.okButton.setTitle("ok_new_version".localized, for: .normal)
        view.cancelButton.setTitle("cancel_new_version".localized, for: .normal)
        
        
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.preferredStatusBarStyle = .lightContent
        config.duration = .forever
        config.presentationStyle = .bottom
        config.dimMode = .gray(interactive: true)
        SwiftMessages.show(config: config, view: view)
        
        
        
        
        //      if Force {
        //          alertMessage = "force_text_new_version".localized_with_argument(arg: bundleName, Version)
        //      }
        //
        //
        //      let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        //
        //
        //      if !Force {
        //          let notNowButton = UIAlertAction(title: "cancel_new_version".localized, style: .default)
        //          alertController.addAction(notNowButton)
        //      }
        //
        //      let updateButton = UIAlertAction(title: "ok_new_version".localized, style: .default) { (action:UIAlertAction) in
        //          guard let url = URL(string: AppURL) else {
        //              return
        //          }
        //              UIApplication.shared.open(url, options: [:], completionHandler: nil)
        //      }
        //
        //      alertController.addAction(updateButton)
        //      self.present(alertController, animated: true, completion: nil)
    }
}
