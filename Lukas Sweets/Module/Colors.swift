//
//  Colors.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 20.03.2023.
//

import UIKit

extension UIColor {
    static var mockup: UIColor { return 
        #colorLiteral(red: 0.3135022521, green: 0.4092786312, blue: 0.7617240548, alpha: 1)
    }
    static var backgroundGray: UIColor { return
        #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
    }
    
    static var gray: UIColor { return
        #colorLiteral(red: 0.3568627451, green: 0.3803921569, blue: 0.4117647059, alpha: 1)
    }
    
    static var gray2: UIColor { return
        #colorLiteral(red: 0.6235294118, green: 0.6509803922, blue: 0.6823529412, alpha: 1)
    }
    
    static var blue: UIColor { return
        #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
    }
    
    static var selectedRed: UIColor {
        return #colorLiteral(red: 1, green: 0.8901960784, blue: 0.8862745098, alpha: 1)
    }
    
    static var red: UIColor {
        return #colorLiteral(red: 0.9333333333, green: 0.2509803922, blue: 0.2745098039, alpha: 1)
    }
    
    static var lime: UIColor {
        return #colorLiteral(red: 0.8941176471, green: 0.968627451, blue: 0.5333333333, alpha: 1)
    }
}
