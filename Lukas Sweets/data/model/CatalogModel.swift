//
//  CatalogModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

struct CatalogModel: Decodable {
    let categories: [CategoriesModel]
    let packs: [PacksProductsModel]
}


struct CategoriesModel: Decodable {
    let id: Int
    let name: String
    let icon: String
}

struct PacksProductsModel: Decodable, Hashable {
    let id: Int
    let name: String
    let image: String
    let category: String
}
