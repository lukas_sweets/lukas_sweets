//
//  NewsModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct NewsModel: Decodable {
    let title: String
    let validity: String
    let text_news: String
    let picture: String
    let button_show: Bool
    let button_text: String?
    let button_url: String?
}
