//
//  HistoryDetailsModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//


struct HistoryDetailsModel: Decodable {
    let id: Int
    let history_id: Int
    let goods_name: String
    let cnt: Double
    let sum_total: Double
}

extension HistoryDetailsModel {
    var modelDb: CheckDetailsDbModel {
        return CheckDetailsDbModel(id: self.id, checkID: self.history_id, goodsName: self.goods_name, cnt: self.cnt, sumTotal: self.sum_total)
    }
}
