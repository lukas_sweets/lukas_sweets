//
//  ProductAvailabilityModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct ProductAvailabilityModel: Decodable{
    let address: String
    let time_work: String
    let availabilities: Int
}

extension ProductAvailabilityModel {
    
    enum Availability {
        case InStock
        case Ends
        case OutOfStock
    }
    
    var availability: Availability {
        get {
            switch self.availabilities {
            case 0: return .OutOfStock//"availability_off".localized
            case 1 : return .Ends//"availability_min".localized
            case 2: return .InStock//"availability_ok".localized
            default:
                return .OutOfStock
            }
        }
    }
}
