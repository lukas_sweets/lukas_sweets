//
//  StockModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct StockModel: Decodable{
    let id: Int
    let name: String
    let short_name: String
    let descriptions: String
    let old_price: String?
    let new_price: String?
    let time: String
    let picture: String
    let top_position: Bool
    let show_label: Bool
    let label_text: String?
    let label_color: String?
    let label_bg_color: String?
    let order: Int?
    let products_pack_id: Int?
}


extension StockModel {
    var modelDb: StockDbModel {
        return StockDbModel(id: self.id, name: self.name, shortName: self.short_name, description: self.descriptions, oldPrice: self.old_price, newPrice: self.new_price, picture: self.picture, showLabel: self.show_label, labelColor: self.label_color, labelText: self.label_text, labelBgColor: self.label_bg_color, topPosition: self.top_position, order: self.order, productsPackId: self.products_pack_id, time: self.time)
    }
}
