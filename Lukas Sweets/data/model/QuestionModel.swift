//
//  QuestionModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 18.04.2023.
//

struct FAQ {
    var question: String
    var answer: String
}
