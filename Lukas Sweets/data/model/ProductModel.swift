//
//  ProductModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//


struct ProductModel: Decodable {
    let pack: PackModel
    let analog: [AnalogModel]
}


struct PackModel: Decodable {
    let product_name: String
    let article: String
    let image: String
    let name: String
    let short_name: String
    let description: String
    let category: String
    let expiration_date: String
    let energy_value: Double
    let composition: String
    let storage_conditions: String
    let proteins: Double
    let fats: Double
    let carbs: Double
    let more_nutritional_values: String?
    let allergens: [String]
    let price: Double?
}

struct AnalogModel: Decodable, Hashable{
    let id: Int
    let name: String
    var current: Bool = false
}


struct PackFindBarcodeModel: Decodable{
    let pack_id: Int
}
