//
//  StoreModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct StoreModel: Decodable {
    let id: Int
    let address: String
    let time_work: String
    let lat: String?
    let lon: String?
    let phone_number: String?
    let city_id: String?
}


extension StoreModel {
    var modelDb: StoreDbModel {
        return StoreDbModel(id: self.id, timeWork: self.time_work, lat: self.lat, lon: self.lon, address: self.address, phone: self.phone_number, cityID: self.city_id)
    }
}
