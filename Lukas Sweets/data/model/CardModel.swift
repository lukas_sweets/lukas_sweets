//
//  CardModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct CardModel: Decodable {
    var card_number: String
    var bonus_card: Int
    var purchase_amount: Double?
    var balance: Int?
    var loyal_size: Int
    
//    init (cardNumber: String, bonusCard: Int, purchaseAmount: Double?, balance: Int?, loyalSize: Int) {
//        self.card_number = cardNumber
//        self.bonus_card = bonusCard
//        self.purchase_amount = purchaseAmount
//        self.balance = balance
//        self.loyal_size = loyalSize
//    }
}

extension CardModel {
    var modelDb: CardDbModel {
        return CardDbModel(cardNumber: self.card_number, bonusCard: self.bonus_card, balance: self.balance, purchaseAmount: self.purchase_amount, loyalSize: self.loyal_size)
    }
}
