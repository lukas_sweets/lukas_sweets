//
//  UserCardsModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct UserCardsModel: Decodable {
    let user: UserModel
    let card: [CardModel] 
}
