//
//  UserModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct UserModel: Decodable, Encodable{
    let phone : String
    var name : String
    var surname : String?
    var patronymic : String?
    var birthday : String?
    var sex : Int
    var email : String?
    var send_eco_check : Bool
    var city_id: String?
    var promo_notifications: Bool
    var feedback_notifications: Bool
}

extension UserModel{
    var modelDb: UserDbModel {
        return UserDbModel(phone: self.phone, name: self.name, surname: self.surname, patronymic: self.patronymic, sex: self.sex, birthday: self.birthday, email: self.email, sendEcoCheck: self.send_eco_check, cityID: self.city_id, promoNotifications: self.promo_notifications, feedbackNotifications: self.feedback_notifications)
    }
}
