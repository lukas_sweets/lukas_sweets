//
//  HistoryModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation


struct HistoryModel: Decodable {
    let id: Int
    let date_time: String
    let bonus_total: Int
    let sum_total: Double
    let comment: String?
    let reason: Int
    let qr: String?
    let num: String
    let bonus_add: Int
    let pay_cash: Double
    let pay_card: Double
    let pay_bonus: Double
    let pay_certificate: Double
    let store: String?
}


extension HistoryModel {
    var modelDb: CheckDbModel {
        return CheckDbModel(id: self.id, dateTime: self.date_time, sumTotal: self.sum_total, bonusTotal: self.bonus_total, store: self.store, comment: self.comment, reason: self.reason, qr: self.qr, num: self.num, bonusAdd: self.bonus_add, payCash: self.pay_cash, payCard: self.pay_card, payBonus: self.pay_bonus, payCertificate: self.pay_certificate)
    }
}
