//
//  HistoryGeneralModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct HistoryGeneralModel: Decodable {
    let countHistory: Int
    let history: [HistoryModel]
}
