//
//  FeedbackModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct FeedbackModel: Decodable {
    let message_id: Int
    let feedback_id: Int
    let date_time: String
    let subject: Int
    let status: Int
    let text: String
    let author: Int
    let is_read: Int
}


extension FeedbackModel {
    var modelDb: FeedbackDbModel {
        return FeedbackDbModel(messageID: self.message_id, feedbackID: self.feedback_id, dateTime: self.date_time, subject: self.subject, status: self.status, text: self.text, author: self.author, isRead: self.is_read)
    }
}


struct NewFeedbackModel: Encodable {
    var phone: String = Preference.phone_number
    var name: String = ""
    var subject: Int = 0
    var city: String? = nil
    var store: Int? = nil
    var text: String = ""
    let platform: String = "iOS"
    let version: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
    let build: String = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
}
