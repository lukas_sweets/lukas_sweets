//
//  LuckyWheelModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

struct LuckyWheelCountModel: Decodable {
    let count: Int
    let coupons: Int
    let article: [LuckyWheelArticleModel]
}


struct LuckyWheelArticleModel: Decodable {
    let name: String
}

struct LuckyWheelCouponsModel: Decodable {
    let picture: String
    let name: String
    let date_to: String
    let store: String
    let is_used: Bool
    let is_off: Bool
    var color: Int?
}


struct LuckyWheelPlayModel: Decodable {
    let count: Int
    let coupons: Int
    let win_prise: LuckyWheelWinPriseModel
    let prises: [LuckyWheelPrisesModel]
}

struct LuckyWheelWinPriseModel: Decodable {
    let name: String
    let picture: String
    let angle: Float
    let is_bonus: Bool
}


struct LuckyWheelPrisesModel: Decodable{
    let name: String
    let icon: String
}
