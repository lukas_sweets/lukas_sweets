//
//  CheckDetailsDbModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 23.03.2023.
//

import RealmSwift

class CheckDetailsDbModel: Object {
    
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var checkID: Int = 0
    @Persisted var goodsName: String = ""
    @Persisted var cnt: Double = 0.0
    @Persisted var sumTotal: Double = 0.0
    
    convenience init(id: Int, checkID: Int, goodsName: String, cnt: Double, sumTotal: Double) {
        self.init()
        self.id = id
        self.checkID = checkID
        self.goodsName = goodsName
        self.cnt = cnt
        self.sumTotal = sumTotal
    }
    
}


extension CheckDetailsDbModel {
    var model: HistoryDetailsModel {
        return HistoryDetailsModel(id: self.id, history_id: self.checkID, goods_name: self.goodsName, cnt: self.cnt, sum_total: self.sumTotal)
    }
}
