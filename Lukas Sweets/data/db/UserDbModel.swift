//
//  UserDbModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 19.03.2023.
//

import RealmSwift

class UserDbModel: Object {
    
    @Persisted(primaryKey: true) var phone: String = ""
    @Persisted var name: String = ""
    @Persisted var surname: String? = ""
    @Persisted var patronymic: String? = ""
    @Persisted var sex: Int = 0
    @Persisted var birthday: String? = ""
    @Persisted var email: String? = ""
    @Persisted var sendEcoCheck: Bool = false
    @Persisted var cityID: String? = ""
    @Persisted var promoNotifications: Bool = true
    @Persisted var feedbackNotifications: Bool = true
    
    
    
    
    convenience init(phone: String, name: String, surname: String?, patronymic: String?, sex: Int, birthday: String?, email: String? , sendEcoCheck: Bool, cityID: String?, promoNotifications: Bool, feedbackNotifications: Bool) {
        self.init()
        self.phone = phone
        self.name = name
        self.surname = surname
        self.patronymic = patronymic
        self.sex = sex
        self.birthday = birthday
        self.email = email
        self.sendEcoCheck = sendEcoCheck
        self.cityID = cityID
        self.promoNotifications = promoNotifications
        self.feedbackNotifications = feedbackNotifications
    }
    
    
}

extension UserDbModel {
    var model: UserModel {
        return UserModel(phone: self.phone, name: self.name, surname: self.surname, patronymic: self.patronymic, birthday: self.birthday, sex: self.sex, email: self.email, send_eco_check: self.sendEcoCheck, city_id: self.cityID, promo_notifications: self.promoNotifications, feedback_notifications: self.feedbackNotifications)
    }
}
