//
//  StockDbModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 19.03.2023.
//

import RealmSwift

class StockDbModel: Object {
    
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var name: String = ""
    @Persisted var shortName: String = ""
    @Persisted var descriptions: String = ""
    @Persisted var oldPrice: String? = ""
    @Persisted var newPrice: String? = ""
    @Persisted var pictire: String = ""
    @Persisted var showLabel: Bool = false
    @Persisted var labelColor: String? = ""
    @Persisted var labelText: String? = ""
    @Persisted var labelBgColor: String? = ""
    @Persisted var topPosition: Bool = false
    @Persisted var order: Int? = 0
    @Persisted var productsPackId: Int? = 0
    @Persisted var time: String = ""
    
    
    convenience init(id: Int, name: String, shortName: String, description: String, oldPrice: String?, newPrice: String?, picture: String, showLabel: Bool, labelColor: String?, labelText: String?, labelBgColor: String?, topPosition: Bool, order: Int?, productsPackId: Int?, time: String) {
        self.init()
        self.id = id
        self.name = name
        self.shortName = shortName
        self.descriptions = description
        self.oldPrice = oldPrice
        self.newPrice = newPrice
        self.pictire = picture
        self.showLabel = showLabel
        self.labelColor = labelColor
        self.labelText = labelText
        self.labelBgColor = labelBgColor
        self.topPosition = topPosition
        self.order = order
        self.productsPackId = productsPackId
        self.time = time
    }
    
}


extension StockDbModel {
    var model: StockModel {
        return StockModel(id: self.id, name: self.name, short_name: self.shortName, descriptions: self.descriptions, old_price: self.oldPrice, new_price: self.newPrice, time: self.time, picture: self.pictire, top_position: self.topPosition, show_label: self.showLabel, label_text: self.labelText, label_color: self.labelColor, label_bg_color: self.labelBgColor, order: self.order, products_pack_id: self.productsPackId)
    }
}
