//
//  RealmDatabase.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 19.03.2023.
//

import UIKit
import RealmSwift

class RealmDatabase {
    
    static let shared = RealmDatabase()
    let database: Realm
    
    
    private init() {
        do {
//        deleteRealmIfMigrationNeeded: true
//            let config = Realm.Configuration(schemaVersion: 1, migrationBlock: { migration, oldVersion in
            let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
            database = try Realm(configuration: config)
        }
        catch {
            Alert.alert(title: "Woops", body: "Database is failsed", statusBarStyle: .default)
            fatalError(error.localizedDescription)
        }
    }
    
    public func save<T: Object>(object: T, _ errorHandler: @escaping ((_ error : Swift.Error) -> Void) = { _ in return }) {
            do {
                try database.write {
                    database.add(object, update: .all)
                }
            }
            catch {
                errorHandler(error)
            }
        }
    
    public func saveArray<T: Object>(object: [T], _ errorHandler: @escaping ((_ error : Swift.Error) -> Void) = { _ in return }) {
            do {
                try database.write {
                    database.add(object, update: .all)
                }
            }
            catch {
                errorHandler(error)
            }
        }
    
    public func fetch<T: Object>(object: T) -> Results<T> {
        return database.objects(T.self)
        }
    
    func deleteAll() {
        do {
            try database.write{
                database.deleteAll()
            }
        } catch {
            
        }
    }
}
