//
//  StoreDbModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 30.03.2023.
//

import RealmSwift

class StoreDbModel: Object {
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var timeWork: String = ""
    @Persisted var lat: String? = ""
    @Persisted var lon: String? = ""
    @Persisted var address: String = ""
    @Persisted var phone: String? = ""
    @Persisted var cityID: String? = ""
    
    
    
    convenience init(id: Int, timeWork: String, lat: String?, lon: String?, address: String, phone: String?, cityID: String?) {
        self.init()
        self.id = id
        self.timeWork = timeWork
        self.lat = lat
        self.lon = lon
        self.address = address
        self.phone = phone
        self.cityID = cityID
    }
}

extension StoreDbModel {
    var model: StoreModel {
        return StoreModel(id: self.id, address: self.address, time_work: self.timeWork, lat: self.lat, lon: self.lon, phone_number: self.phone, city_id: self.cityID)
    }
}
