//
//  FeedbackDbModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 19.03.2023.
//

import RealmSwift


class FeedbackDbModel: Object {
    
    @Persisted(primaryKey: true) var messageID: Int = 0
    @Persisted var feedbackID: Int = 0
    @Persisted var dateTime: String = ""
    @Persisted var subject: Int = 0
    @Persisted var status: Int = 0
    @Persisted var text: String = ""
    @Persisted var author: Int = 0
    @Persisted var isRead: Int = 0
    
    
    convenience init(messageID: Int, feedbackID: Int, dateTime: String, subject: Int, status: Int, text: String, author: Int, isRead: Int) {
        self.init()
        self.messageID = messageID
        self.feedbackID = feedbackID
        self.dateTime = dateTime
        self.subject = subject
        self.status = status
        self.text = text
        self.author = author
        self.isRead = isRead
    }
    
}

extension FeedbackDbModel {
    var model: FeedbackModel {
        return FeedbackModel(message_id: self.messageID, feedback_id: self.feedbackID, date_time: self.dateTime, subject: self.subject, status: self.status, text: self.text, author: self.author, is_read: self.isRead)
    }
}
