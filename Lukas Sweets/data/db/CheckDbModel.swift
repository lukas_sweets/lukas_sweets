//
//  HistoryDbModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 22.03.2023.
//

import RealmSwift

class CheckDbModel: Object {
    
    @Persisted(primaryKey: true) var id: Int = 0
    @Persisted var date_time: String = ""
    @Persisted var sum_total: Double = 0.0
    @Persisted var bonus_total: Int = 0
    @Persisted var store: String? = ""
    @Persisted var comment: String? = ""
    @Persisted var reason: Int = 1
    @Persisted var qr: String? = ""
    @Persisted var num: String = ""
    @Persisted var bonus_add: Int = 0
    @Persisted var pay_cash: Double = 0.0
    @Persisted var pay_card: Double = 0.0
    @Persisted var pay_bonus: Double = 0.0
    @Persisted var pay_certificate: Double = 0.0
    
    
    convenience init(id: Int, dateTime: String, sumTotal: Double, bonusTotal: Int, store: String?, comment: String?, reason: Int, qr: String?, num: String, bonusAdd: Int, payCash: Double, payCard: Double, payBonus: Double, payCertificate: Double) {
        self.init()
        self.id = id
        self.date_time = dateTime
        self.sum_total = sumTotal
        self.bonus_total = bonusTotal
        self.store = store
        self.comment = comment
        self.reason = reason
        self.qr = qr
        self.num = num
        self.bonus_add = bonusAdd
        self.pay_cash = payCash
        self.pay_card = payCard
        self.pay_bonus = payBonus
        self.pay_certificate = payCertificate
    }
    
}


extension CheckDbModel {
    var model: HistoryModel {
        return HistoryModel(id: self.id, date_time: self.date_time, bonus_total: self.bonus_total, sum_total: self.sum_total, comment: self.comment, reason: self.reason, qr: self.qr, num: self.num, bonus_add: self.bonus_add, pay_cash: self.pay_cash, pay_card: self.pay_card, pay_bonus: self.pay_bonus, pay_certificate: self.pay_certificate, store: self.store)
    }
}

