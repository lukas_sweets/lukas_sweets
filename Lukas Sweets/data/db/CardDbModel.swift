//
//  CardDbModel.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 19.03.2023.
//

import RealmSwift


class CardDbModel: Object {
    
    @Persisted(primaryKey: true) var cardNumber: String = ""
    @Persisted var bonusCard: Int = 0
    @Persisted var balance: Int? = 0
    @Persisted var purchaseAmount: Double? = 0.0
    @Persisted var loyalSize: Int = 3
    
    
    convenience init(cardNumber: String, bonusCard: Int, balance: Int?, purchaseAmount: Double?, loyalSize: Int) {
        self.init()
        self.cardNumber = cardNumber
        self.bonusCard = bonusCard
        self.balance = balance
        self.purchaseAmount = purchaseAmount
        self.loyalSize = loyalSize
    }
}

extension CardDbModel {
    var model: CardModel {
        return CardModel(card_number: self.cardNumber, bonus_card: self.bonusCard, purchase_amount: self.purchaseAmount, balance: self.balance, loyal_size: self.loyalSize )
    }
}
