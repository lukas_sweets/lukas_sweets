//
//  SmsRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 13.03.2023.
//

import Foundation
import Alamofire


class SmsRemoteDataSource: SmsProtocol {
    
    static func sendOtpCode(code: String, phone: String) async -> Result<ResponseCode> {
        let result: Result<ResponseCode> = await withCheckedContinuation({ continuation in
            
            let parameters = [
                "platform" : "ios",
                "phone": phone,
                "code" : code,
            ]
            
           
            AF.request("\(Api.api)/sms/otp", method: .post, parameters: parameters)
            { $0.timeoutInterval = 5 }
                .validate()
                .responseString(completionHandler: {response in
                    switch response.result {
                    case .success:
                        continuation.resume(returning: .success(.ok))
                    case .failure(let error):
                        Log.e(error.localizedDescription)
                        continuation.resume(returning: .error(.init(code: .error, message: error.localizedDescription)))
                    }
                })
        })
        
        return result
    }
    
    
}
