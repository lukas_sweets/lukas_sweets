//
//  SmsRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 13.03.2023.
//

import Foundation

protocol SmsProtocol {
    static func sendOtpCode(code: String, phone: String) async -> Result<ResponseCode>
}


class SmsRepository {
    
    static func sendOtpCode(code: String, phone: String) async -> Result<ResponseCode> {
        return await SmsRemoteDataSource.sendOtpCode(code: code, phone: phone)
    }
    
    
}
