//
//  CatalogRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 07.04.2023.
//

protocol CatalogRepositoryProtocol{
    static func getCategories() async -> Result<CatalogModel>
    static func getProduct(packID: Int, cityID: String?) async -> Result<ProductModel?>
    static func getAvailabilityProduct(packID: Int, cityID: String?) async -> Result<[ProductAvailabilityModel]>
    static func getProductFromBarcode(barcode: String) async -> Result<Int>
    
}

extension CatalogRepositoryProtocol {
}

class CatalogRepository: CatalogRepositoryProtocol {
    
    static func getCategories() async -> Result<CatalogModel> {
        return await CatalogRemoteDataSource.getCategories()
    }
    
    static func getProduct(packID: Int, cityID: String?) async -> Result<ProductModel?> {
        return await CatalogRemoteDataSource.getProduct(packID: packID, cityID: cityID)
    }
    
    static func getAvailabilityProduct(packID: Int, cityID: String?) async -> Result<[ProductAvailabilityModel]> {
        return await CatalogRemoteDataSource.getAvailabilityProduct(packID: packID, cityID: cityID)
    }
    
    static func getProductFromBarcode(barcode: String) async -> Result<Int> {
        return await CatalogRemoteDataSource.getProductFromBarcode(barcode: barcode)
    }
    
}
