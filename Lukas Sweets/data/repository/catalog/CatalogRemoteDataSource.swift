//
//  CatalogRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 07.04.2023.
//

import Alamofire


class CatalogRemoteDataSource: CatalogRepositoryProtocol {
    
    static func getCategories() async -> Result<CatalogModel> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<CatalogModel> = await withCheckedContinuation({ continuation in
                
                AF.request("\(Api.path)/catalog", method: .get )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: CatalogModel.self) { response in
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                print(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }        
    }

    
    static func getProduct(packID: Int, cityID: String?) async -> Result<ProductModel?> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<ProductModel?> = await withCheckedContinuation({ continuation in
                
                var parameters: [String: Any] = [
                    "pack_id": packID,
                ]
                
                if let cityID = cityID {
                    parameters["city_id"] = cityID
                }
                
                AF.request("\(Api.path)/catalog/pack", method: .get, parameters: parameters )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: ProductModel.self) { response in
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
    static func getAvailabilityProduct(packID: Int, cityID: String?) async -> Result<[ProductAvailabilityModel]> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<[ProductAvailabilityModel]> = await withCheckedContinuation({ continuation in
                
                var parameters: [String: Any] = [
                    "pack_id": packID,
                ]
                
                if let cityID = cityID {
                    parameters["city_id"] = cityID
                }
                
                AF.request("\(Api.path)/catalog/availabilities", method: .get, parameters: parameters )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: [ProductAvailabilityModel].self) { response in
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
    
    static func getProductFromBarcode(barcode: String) async -> Result<Int> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Int> = await withCheckedContinuation({ continuation in
                
                let parameters: [String: Any] = [
                    "barcode": barcode
                ]
                
                
                AF.request("\(Api.path)/catalog/find", method: .get, parameters: parameters )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: PackFindBarcodeModel.self) { response in
                        print(response)
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get().pack_id))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
    
}
