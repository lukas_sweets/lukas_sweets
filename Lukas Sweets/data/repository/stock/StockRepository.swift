//
//  StockRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 20.03.2023.
//


protocol StockRepositoryProtocol {}

extension StockRepositoryProtocol {
    private static func getStocksApi() async -> [StockModel] { return [] }
    static func getStocks() async -> ([StockModel], [StockModel]) { return ([], []) }
    static func setStocks(stocks: [StockModel]) async {}
    static func getStock(id: Int) async -> StockModel? { return nil }
}

class StockRepository: StockRepositoryProtocol {
    
    @MainActor
    static func getStocks() async -> ([StockModel], [StockModel]) {
        
        // Download All stocks from API
        let response = await StockRemoteDataSource.getStocksApi()
        
        switch response {
        case .success(let data):
            await StockLocalDataSource.setStocks(stocks: data)
            break
        default:
            break
        }
        
        return await StockLocalDataSource.getStocks()
                
    }
   
    @MainActor
    static func getStock(id: Int?) async -> StockModel? {
        if let ids = id {
            return await StockLocalDataSource.getStock(id: ids)
        } else {
            return nil
        }
    }
}
