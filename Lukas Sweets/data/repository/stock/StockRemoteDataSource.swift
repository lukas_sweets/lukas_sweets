//
//  StockRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 20.03.2023.
//

import Alamofire


class StockRemoteDataSource: StockRepositoryProtocol {
    
    static func getStocksApi() async -> Result<[StockModel]> {
        
        let result: Result<[StockModel]> = await withCheckedContinuation({ continuation in
            
            AF.request("\(Api.path)/stocks/export", method: .get)
            { $0.timeoutInterval = 5 }
                .validate()
                .responseDecodable(of: [StockModel].self) { response in
                    switch response.result {
                    case .success:
                        do {
                            continuation.resume(returning: .success(try response.result.get()))
                        } catch let error {
                            print(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                        break
                        
                    case .failure(let error):
                        print(error.localizedDescription)
                        continuation.resume(returning: .error(nil))
                    }
                }
        })
        
        return result 
    }
    
    
}
