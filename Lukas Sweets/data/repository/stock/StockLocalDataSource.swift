//
//  StockLocalDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 20.03.2023.
//

import RealmSwift

class StockLocalDataSource: StockRepositoryProtocol {
    
    @MainActor
    static func getStocks() async -> ([StockModel], [StockModel]) {

        
        let stocksTopPosition: [StockModel] = RealmDatabase.shared.fetch(object: StockDbModel())
            .where{ $0.topPosition == true }
            .sorted(by: [SortDescriptor(keyPath: "order")])
            .map({$0.model})
        
        let stocksDownPosition: [StockModel] = RealmDatabase.shared.fetch(object: StockDbModel())
            .where{ $0.topPosition == false }
            .sorted(by: [SortDescriptor(keyPath: "order")])
            .map({$0.model})

        return (stocksTopPosition, stocksDownPosition)
    }
    
    
    @MainActor
    static func setStocks(stocks: [StockModel]) async {
        await deleteAllStocks()
        var stockToDb = Array<StockDbModel>()
        for stock in stocks {
            stockToDb.append(stock.modelDb)
        }
        RealmDatabase.shared.saveArray(object: stockToDb)
    }
    
    @MainActor
    static func deleteAllStocks() async {
        do {
            try RealmDatabase.shared.database.write {
                RealmDatabase.shared.database.delete(
                    RealmDatabase.shared.fetch(object: StockDbModel())
                )
            }
        } catch {}
    }
    
    
    @MainActor
    static func getStock(id: Int) async -> StockModel? {
        return RealmDatabase.shared.fetch(object: StockDbModel())
            .where {
                $0.id == id
            }.first?.model
        
    }
    
    
}
