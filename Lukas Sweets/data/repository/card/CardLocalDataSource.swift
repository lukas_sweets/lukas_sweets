//
//  CardLocalDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 13.03.2023.
//

import RealmSwift

class CardLocalDataSource: CardRepositoryProtocol {
    
    @MainActor
    static func setCards(listCardsModel: [CardModel]) async {
        await deleteAllCards()
        RealmDatabase.shared.saveArray(object: listCardsModel.map {$0.modelDb}) { error in
            print(error)
        }
        
        Preference.instance.bonus_card_number = await getBonusCard()?.card_number ?? ""
        Preference.instance.employee_card_number = await getEmployeeCards()?.card_number ?? ""
    }
    
    
    @MainActor
    static func getBonusCard() async -> CardModel? {
        return RealmDatabase.shared.fetch(object: CardDbModel()).where{
            $0.bonusCard == 1
        }.first?.model
    }
    
    
    @MainActor
    static func getEmployeeCards() async -> CardModel? {
        return RealmDatabase.shared.fetch(object: CardDbModel())
            .where{
            $0.bonusCard == 0
        }.first?.model
    }
    
    @MainActor
    static func deleteAllCards() async {
        do {
            try RealmDatabase.shared.database.write {
                RealmDatabase.shared.database.delete(
                    RealmDatabase.shared.fetch(object: CardDbModel())
                )
            }
        } catch {}
    }
    
    
}
