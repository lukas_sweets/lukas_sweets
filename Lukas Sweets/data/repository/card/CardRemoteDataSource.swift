//
//  CardRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Alamofire
import UIKit


class CardRemoteDataSource: CardRepositoryProtocol {
    
    @MainActor
    static func getApiCards(registration: Bool, phone: String) async -> Result<UserCardsModel> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<UserCardsModel> = await withCheckedContinuation({ continuation in
                
                var parameters = [
                    "platform" : "ios",
                    "phone": phone,
                    "token" : Preference.push_token,
                    "deviceID": UIDevice.current.identifierForVendor?.uuidString
                ]
                if (registration){
                    parameters["registration"] = "true"
                }
                
                
                AF.request("\(Api.path)/cards", method: .get, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: UserCardsModel.self) { response in

                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                continuation.resume(returning: .error(ApiError(code: .error, message: error.localizedDescription, responseCode: nil)))
                            }
                            break
                            
                        case .failure(let error):
                            switch response.response?.statusCode {
                            case 401, 402:
                                continuation.resume(returning: .error(ApiError(code: .no_card, message: nil)))
                                break
                            case 403:
                                continuation.resume(returning: .error(ApiError(code: .error_device, message: nil)))
                                break
                            default:
                                continuation.resume(returning: .error(ApiError(code: .error, message: error.localizedDescription)))
                                break
                            }
                            
                        }
                    }
            })
            
            return result
        }
    }
    
    
    static func deleteCard() async -> Result<Any?> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any?> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "card_number" : Preference.instance.bonus_card_number,
                ]
                
                AF.request("\(Api.path)/cards", method: .delete, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .response { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(nil))
                            break
                        case .failure(let error):
                            print(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
    
    @MainActor
    static func registrationBonusCard(phone: String, name: String) async -> Result<Any> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any> = await withCheckedContinuation({ continuation in
                
                    let parameters = [
                        "name": name,
                        "phone": phone,
                        "token": Preference.push_token,
                        "deviceID" : UIDevice.current.identifierForVendor?.uuidString,
                        "platform": "ios"
                    ]

                AF.request("\(Api.path)/cards", method: .post, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .response { response in
                        Log.e(response)
                        print(response)
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(1))
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
}
