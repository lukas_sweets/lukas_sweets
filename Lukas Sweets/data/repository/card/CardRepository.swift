//
//  CardRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 10.03.2023.
//

import Foundation

protocol CardRepositoryProtocol {}

extension CardRepositoryProtocol {
    static func getApiCards(registration: Bool, phone: String) async -> Result<UserCardsModel> { return .error(nil) }
    
    static func setCards(listCardsModel: [CardModel]) async {}
    
    static func getBonusCard() async -> Result<CardModel> { return .error(nil) }
    static func deleteCard() async -> Result<Any?>? { return nil }
    static func registrationBonusCard(phone: String, name: String) async -> Result<Any> { return .error(nil) }
}



class CardRepository: CardRepositoryProtocol {
    
    static func setCards(listCardsModel: [CardModel]) async {
        await CardLocalDataSource.setCards(listCardsModel: listCardsModel)
    }
    
    
    static func getApiCards(registration: Bool = false, phone: String = Preference.phone_number) async -> Result<UserCardsModel> {
        let data = await CardRemoteDataSource.getApiCards(registration: registration, phone: phone)
        
        switch data {
        case .success(let dataModel):
            let _ = await [
                UserRepository.setUser(userModel: dataModel.user),
                CardRepository.setCards(listCardsModel: dataModel.card)
            ]
            
            Preference.phone_number = phone
            break
        default:
            break
        }
        
        return data
    }
    
    
    static func getBonusCard() async -> Result<CardModel> {
        let response = await getApiCards()
        let card = await CardLocalDataSource.getBonusCard()
        
        
        switch response {
            // Api OK
        case .success:
            // Get card from DB
            if let card = card {
                return .success(card)
            } else {
                return .error(nil)
            }
            //Api Error
        case .internet:
            if let card = card {
                return .internet(card)
            } else {
                return .error(nil)
            }
        case .error(let _):
//            Alert.alert(title: String(error?.responseCode ?? 0), body: error?.message ?? "", statusBarStyle: .default)
            return .error(nil)
//        default:
//            return .error(nil)
        }
        
    }
    
    static func deleteCard() async -> Result<Any?> {
        return await CardRemoteDataSource.deleteCard()
    }
    
    static func registrationBonusCard(phone: String, name: String) async -> Result<Any> {
        let resultApi = await CardRemoteDataSource.registrationBonusCard(phone: phone, name: name)
        
        switch resultApi {
        case .success:
            Preference.phone_number = phone
            break
        default:
            break
        }
        
        return resultApi
        
    }


    
}
