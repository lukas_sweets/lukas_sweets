//
//  NewsRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 20.03.2023.
//



protocol NewsRepositoryProtocol {
    static func getNews() async -> [NewsModel]
}


class NewsRepository: NewsRepositoryProtocol {
    
    static func getNews() async -> [NewsModel] {
        return await NewsRemoteDataSource.getNews()
    }
    
    
}
