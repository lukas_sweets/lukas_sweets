//
//  NewsRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 20.03.2023.
//

import Alamofire

class NewsRemoteDataSource: NewsRepositoryProtocol {
    
    static func getNews() async -> [NewsModel] {
        let result: [NewsModel] = await withCheckedContinuation({ continuation in
            
            AF.request("\(Api.path)/news/export", method: .get)
            { $0.timeoutInterval = 5 }
                .validate()
                .responseDecodable(of: [NewsModel].self) { response in
                    switch response.result {
                    case .success:
                        do {
                            continuation.resume(returning: try response.result.get())
                        } catch let error {
                            print(error.localizedDescription)
                            continuation.resume(returning: [])
                        }
                        break
                        
                    case .failure(let error):
                        print(error.localizedDescription)
                        continuation.resume(returning: [])
                    }
                }
        })
        
        return result
    }
    
    
}
