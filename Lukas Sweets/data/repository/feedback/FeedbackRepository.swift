//
//  FeedbackRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 24.03.2023.
//

protocol FeedbackRepositoryProtocol {}



extension FeedbackRepositoryProtocol{
    static func getApiFeedback(readMessages: [Int]) async -> Result<Any?>? { return nil }
    static func getTopicFeedbacks(remote: Bool = true) async -> Result<[FeedbackModel]>? { return nil }
    static func setFeedbacks(feedbacks: [FeedbackModel]) async {}
    static func getFeedbacks(feedbackID: Int, readedMessages: [Int] = [], local: Bool = true) async -> [FeedbackModel] { return [] }
    static func getReadedMessages(feedbackID: Int) async -> [Int] { return [] }
    static func sendAnswerFeedback(feedbackID: Int, dateTime: String, text: String) async -> Result<Any> { return .error(nil) }
    static func sendNewFeedback(feedback: NewFeedbackModel) async -> Result<Any?>? { return nil }
    static func getNewMessages() async -> Int { return 0 }
}

class FeedbackRepository: FeedbackRepositoryProtocol {
    
    static func getApiFeedbacks(readedMessages: [Int] = []) async -> Result<Any?> {
        
        let result = await FeedbackRemoteDataSource.getApiFeedback(readMessages: readedMessages)
        switch result{
        case .success(let data):
            await FeedbackLocalDataSource.setFeedbacks(feedbacks: data)
            return .success(nil)
        case .internet:
            return .internet(nil)
        default:
            return .error(nil)
        }
    }
    
    static func getTopicFeedbacks(remote: Bool = true) async -> Result<[FeedbackModel]> {

        let model = await FeedbackLocalDataSource.getTopicFeedbacks()
        let result: Result<[FeedbackModel]> = .success(model)
        
        if remote {
            switch await getApiFeedbacks(readedMessages: []) {
            case .success:
                return .success(await FeedbackLocalDataSource.getTopicFeedbacks())
            case .internet:
                return .internet(await FeedbackLocalDataSource.getTopicFeedbacks())
            default:
                return .error(nil)
            }
        }
        
        return result
    }
    
    static func getFeedbacks(feedbackID: Int, readedMessages: [Int] = [], remote: Bool = true) async -> Result<[FeedbackModel]> {
        var resultApi: Result<Any?> = .success(nil)
        
        if remote {
            resultApi = await getApiFeedbacks(readedMessages: await FeedbackLocalDataSource.getReadedMessages(feedbackID: feedbackID))
        }
        
        let data = await FeedbackLocalDataSource.getFeedbacks(feedbackID: feedbackID)
        
        switch resultApi {
        case .success:
            return .success(data)
        case .internet:
            return .internet(data)
        default:
            return .error(nil)
        }
    }
    
    static func sendAnswerFeedback(feedbackID: Int, dateTime: String, text: String) async -> Result<Any> {
        return await FeedbackRemoteDataSource.sendAnswerFeedback(feedbackID: feedbackID, dateTime: dateTime, text: text)
    }

    static func sendNewFeedback(feedback: NewFeedbackModel) async -> Result<Any?>? {
        return await FeedbackRemoteDataSource.sendNewFeedback(feedback: feedback)
    }
    
    static func getNewMessages() async -> Int {
        return await FeedbackLocalDataSource.getNewMessages()
    }

}
