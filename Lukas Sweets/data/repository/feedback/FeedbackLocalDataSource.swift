//
//  FeedbcakLocalDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 24.03.2023.
//

import RealmSwift


class FeedbackLocalDataSource: FeedbackRepositoryProtocol {
    
    @MainActor
    static func getTopicFeedbacks() async -> [FeedbackModel] {
        var model: [FeedbackModel] = []
        
        let all = RealmDatabase.shared.fetch(object: FeedbackDbModel()).map{$0.model}
        
        let group = Dictionary(grouping: all, by: { $0.feedback_id })
        group.forEach({ g in
            let dates = g.value.map{$0.date_time.toDateTime()}
            let readeble = g.value.map{$0.is_read}
            let info = g.value.first
            
            if let info = info, let datesMax = dates.max(), let readebleMin = readeble.min() {
                model.append(FeedbackModel(message_id: 0, feedback_id: info.feedback_id, date_time: datesMax.toFormatString(), subject: info.subject, status: info.status, text: "", author: info.author, is_read: readebleMin))
            }
        })
        
        model = model.sorted{ $0.date_time.toDateTime() > $1.date_time.toDateTime()}
        
        return model
    }
    
    
    @MainActor
    static func setFeedbacks(feedbacks: [FeedbackModel]) async {
        RealmDatabase.shared.saveArray(object: feedbacks.map{$0.modelDb})
    }
    
    @MainActor
    static func getFeedbacks(feedbackID: Int) async -> [FeedbackModel] {
       return RealmDatabase.shared.fetch(object: FeedbackDbModel())
            .where {
                $0.feedbackID == feedbackID
            }
            .sorted {
                $0.dateTime.toDateTime() > $1.dateTime.toDateTime()
            }
            .map {
                $0.model
            }
    }
    
    @MainActor
    static func getReadedMessages(feedbackID: Int) async -> [Int] {
        return RealmDatabase.shared.fetch(object: FeedbackDbModel())
            .where {
                ($0.feedbackID == feedbackID) && ($0.isRead == 0) && ($0.author == 1)
            }
            .map{
                $0.messageID
            }
    }
    
    @MainActor
    static func getNewMessages() async -> Int {
        return RealmDatabase.shared.fetch(object: FeedbackDbModel())
            .where({
                ($0.isRead == 0) && ($0.author == 1)
            }).count
    }


}
