//
//  FeedbackRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 24.03.2023.
//

import Alamofire


class FeedbackRemoteDataSource {
    
    static func getApiFeedback(readMessages: [Int] = []) async -> Result<[FeedbackModel]> {
        if !Connectivity.isConnectedToInternet {
            return .internet([])
        } else {
            let result: Result<[FeedbackModel]> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "phone": Preference.phone_number,
                    "read_messages": readMessages.map {String($0)}.joined(separator: ",")
                ]
                
                AF.request("\(Api.path)/feedback/mobile", method: .get, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: [FeedbackModel].self) { response in
                        
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success( try response.result.get()))
                            } catch let error {
                                print(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
        
    }
    
    static func sendAnswerFeedback(feedbackID: Int, dateTime: String, text: String) async -> Result<Any> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "feedback_id": feedbackID,
                    "date_time": dateTime,
                    "text": text,
                    "author": "0",
                    "is_read": "1"
                ] as [String : Any]
                
                AF.request("\(Api.path)/feedback/answer", method: .post, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .response { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(1))
                            break
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
    static func sendNewFeedback(feedback: NewFeedbackModel) async -> Result<Any?>? {
        
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any?> = await withCheckedContinuation({ continuation in
                
                AF.request("\(Api.path)/feedback/new", method: .post, parameters: feedback )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .response { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(nil))
                            break
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
}
