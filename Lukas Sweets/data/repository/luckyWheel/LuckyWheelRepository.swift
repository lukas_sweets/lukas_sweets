//
//  LuckyWheelRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 18.04.2023.
//

import UIKit


protocol LuckyWheelRepositoryProtocol {
    static func startLuckyWheel() async -> Result<LuckyWheelPlayModel>
    static func getCountFreeSpin() async -> Result<LuckyWheelCountModel>
    static func getCoupons() async -> Result<[LuckyWheelCouponsModel]>
}

class LuckyWheelRepository: LuckyWheelRepositoryProtocol {
    
    typealias remoteSource = LuckyWheelRemoteDataSource

    static func startLuckyWheel() async -> Result<LuckyWheelPlayModel> {
        return await remoteSource.startLuckyWheel()
    }
    
    static func getCountFreeSpin() async -> Result<LuckyWheelCountModel> {
        return await remoteSource.getCountFreeSpin()
    }
    
    static func getCoupons() async -> Result<[LuckyWheelCouponsModel]> {
        return await remoteSource.getCoupons()
    }
    
}
