//
//  LuckyWheelRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 18.04.2023.
//

import Alamofire


class LuckyWheelRemoteDataSource: LuckyWheelRepositoryProtocol {
    
    static func startLuckyWheel() async -> Result<LuckyWheelPlayModel> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<LuckyWheelPlayModel> = await withCheckedContinuation({ continuation in
                
                let parameters: [String: Any] = [
                    "phone": Preference.phone_number
                ]
                
                
                AF.request("\(Api.path)/wheel/start", method: .get, parameters: parameters )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: LuckyWheelPlayModel.self) { response in
                        print(response)
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
    
    
    static func getCountFreeSpin() async -> Result<LuckyWheelCountModel> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<LuckyWheelCountModel> = await withCheckedContinuation({ continuation in
                
                let parameters: [String: Any] = [
                    "phone": Preference.phone_number
                ]
                
                
                AF.request("\(Api.path)/wheel/count", method: .get, parameters: parameters )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: LuckyWheelCountModel.self) { response in
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
    
    static func getCoupons() async -> Result<[LuckyWheelCouponsModel]> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<[LuckyWheelCouponsModel]> = await withCheckedContinuation({ continuation in
                
                let parameters: [String: Any] = [
                    "phone": Preference.phone_number
                ]
                
                
                AF.request("\(Api.path)/wheel/coupons", method: .get, parameters: parameters )
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: [LuckyWheelCouponsModel].self) { response in
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            return result
        }
    }
    
    
}
