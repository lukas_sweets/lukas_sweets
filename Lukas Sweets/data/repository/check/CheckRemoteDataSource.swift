//
//  CheckRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 22.03.2023.
//

import Alamofire


class CheckRemoteDataSource: CheckRepositoryProtocol {
    
    static func getApiChecks() async -> Result<HistoryGeneralModel> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<HistoryGeneralModel> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "phone" : Preference.phone_number,
                    "limit": 9999999999,
                ] as [String : Any]
                
                AF.request("\(Api.path)/histories", method: .get, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: HistoryGeneralModel.self) { response in

                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(.init(code: nil, message: error.localizedDescription, responseCode: error.responseCode)))
                            break
                        }
                    }
            })
            
            return result
        }
    }
    
    static func getApiCheck(checkID: Int) async -> Result<[HistoryDetailsModel]> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<[HistoryDetailsModel]> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "id" : checkID,
                ]
                
                AF.request("\(Api.path)/history/details", method: .get, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: [HistoryDetailsModel].self) { response in
                        
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                Log.e(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            Log.e(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                            break
                        }
                    }
            })
            
            return result
        }
    }
    
}
