//
//  CheckLocalDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 22.03.2023.
//

import RealmSwift


class CheckLocalDataSource: CheckRepositoryProtocol {
    
    @MainActor
    static func setChecks(_ historyModel: [HistoryModel]) async {
        await CheckLocalDataSource.deleteAllChecks()
        RealmDatabase.shared.saveArray(object: historyModel
            .map {
                $0.modelDb
                
            }
        )
    }
    
    @MainActor
    static func setCheck(_ checkDetailsModel: [HistoryDetailsModel]) async {

        RealmDatabase.shared.saveArray(object: checkDetailsModel
            .map {
                $0.modelDb
            }
        )
    }
    
    @MainActor
    static func deleteAllChecks() async {
        do {
            try RealmDatabase.shared.database.write {
                RealmDatabase.shared.database.delete(
                    RealmDatabase.shared.fetch(object: CheckDbModel())
                )
            }
        } catch {}
    }
    
    @MainActor
    static func deleteAllCheckDetails(checkID: Int) async {
        do {
            try RealmDatabase.shared.database.write {
                RealmDatabase.shared.database.delete(
                    RealmDatabase.shared.fetch(object: CheckDetailsDbModel()).where{
                        $0.checkID == checkID
                    }
                )
            }
        } catch {}
    }
    
    
    @MainActor
    static func getChecks(limit: Int) async -> [HistoryModel] {
        return RealmDatabase.shared.fetch(object: CheckDbModel())
            .sorted(by: [SortDescriptor(keyPath: "id", ascending: false)])
            .prefix(limit)
            .map {
                $0.model
            }
    }
    
    @MainActor
    static func getCheck(checkID: Int) async -> HistoryModel? {
        print("getCheck", checkID)
        return RealmDatabase.shared.fetch(object: CheckDbModel()).where {
            $0.id == checkID
        }.first?.model
    }
    
    
    @MainActor
    static func getCheckDetails(checkID: Int) async -> [HistoryDetailsModel] {
        return RealmDatabase.shared.fetch(object: CheckDetailsDbModel()).where {
            $0.checkID == checkID
        }
        .map{
            $0.model
        }
        
    }
    
}
