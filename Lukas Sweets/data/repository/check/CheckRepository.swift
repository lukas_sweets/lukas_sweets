//
//  CheckRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 22.03.2023.
//



protocol CheckRepositoryProtocol {
}

extension CheckRepositoryProtocol {
    static func getApiChecks() async -> Result<HistoryGeneralModel> { return .error(nil) }
    static func getApiCheck(checkID: Int) async -> Result<[HistoryDetailsModel]> { return .error(nil) }
    static func setChecks(_ historyModel: [HistoryModel]) async {}
    static func setCheck(_ checkDetailsModel: [HistoryDetailsModel]) async {}
    static func getChecks(limit: Int = 20, reload: Bool = false) async -> [HistoryModel] { return [] }
    static func getCheck(checkID: Int, reload: Bool = false) async -> Result<HistoryModel>? { return nil }
    static func getCheckDetails(checkID: Int, reload: Bool = false) async -> [HistoryDetailsModel] { return [] }
    static func deleteAllChecks() async { return }
    static func deleteAllCheckDetails(checkID: Int) async {}
}



class CheckRepository: CheckRepositoryProtocol {
    
    @MainActor
    static func getApiChecks() async -> Result<HistoryGeneralModel> {
        let result = await CheckRemoteDataSource.getApiChecks()
        
        switch result {
        case .success(let data):
            await CheckLocalDataSource.setChecks(data.history)
            Preference.instance.checks_count = data.countHistory
            break
        default: break
        }
        
        return result
    }
    
    @MainActor
    static func getApiCheck(checkID: Int) async -> Result<[HistoryDetailsModel]> {
        let result = await CheckRemoteDataSource.getApiCheck(checkID: checkID )
        
        switch result {
        case .success(let data):
            await CheckLocalDataSource.deleteAllCheckDetails(checkID: checkID)
            await CheckLocalDataSource.setCheck(data)
            break
        default: break
        }
        
        return result
    }
    
    
    @MainActor
    static func getChecks(limit: Int = 20, reload: Bool = false) async -> Result<[HistoryModel]> {

        if reload {
            switch await getApiChecks() {
            case .success:
                let data = await CheckLocalDataSource.getChecks(limit: limit)
                return .success(data)
            case .internet:
                let data = await CheckLocalDataSource.getChecks(limit: limit)
                return .internet(data)
            case .error:
                return .error(nil)
            }
        } else {
            let data = await CheckLocalDataSource.getChecks(limit: limit)
            return .success(data)
        }
        
    }
    
    
    @MainActor
    static func getCheck(checkID: Int, reload: Bool = false) async -> Result<HistoryModel> {
        var resultDB = await CheckLocalDataSource.getCheck(checkID: checkID)
        var resultApi: Result<HistoryGeneralModel>?


        if reload || resultDB == nil {
            resultApi = await getApiChecks()
            resultDB = await CheckLocalDataSource.getCheck(checkID: checkID)
        }
        
            switch resultApi {
            case .success, .none, .internet:
                if let resultDB = resultDB {
                    return .success(resultDB)
                } else {
                    return .internet(nil)
                }
            default:
                return .error(nil)
            }
        
    }
    
    
    @MainActor
    static func getCheckDetails(checkID: Int, reload: Bool = false) async -> Result<[HistoryDetailsModel]> {
        
        var resultDB = await CheckLocalDataSource.getCheckDetails(checkID: checkID)
        var resultApi: Result<[HistoryDetailsModel]>?

        
        if reload || resultDB.isEmpty {
            resultApi = await getApiCheck(checkID: checkID)
            resultDB = await CheckLocalDataSource.getCheckDetails(checkID: checkID)
        }
        
        switch resultApi {
        case .success, .none, .internet:
//            if let resultDB = resultDB {
                return .success(resultDB)
//            } else {
//                return .internet(nil)
//            }
        default:
            return .error(nil)
        }
        
//        switch resultApi {
//        case .success(let data):
//            await CheckLocalDataSource.setCheck(data)
//            return .success(resultDB)
//        default:
//            if resultDB.isEmpty{
//                return .error
//            }
//            return .internet(resultDB)
//        }
        
    }
    
    
    
    
}
