//
//  UserLocalDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 13.03.2023.
//

import Foundation


class UserLocalDataSource: UserRepositoryProtocol {
 
    @MainActor
    static func getUser() async -> UserModel? {
        return RealmDatabase.shared.fetch(object: UserDbModel()).first?.model
    }
    
    @MainActor
    static func setUser(userModel: UserModel) async  {
        let user = UserDbModel()
        user.name = userModel.name
        user.phone = userModel.phone
        user.surname = userModel.surname
        user.patronymic = userModel.patronymic
        user.sex = userModel.sex
        user.birthday = userModel.birthday
        user.email = userModel.email
        user.sendEcoCheck = userModel.send_eco_check
        user.cityID = userModel.city_id
        
        RealmDatabase.shared.save(object: user)
    }
    
    @MainActor
    static func getUserCity() async -> String? {
        return await getUser()?.city_id
    }
    
    @MainActor
    static func updateUserCity(cityID: String?) async -> Result<Any?>? {
        if var user = await UserLocalDataSource.getUser() {
            user.city_id = cityID
            RealmDatabase.shared.save(object: user.modelDb)
        }
        return nil
    }
    
    @MainActor
    static func updateUserEcoCheck(send: Bool) async -> Result<Any?>? {
        if var user = await UserLocalDataSource.getUser() {
            user.send_eco_check = send
            RealmDatabase.shared.save(object: user.modelDb)
        }
        return nil
    }
    
    @MainActor
    static func updateUserPromoNotifications(send: Bool) async -> Result<Any?>? {
        if var user = await UserLocalDataSource.getUser() {
            user.promo_notifications = send
            RealmDatabase.shared.save(object: user.modelDb)
        }
        return nil
    }
    
    @MainActor
    static func updateUserFeedbackNotifications(send: Bool) async -> Result<Any?>? {
        if var user = await UserLocalDataSource.getUser() {
            user.feedback_notifications = send
            RealmDatabase.shared.save(object: user.modelDb)
        }
        return nil
    }
    
    
}
