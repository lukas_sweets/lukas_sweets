//
//  UserRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 16.03.2023.
//

import Foundation
import Alamofire

class UserRemoteDataSource: UserRepositoryProtocol {
   
    static func updateUser(userModel: UserModel) async -> Result<Any?> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any?> = await withCheckedContinuation({ continuation in
                
                AF.request("\(Api.path)/client", method: .put, parameters: userModel)
                { $0.timeoutInterval = 5 }
                    .response(completionHandler: { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(nil))
                            break
                            
                        case .failure(let error):
                            continuation.resume(returning: .error(ApiError(code: .error, message: error.localizedDescription)))
                        }
                        
                    })
            })
            return result
        }
    }
    
    static func updateUserCity(cityID: String?) async -> Result<Any?>? {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any?> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "phone": Preference.phone_number,
                    "city": cityID
                ]
                
                AF.request("\(Api.path)/client/city", method: .put, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .response(completionHandler: { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(nil))
                            break
                            
                        case .failure(_):
                            continuation.resume(returning: .error(nil))
                        }
                    })
            })
            return result
        }
    }
    
    
    static func updateUserEcoCheck(send: Bool) async -> Result<Any?>? {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any?> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "phone": Preference.phone_number,
                    "send_eco_check": send
                ] as [String : Any]
                
                AF.request("\(Api.path)/client/eco", method: .put, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .response(completionHandler: { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(nil))
                            break
                            
                        case .failure(_):
                            continuation.resume(returning: .error(nil))
                        }
                    })
            })
            return result
        }
    }
    
    
    static func updateUserPromoNotifications(send: Bool) async -> Result<Any?>? {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any?> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "phone": Preference.phone_number,
                    "promo_notifications": send
                ] as [String : Any]
                
                AF.request("\(Api.path)/client/notifications/promo", method: .put, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .response(completionHandler: { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(nil))
                            break
                            
                        case .failure(_):
                            continuation.resume(returning: .error(nil))
                        }
                    })
            })
            return result
        }
    }
    
    static func updateUserFeedbackNotifications(send: Bool) async -> Result<Any?>? {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<Any?> = await withCheckedContinuation({ continuation in
                
                let parameters = [
                    "phone": Preference.phone_number,
                    "feedback_notifications": send
                ] as [String : Any]
                
                AF.request("\(Api.path)/client/notifications/feedback", method: .put, parameters: parameters)
                { $0.timeoutInterval = 5 }
                    .response(completionHandler: { response in
                        switch response.result {
                        case .success:
                            continuation.resume(returning: .success(nil))
                            break
                            
                        case .failure(_):
                            continuation.resume(returning: .error(nil))
                        }
                    })
            })
            return result
        }
    }
    
    
    
}

