//
//  UserRepository.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 13.03.2023.
//

import Foundation

protocol UserRepositoryProtocol {
    static func updateUserCity(cityID: String?) async -> Result<Any?>?
    static func updateUserEcoCheck(send: Bool) async -> Result<Any?>?
    static func updateUserPromoNotifications(send: Bool) async -> Result<Any?>?
    static func updateUserFeedbackNotifications(send: Bool) async -> Result<Any?>?
}

extension UserRepositoryProtocol {
    static func setUser(userModel: UserModel) async { }
    static func getUser() async -> UserModel? { return nil }
    static func updateUser(userModel: UserModel) async -> Result<Any>? {
        return nil
    }
    static func getUserCity() async -> String? { return nil }
}


class UserRepository: UserRepositoryProtocol {
    
    
    
    
    static func getUser() async -> UserModel? {
        return await UserLocalDataSource.getUser()
    }
    
    
    static func setUser(userModel: UserModel) async {
        await UserLocalDataSource.setUser(userModel: userModel)
    }
    
    static func updateUser(userModel: UserModel) async -> Result<Any?> {
        let result = await UserRemoteDataSource.updateUser(userModel: userModel)
        
        switch result {
        case .success: await setUser(userModel: userModel)
        default: break
        }
        
        return result
    }
    
    static func getUserCity() async -> String? {
        return await UserLocalDataSource.getUserCity()
    }
    
    static func updateUserCity(cityID: String?) async -> Result<Any?>?{
        let resultApi = await UserRemoteDataSource.updateUserCity(cityID: cityID)
        
        switch resultApi{
        case .success:
            _ = await UserLocalDataSource.updateUserCity(cityID: cityID)
            break
        default:
            break
        }
        return resultApi
    }
    
    static func updateUserEcoCheck(send: Bool) async -> Result<Any?>?{
        let resultApi = await UserRemoteDataSource.updateUserEcoCheck(send: send)
        
        switch resultApi{
        case .success:
            _ = await UserLocalDataSource.updateUserEcoCheck(send: send)
            break
        default:
            break
        }
        return resultApi
    }
    
    static func updateUserPromoNotifications(send: Bool) async -> Result<Any?>? {
        let resultApi = await UserRemoteDataSource.updateUserPromoNotifications(send: send)
        
        switch resultApi{
        case .success:
            _ = await UserLocalDataSource.updateUserPromoNotifications(send: send)
            break
        default:
            break
        }
        return resultApi
    }
    static func updateUserFeedbackNotifications(send: Bool) async -> Result<Any?>? {
        let resultApi = await UserRemoteDataSource.updateUserFeedbackNotifications(send: send)
        
        switch resultApi{
        case .success:
            _ = await UserLocalDataSource.updateUserFeedbackNotifications(send: send)
            break
        default:
            break
        }
        return resultApi
    }
    
    
}
