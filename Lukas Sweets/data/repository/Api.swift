//
//  Api.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 11.03.2023.
//

import Foundation

protocol ApiProtocol {
    static var path: String { get }
}

struct Api: ApiProtocol {

    static var path : String = "https://sweets.lukas.ua" + "/api" + "/v1"
    static var api : String = "https://sweets.lukas.ua" + "/api"
        
}
