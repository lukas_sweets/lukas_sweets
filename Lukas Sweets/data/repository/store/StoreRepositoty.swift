//
//  StoreRepositoty.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 30.03.2023.
//

protocol StoreRepositoryProtocol {
}

extension StoreRepositoryProtocol {
    static func setStores(stores: [StoreModel]) async {}
    static func deleteAllStores() async {}
    static func getStores(remote: Bool) async -> [StoreModel] { return [] }
    static func getStoresApi() async -> Result<[StoreModel]> { return .error(nil) }
    static func getStores(city: String?) async -> [StoreModel] { return [] }
    static func getCities() async -> Set<String> { return [] }

}

class StoreRepositoty: StoreRepositoryProtocol {
    
    @MainActor
    static func getStores(remote: Bool = false) async -> [StoreModel] {
        if remote {
            switch await StoreRemoteDataSource.getStoresApi() {
            case .success(let data):
                await StoreLocalDataSource.setStores(stores: data)
                break
            default:
                break
            }
        }
        
        return await StoreLocalDataSource.getStores()
    }
    
    @MainActor
    static func getCities() async -> Set<String> {
        return await StoreLocalDataSource.getCities()
    }

    @MainActor
    static func getStores(city: String?) async -> [StoreModel] {
        return await StoreLocalDataSource.getStores(city: city)
    }

    
}
