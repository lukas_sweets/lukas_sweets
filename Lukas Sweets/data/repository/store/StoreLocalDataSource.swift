//
//  StoreLocalDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 30.03.2023.
//

import RealmSwift


class StoreLocalDataSource: StoreRepositoryProtocol {
    
    @MainActor
    static func getStores() async -> [StoreModel] {
        return Array(RealmDatabase.shared.fetch(object: StoreDbModel())
            .map{
                $0.model
            })
            
    }
    
    @MainActor
    static func getStores(city: String?) async -> [StoreModel] {
        if let city = city {
            return Array(RealmDatabase.shared.fetch(object: StoreDbModel())
                .where({
                    $0.cityID == city
                })
                .map{
                    $0.model
                })
        } else {
            return await getStores()
        }
    }
    
    @MainActor
    static func setStores(stores: [StoreModel]) async {
        await deleteAllStores()
        RealmDatabase.shared.saveArray(object: stores.map {$0.modelDb})
    }
    
    @MainActor
    static func deleteAllStores() async {
        do {
            try RealmDatabase.shared.database.write {
                RealmDatabase.shared.database.delete(
                    RealmDatabase.shared.fetch(object: StoreDbModel())
                )
            }
        } catch {}
    }

    @MainActor
    static func getCities() async -> Set<String> {
        print ("get")
        let data = Set(RealmDatabase.shared.fetch(object: StoreDbModel())
            .filter{
                $0.cityID != nil
            }
            .map{$0.cityID})
        return data as! Set<String>

    }

    
}
