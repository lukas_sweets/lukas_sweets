//
//  StoreRemoteDataSource.swift
//  Lukas Sweets
//
//  Created by Maksym Khaidarov on 30.03.2023.
//

import Alamofire

class StoreRemoteDataSource: StoreRepositoryProtocol {
    
    
    static func getStoresApi() async -> Result<[StoreModel]> {
        if !Connectivity.isConnectedToInternet {
            return .internet(nil)
        } else {
            let result: Result<[StoreModel]> = await withCheckedContinuation({ continuation in
                
                AF.request("\(Api.path)/stores/export", method: .get)
                { $0.timeoutInterval = 5 }
                    .validate()
                    .responseDecodable(of: [StoreModel].self) { response in
                        switch response.result {
                        case .success:
                            do {
                                continuation.resume(returning: .success(try response.result.get()))
                            } catch let error {
                                print(error.localizedDescription)
                                continuation.resume(returning: .error(nil))
                            }
                            break
                            
                        case .failure(let error):
                            print(error.localizedDescription)
                            continuation.resume(returning: .error(nil))
                        }
                    }
            })
            
            return result
        }
    }
    
    
}
