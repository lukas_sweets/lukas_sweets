//
//  MyTabBarCtrl.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 14.12.2019.
//

import UIKit
import AudioToolbox
import SwiftMessages

class AppTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
  
    
    private func showBarcodeBottomSheet() {
        do {
            guard let bonus_card_number = Preference.instance.bonus_card_number else { return }
            let view: BarcodeBottomSheet = try SwiftMessages.viewFromNib()
            view.configureDropShadow()
            view.bodyLabel?.text = bonus_card_number
            view.iconImageView?.image = BarcodeGenerator.generate(from: bonus_card_number, descriptor: .code128, size: CGSize(width: view.iconImageView?.frame.width ?? 20, height: view.iconImageView?.frame.height ?? 100))
            view.cornerRadius = 32
            
            var config = SwiftMessages.defaultConfig
            config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            config.preferredStatusBarStyle = .default
            config.duration = .forever
            config.presentationStyle = .bottom
            config.dimMode = .gray(interactive: true)
            SwiftMessages.show(config: config, view: view)
        } catch {
            
        }
    }


    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if tabBarController.viewControllers?.firstIndex(of: viewController)! == 2 {
            showBarcodeBottomSheet()
            return false
        }
        
        navigationController?.navigationBar.barStyle = .default
        navigationController?.setNavigationBarHidden(true, animated: false)


        switch tabBarController.viewControllers?.firstIndex(of: viewController)! {
        case 0:
            self.title = "LUKAS SWEETS"
            tabBarController.navigationItem.hidesBackButton = true
        case 1:
            self.title = "catalog".localized
            tabBarController.navigationItem.hidesBackButton = true
        case 3:
            self.title = "feedback".localized
            tabBarController.navigationItem.hidesBackButton = true
            tabBarController.navigationItem.rightBarButtonItem = nil
        case 4:
            self.title = ""
            tabBarController.navigationItem.hidesBackButton = true
        default:
            break
        }
        
        return true
    }
    
    
    
    
}


