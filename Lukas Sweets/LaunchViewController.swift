//
//  LaunchViewController.swift
//  Lukas Sweets
//
//  Created by MacBook Air on 28.01.2020.
//

import UIKit

final class LaunchViewController: UIViewController {
    
    @IBOutlet weak var christmasBG: UIImageView!
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        
        if(newYear()) {
            christmasBG.isHidden = false
        } else {
            christmasBG.isHidden = true
        }
        
        start()
        
    }
    
    
    
    private func start() {
        ArgAppUpdater.getSingleton().showUpdateWithForce()
        
        Task {
            // Get card API
            let result2 = await [
                CardRepository.getApiCards(),
                StoreRepositoty.getStores(remote: true),
                //CheckRepository.getApiChecks()
            ] as [Any]
            
            switch result2.first as? Result<UserCardsModel> {
                // OK
            case .success:
                //Load store
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppTabBarController")
                navigationController?.pushViewController(vc, animated: true)
                break
            case .internet:
                switch await CardRepository.getBonusCard() {
                case .internet:
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AppTabBarController")
                    navigationController?.pushViewController(vc, animated: true)
                    break
                default:
                    Alert.noInternet()
                    break
                }
                break
            case .error:
                let vc = UIStoryboard(name: "Registration", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController")
                navigationController?.pushViewController(vc, animated: true)
                break
            default:
                break
            }
            
        }
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}




