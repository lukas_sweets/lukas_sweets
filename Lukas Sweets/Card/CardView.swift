//
//  CardView.swift
//  Lukas Sweets
//
//  Created by Максим Хайдаров on 6/8/20.
//

import UIKit

public class CardView: UIView {

    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var cardHolder: UILabel!
    @IBOutlet weak var bonus: UILabel!
    
    @IBOutlet weak var countBonusView: UIView!
    
    @IBOutlet weak var ten: UILabel!
    @IBOutlet weak var hundred: UILabel!
    @IBOutlet weak var thousand: UILabel!
    @IBOutlet weak var oneThousand: UILabel!
    @IBOutlet weak var one: UILabel!
    
    
    @IBOutlet weak var tenths: UILabel!
    @IBOutlet weak var hundredths: UILabel!
    
    
    override init(frame: CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CardView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
