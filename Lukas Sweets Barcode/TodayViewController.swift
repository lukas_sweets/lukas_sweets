//
//  TodayViewController.swift
//  Lukas Sweets Barcode
//
//  Created by MacBook Air on 13.02.2020.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var imageBarCode: UIImageView!
    @IBOutlet weak var textNumberCard: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    let widgetUserDefaults = UserDefaults.init(suiteName: "group.lukassweets.Widget")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //guard let widgetUserDefaults = UserDefaults.init(suiteName: "group.lukasSweets.WidgetShare") else { print("ERROR"); return }
        
        guard let widgetUserDefaults = widgetUserDefaults else { return }
        if let cardNumber = widgetUserDefaults.string(forKey: "cardNumber") {
            if let image = generationBarcode(from: cardNumber) {
                self.imageBarCode.image = UIImage(data: (image.pngData())!)
                self.textNumberCard.text = widgetUserDefaults.string(forKey: "cardNumber")
            }
        } else {
            cardView.isHidden = true
        }
        //        }
        
    }
    
    
    
    
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        
        guard let widgetUserDefaults = widgetUserDefaults else { return }
        if let cardNumber = widgetUserDefaults.string(forKey: "cardNumber") {
            if let image = generationBarcode(from: cardNumber) {
                self.imageBarCode.image = UIImage(data: (image.pngData())!)
                self.textNumberCard.text = widgetUserDefaults.string(forKey: "cardNumber")
            }
        } else {
            cardView.isHidden = true
        }
        
        completionHandler(.newData)
        
    }
    
    
    
    
    
    
    
    
    func generationBarcode(from string: String)->UIImage? {
        let data = string.data(using: .ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 2, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    //END
}



